(define (problem prueba)
(:domain clarc)
(:objects
	;;Events raised by the planner
	question_failed answer_failed max_q_failed max_a_failed - event
	;;External events
	doctor_needed patient_absent - event
	;;behaviors
	call_doctor call_patient ignore speak_louder show_video - behavior
	finish_and_call_doctor - behavior
	
	;qI1
	qI1 - question
;; 	intro_5 intro_6 - label
;; 	intro1_s1 intro1_a1  - label  
;; 	;qI2
;; 	qI2 - question
;; 	intro2_s1 intro2_e1 intro2_a1 intro2_a2 intro2_a3 intro2_a3_2 - label
;; 	intro2_a4 intro3 - label
;; 	;qI3 
;; 	qI3 - question
;;	intro4 intro5 - label =>
	intro1 label 	
;; 	;IntroBarthel
;; 	qIB - question
	;;intro6 intro7_a1 intro8 intro9 - label
	intro2 - label
	;q1
	q1 - question
	q1_s1 q1_o1 q1_o2 q1_o3 q1_e1 q1_a1 q1_t1 - label
	;q2
	q2 - question
	q2_s1 q2_o1 q2_o2 q2_e1 q2_a1 q2_t1 - label
	;q3
	q3 - question
	q3_s1 q3_o1 q3_o2 q3_o3 q3_e1 q3_a1 q3_t1 - label
	;q4
	q4 - question
	q4_s1 q4_o1 q4_o2 q4_e1 q4_a1 q4_t1 - label
	;q5
	q5 - question
	q5_s1 q5_o1 q5_o2 q5_o3 q5_e1 q5_a1 q5_t1 - label
	;q6
	q6 - question
	q6_s1 q6_o1 q6_o2 q6_o3 q6_e1 q6_a1 q6_t1 - label
	;q7
	q7 - question
	q7_s1 q7_o1 q7_o2 q7_o3 q7_e1 q7_a1 q7_t1 - label
	;q8
	q8 - question
	q8_s1 q8_o1 q8_o2 q8_o3 q8_o4 q8_e1 q8_a1 q8_t1 - label
	;q9
	q9 - question
	q9_s1 q9_o1 q9_o2 q9_o3 q9_o4 q9_e1 q9_a1 q9_t1 - label
	;q10
	q10 - question
	q10_s1 q10_o1 q10_o2 q10_o3 q10_e1 q10_a1 q10_t1 - label
)
(:init 
       ;;General
       (can_continue)
;       (detected_event patient_absent)
       (= (number_failed_questions) 0)
       (= (max_failed_questions) 2)
       (= (number_of_questions) 10)
       (= (current_question) 0)
       (question_failed_event question_failed)
       (max_questions_failed_event max_q_failed)
       (answer_failed_event answer_failed)
       (max_answers_failed_event max_a_failed)
       (= (current_label) 0)
;=============================================================================  
       ;;qI1
       (= (question_position qI1) 0)
       (behavior_of_event qI1 question_failed call_doctor)
       (behavior_of_event qI1 doctor_needed call_doctor)
       (behavior_of_event qI1 patient_absent call_patient)
       (behavior_of_event qI1 max_q_failed call_doctor)
       (behavior_of_event qI1 max_a_failed call_doctor)
       (ready_for_answer qI1)
       (= (answers qI1) 0)
       (= (answers_required qI1) 0)
       (= (number_failed_answers qI1) 0)
       (= (max_failed_answers qI1) 1)

       (belongs intro1 qI1)
       (= (label_order intro1) 0)
       (= (max_repetitions intro1) 1)
       (= (repetitions intro1) 0)

       (belongs intro2 qI1)
       (= (label_order intro2) 1)
       (= (max_repetitions intro2) 1)
       (= (repetitions intro2) 0)
       (is_last_label intro2)
       (is_first_farewell intro2)
       
       ;;(belongs intro1_s1 qI1)
       ;;(= (label_order intro1_s1) 0)
       ;;(= (max_repetitions intro1_s1) 1)
       ;;(= (repetitions intro1_s1) 0)

       ;;(belongs intro1_a1 qI1)
       ;;(= (label_order intro1_a1) 1)
       ;;(= (max_repetitions intro1_a1) 2) ;;2 tries to get the answer
       ;;(= (repetitions intro1_a1) 0)
       ;;(needs_feedback intro1_a1)
       ;;(needs_validation intro1_a1)
       ;;(must_be_correct intro1_a1)
       ;;(answer_failed_behavior intro1_a1 answer_failed finish_and_call_doctor)
       ;;(is_last_label intro1_a1)
       ;;(is_first_farewell intro1_a1)
;============================================================================= 
	;;qI2
       ;;(= (question_position qI2) 1)
       ;;(behavior_of_event qI2 question_failed finish_and_call_doctor)
       ;;(behavior_of_event qI2 patient_absent call_patient)
       ;;(behavior_of_event qI2 doctor_needed call_doctor)
       ;;(behavior_of_event qI2 max_q_failed call_doctor)
       ;;(behavior_of_event qI2 max_a_failed call_doctor)
       ;;(ready_for_answer qI2)
       ;;(= (answers qI2) 0)
       ;;(= (answers_required qI2) 1)
       ;;(= (number_failed_answers qI2) 0)
       ;;(= (max_failed_answers qI2) 3)

       ;;(belongs intro2_s1 qI2)
       ;;(= (label_order intro2_s1) 0)
       ;;(= (max_repetitions intro2_s1) 1)
       ;;(= (repetitions intro2_s1) 0)

       ;;(belongs intro2_e1 qI2)
       ;;(= (label_order intro2_e1) 1)
       ;;(= (max_repetitions intro2_e1) 1)
       ;;(= (repetitions intro2_e1) 0)

       ;;(belongs intro2_a1 qI2)
       ;;(= (label_order intro2_a1) 2)
       ;;(= (max_repetitions intro2_a1) 1)
       ;;(= (repetitions intro2_a1) 0)
       ;;(needs_feedback intro2_a1)
       ;;(needs_validation intro2_a1)
       ;;(must_be_correct intro2_a1)
       ;;(answer_failed_behavior intro2_a1 answer_failed ignore)

       ;;(belongs intro2_a2 qI2)
       ;;(= (label_order intro2_a2) 3)
       ;;(= (max_repetitions intro2_a2) 1)
       ;;(= (repetitions intro2_a2) 0)
       ;;(needs_feedback intro2_a2)
       ;;(needs_validation intro2_a2)
       ;;(must_be_correct intro2_a2)
       ;;(answer_failed_behavior intro2_a2 answer_failed ignore) 

       ;;(belongs intro2_a3 qI2)
       ;;(= (label_order intro2_a3) 4)
       ;;(= (max_repetitions intro2_a3) 2)
       ;;(= (repetitions intro2_a3) 0)
       ;;(needs_feedback intro2_a3)
       ;;(needs_validation intro2_a3)
       ;;(must_be_correct intro2_a3)
       ;;(answer_failed_behavior intro2_a3 answer_failed ignore)
       ;;(waiting_for_behavior intro2_a3)
       ;;(required_behavior intro2_a3 speak_louder)

       ;;(belongs intro2_a4 qI2)
       ;;(= (label_order intro2_a4) 5)
       ;;(= (max_repetitions intro2_a4) 1)
       ;;(= (repetitions intro2_a4) 0)
       ;;(waiting_for_behavior intro2_a4)
       ;;(required_behavior intro2_a4 finish_and_call_doctor)

       ;;(belongs intro3 qI2)
       ;;(= (label_order intro3) 6)
       ;;(= (max_repetitions intro3) 1)
       ;;(= (repetitions intro3) 0)
       ;Not sure if this is working
       ;;(is_last_label intro3)
       ;;(is_first_farewell intro3)
;=============================================================================
       	;;qI3 this question does not require patient feedback
       ;;(= (question_position qI3) 2)
       ;;(behavior_of_event qI3 doctor_needed call_doctor)
       ;;(behavior_of_event qI3 patient_absent call_patient)
       ;;(ready_for_answer qI3)
       ;;(= (answers qI3) 0)
       ;;(= (answers_required qI3) 0)
       ;;(= (number_failed_answers qI3) 0)
       ;;(= (max_failed_answers qI3) 1)    


       ;;(belongs intro4 qI3)
       ;;(= (label_order intro4) 0)
       ;;(= (max_repetitions intro4) 1)
       ;;(= (repetitions intro4) 0)

       ;;(belongs intro5 qI3)
       ;;(= (label_order intro5) 1)
       ;;(= (max_repetitions intro5) 1)
       ;;(= (repetitions intro5) 0)
       ;;(is_last_label intro5)
       ;;(is_first_farewell intro5)

;=============================================================================
      ;;qIB introduction to the barthel test
       ;;(= (question_position qIB) 3)
       ;;(behavior_of_event qIB question_failed ignore)
       ;;(behavior_of_event qIB doctor_needed call_doctor)
       ;;(behavior_of_event qIB patient_absent call_patient)
       ;;(behavior_of_event qIB max_q_failed call_doctor)
       ;;(behavior_of_event qIB max_a_failed call_doctor)
       ;;(ready_for_answer qIB)
       ;;(= (answers qIB) 0)
       ;;(= (answers_required qIB) 1)
       ;;(= (number_failed_answers qIB) 0)
       ;;(= (max_failed_answers qIB) 1)

       ;;(belongs intro6 qIB)
       ;;(= (label_order intro6) 0)
       ;;(= (max_repetitions intro6) 1)
       ;;(= (repetitions intro6) 0)

       ;;(belongs intro7_a1 qIB)
       ;;(= (label_order intro7_a1) 1)
       ;;(= (max_repetitions intro7_a1) 1)
       ;;(= (repetitions intro7_a1) 0)
       ;;(needs_feedback intro7_a1)
       ;;(needs_validation intro7_a1)
       ;;(must_be_correct intro7_a1)
       ;;(answer_failed_behavior intro7_a1 answer_failed show_video)
       ;;(waiting_for_behavior intro7_a1)
       ;;(required_behavior intro7_a1 show_video)

       ;;(belongs intro9 qIB)
       ;;(= (label_order intro9) 2)
       ;;(= (max_repetitions intro9) 1)
       ;;(= (repetitions intro9) 0)
       ;;(is_last_label intro9)
       ;;(is_first_farewell intro9)
;==============================================================================
 	;;q1
       (= (question_position q1) 1)
       (behavior_of_event q1 question_failed ignore)
       (behavior_of_event q1 doctor_needed call_doctor)
       (behavior_of_event q1 patient_absent call_patient)
       (behavior_of_event q1 max_q_failed call_doctor)
       (behavior_of_event q1 max_a_failed ignore)
       (ready_for_answer q1)
       (= (answers q1) 0)
       (= (answers_required q1) 1)
       (= (number_failed_answers q1) 0)
       (= (max_failed_answers q1) 1)
       ;;(needs_confirmation q1)
       
       (belongs q1_s1 q1)
       (= (label_order q1_s1) 0)
       (= (max_repetitions q1_s1) 1)
       (= (repetitions q1_s1) 0)

       (belongs q1_o1 q1)
       (= (label_order q1_o1) 1)
       (= (max_repetitions q1_o1) 1)
       (= (repetitions q1_o1) 0)

       (belongs q1_o2 q1)
       (= (label_order q1_o2) 2)
       (= (max_repetitions q1_o2) 1)
       (= (repetitions q1_o2) 0)

       (belongs q1_o3 q1)
       (= (label_order q1_o3) 3)
       (= (max_repetitions q1_o3) 1)
       (= (repetitions q1_o3) 0)

       (belongs q1_e1 q1)
       (= (label_order q1_e1) 4)
       (= (max_repetitions q1_e1) 1)
       (= (repetitions q1_e1) 0)
       
       (belongs q1_a1 q1)
       (= (label_order q1_a1) 5)
       (= (max_repetitions q1_a1) 3)
       (= (repetitions q1_a1) 0)
       (needs_feedback q1_a1)
       (needs_validation q1_a1)
       (answer_failed_behavior q1_a1 answer_failed ignore)

       (belongs q1_t1 q1)
       (= (label_order q1_t1) 6)
       (= (max_repetitions q1_t1) 1)
       (= (repetitions q1_t1) 0)
       (is_last_label q1_t1)
       (is_first_farewell q1_t1)
;==============================================================================
 	;;q2
       (= (question_position q2) 2)
       (behavior_of_event q2 question_failed ignore)
       (behavior_of_event q2 doctor_needed call_doctor)
       (behavior_of_event q2 patient_absent call_patient)
       (behavior_of_event q2 max_q_failed call_doctor)
       (behavior_of_event q2 max_a_failed ignore)
       (ready_for_answer q2)
       (= (answers q2) 0)
       (= (answers_required q2) 1)
       (= (number_failed_answers q2) 0)
       (= (max_failed_answers q2) 1)
       ;;(needs_confirmation q2)
       
       (belongs q2_s1 q2)
       (= (label_order q2_s1) 0)
       (= (max_repetitions q2_s1) 1)
       (= (repetitions q2_s1) 0)

       (belongs q2_o1 q2)
       (= (label_order q2_o1) 1)
       (= (max_repetitions q2_o1) 1)
       (= (repetitions q2_o1) 0)

       (belongs q2_o2 q2)
       (= (label_order q2_o2) 2)
       (= (max_repetitions q2_o2) 1)
       (= (repetitions q2_o2) 0)

       (belongs q2_e1 q2)
       (= (label_order q2_e1) 3)
       (= (max_repetitions q2_e1) 1)
       (= (repetitions q2_e1) 0)
       
       (belongs q2_a1 q2)
       (= (label_order q2_a1) 4)
       (= (max_repetitions q2_a1) 3)
       (= (repetitions q2_a1) 0)
       (needs_feedback q2_a1)
       (needs_validation q2_a1)
       (answer_failed_behavior q2_a1 answer_failed ignore)

       (belongs q2_t1 q2)
       (= (label_order q2_t1) 5)
       (= (max_repetitions q2_t1) 1)
       (= (repetitions q2_t1) 0)
       (is_last_label q2_t1)
       (is_first_farewell q2_t1)
;==============================================================================
 	;;q3
       (= (question_position q3) 3)
       (behavior_of_event q3 question_failed ignore)
       (behavior_of_event q3 doctor_needed call_doctor)
       (behavior_of_event q3 patient_absent call_patient)
       (behavior_of_event q3 max_q_failed call_doctor)
       (behavior_of_event q3 max_a_failed ignore)
       (ready_for_answer q3)
       (= (answers q3) 0)
       (= (answers_required q3) 1)
       (= (number_failed_answers q3) 0)
       (= (max_failed_answers q3) 1)
       ;;(needs_confirmation q3)

       (belongs q3_s1 q3)
       (= (label_order q3_s1) 0)
       (= (max_repetitions q3_s1) 1)
       (= (repetitions q3_s1) 0)

       (belongs q3_o1 q3)
       (= (label_order q3_o1) 1)
       (= (max_repetitions q3_o1) 1)
       (= (repetitions q3_o1) 0)

       (belongs q3_o2 q3)
       (= (label_order q3_o2) 2)
       (= (max_repetitions q3_o2) 1)
       (= (repetitions q3_o2) 0)

       (belongs q3_o3 q3)
       (= (label_order q3_o3) 3)
       (= (max_repetitions q3_o3) 1)
       (= (repetitions q3_o3) 0)

       (belongs q3_e1 q3)
       (= (label_order q3_e1) 4)
       (= (max_repetitions q3_e1) 1)
       (= (repetitions q3_e1) 0)
       
       (belongs q3_a1 q3)
       (= (label_order q3_a1) 5)
       (= (max_repetitions q3_a1) 3)
       (= (repetitions q3_a1) 0)
       (needs_feedback q3_a1)
       (needs_validation q3_a1)
       (answer_failed_behavior q3_a1 answer_failed ignore)

       (belongs q3_t1 q3)
       (= (label_order q3_t1) 6)
       (= (max_repetitions q3_t1) 1)
       (= (repetitions q3_t1) 0)
       (is_last_label q3_t1)
       (is_first_farewell q3_t1)
;==============================================================================
 	;;q4
       (= (question_position q4) 4)
       (behavior_of_event q4 question_failed ignore)
       (behavior_of_event q4 doctor_needed call_doctor)
       (behavior_of_event q4 patient_absent call_patient)
       (behavior_of_event q4 max_q_failed call_doctor)
       (behavior_of_event q4 max_a_failed ignore)
       (ready_for_answer q4)
       (= (answers q4) 0)
       (= (answers_required q4) 1)
       (= (number_failed_answers q4) 0)
       (= (max_failed_answers q4) 1)
       ;;(needs_confirmation q4)
       
       (belongs q4_s1 q4)
       (= (label_order q4_s1) 0)
       (= (max_repetitions q4_s1) 1)
       (= (repetitions q4_s1) 0)

       (belongs q4_o1 q4)
       (= (label_order q4_o1) 1)
       (= (max_repetitions q4_o1) 1)
       (= (repetitions q4_o1) 0)

       (belongs q4_o2 q4)
       (= (label_order q4_o2) 2)
       (= (max_repetitions q4_o2) 1)
       (= (repetitions q4_o2) 0)

       (belongs q4_e1 q4)
       (= (label_order q4_e1) 3)
       (= (max_repetitions q4_e1) 1)
       (= (repetitions q4_e1) 0)
       
       (belongs q4_a1 q4)
       (= (label_order q4_a1) 4)
       (= (max_repetitions q4_a1) 3)
       (= (repetitions q4_a1) 0)
       (needs_feedback q4_a1)
       (needs_validation q4_a1)
       (answer_failed_behavior q4_a1 answer_failed ignore)

       (belongs q4_t1 q4)
       (= (label_order q4_t1) 5)
       (= (max_repetitions q4_t1) 1)
       (= (repetitions q4_t1) 0)
       (is_last_label q4_t1)
       (is_first_farewell q4_t1)

;==============================================================================
 	;;q5
       (= (question_position q5) 5)
       (behavior_of_event q5 question_failed ignore)
       (behavior_of_event q5 doctor_needed call_doctor)
       (behavior_of_event q5 patient_absent call_patient)
       (behavior_of_event q5 max_q_failed call_doctor)
       (behavior_of_event q5 max_a_failed ignore)
       (ready_for_answer q5)
       (= (answers q5) 0)
       (= (answers_required q5) 1)
       (= (number_failed_answers q5) 0)
       (= (max_failed_answers q5) 1)
       ;;(needs_confirmation q5)
       
       (belongs q5_s1 q5)
       (= (label_order q5_s1) 0)
       (= (max_repetitions q5_s1) 1)
       (= (repetitions q5_s1) 0)

       (belongs q5_o1 q5)
       (= (label_order q5_o1) 1)
       (= (max_repetitions q5_o1) 1)
       (= (repetitions q5_o1) 0)

       (belongs q5_o2 q5)
       (= (label_order q5_o2) 2)
       (= (max_repetitions q5_o2) 1)
       (= (repetitions q5_o2) 0)

       (belongs q5_o3 q5)
       (= (label_order q5_o3) 3)
       (= (max_repetitions q5_o3) 1)
       (= (repetitions q5_o3) 0)

       (belongs q5_e1 q5)
       (= (label_order q5_e1) 4)
       (= (max_repetitions q5_e1) 1)
       (= (repetitions q5_e1) 0)
       
       (belongs q5_a1 q5)
       (= (label_order q5_a1) 5)
       (= (max_repetitions q5_a1) 3)
       (= (repetitions q5_a1) 0)
       (needs_feedback q5_a1)
       (needs_validation q5_a1)
       (answer_failed_behavior q5_a1 answer_failed ignore)

       (belongs q5_t1 q5)
       (= (label_order q5_t1) 6)
       (= (max_repetitions q5_t1) 1)
       (= (repetitions q5_t1) 0)
       (is_last_label q5_t1)
       (is_first_farewell q5_t1)
;==============================================================================
 	;;q6
       (= (question_position q6) 6)
       (behavior_of_event q6 question_failed ignore)
       (behavior_of_event q6 doctor_needed call_doctor)
       (behavior_of_event q6 patient_absent call_patient)
       (behavior_of_event q6 max_q_failed call_doctor)
       (behavior_of_event q6 max_a_failed ignore)
       (ready_for_answer q6)
       (= (answers q6) 0)
       (= (answers_required q6) 1)
       (= (number_failed_answers q6) 0)
       (= (max_failed_answers q6) 1)
       ;;(needs_confirmation q6)
       
       (belongs q6_s1 q6)
       (= (label_order q6_s1) 0)
       (= (max_repetitions q6_s1) 1)
       (= (repetitions q6_s1) 0)

       (belongs q6_o1 q6)
       (= (label_order q6_o1) 1)
       (= (max_repetitions q6_o1) 1)
       (= (repetitions q6_o1) 0)

       (belongs q6_o2 q6)
       (= (label_order q6_o2) 2)
       (= (max_repetitions q6_o2) 1)
       (= (repetitions q6_o2) 0)

       (belongs q6_o3 q6)
       (= (label_order q6_o3) 3)
       (= (max_repetitions q6_o3) 1)
       (= (repetitions q6_o3) 0)

       (belongs q6_e1 q6)
       (= (label_order q6_e1) 4)
       (= (max_repetitions q6_e1) 1)
       (= (repetitions q6_e1) 0)
       
       (belongs q6_a1 q6)
       (= (label_order q6_a1) 5)
       (= (max_repetitions q6_a1) 3)
       (= (repetitions q6_a1) 0)
       (needs_feedback q6_a1)
       (needs_validation q6_a1)
       (answer_failed_behavior q6_a1 answer_failed ignore)

       (belongs q6_t1 q6)
       (= (label_order q6_t1) 6)
       (= (max_repetitions q6_t1) 1)
       (= (repetitions q6_t1) 0)
       (is_last_label q6_t1)
       (is_first_farewell q6_t1)
;==============================================================================
 	;;q7
       (= (question_position q7) 7)
       (behavior_of_event q7 question_failed ignore)
       (behavior_of_event q7 doctor_needed call_doctor)
       (behavior_of_event q7 patient_absent call_patient)
       (behavior_of_event q7 max_q_failed call_doctor)
       (behavior_of_event q7 max_a_failed ignore)
       (ready_for_answer q7)
       (= (answers q7) 0)
       (= (answers_required q7) 1)
       (= (number_failed_answers q7) 0)
       (= (max_failed_answers q7) 1)
       ;;(needs_confirmation q7)
       
       (belongs q7_s1 q7)
       (= (label_order q7_s1) 0)
       (= (max_repetitions q7_s1) 1)
       (= (repetitions q7_s1) 0)

       (belongs q7_o1 q7)
       (= (label_order q7_o1) 1)
       (= (max_repetitions q7_o1) 1)
       (= (repetitions q7_o1) 0)

       (belongs q7_o2 q7)
       (= (label_order q7_o2) 2)
       (= (max_repetitions q7_o2) 1)
       (= (repetitions q7_o2) 0)

       (belongs q7_o3 q7)
       (= (label_order q7_o3) 3)
       (= (max_repetitions q7_o3) 1)
       (= (repetitions q7_o3) 0)

       (belongs q7_e1 q7)
       (= (label_order q7_e1) 4)
       (= (max_repetitions q7_e1) 1)
       (= (repetitions q7_e1) 0)
       
       (belongs q7_a1 q7)
       (= (label_order q7_a1) 5)
       (= (max_repetitions q7_a1) 3)
       (= (repetitions q7_a1) 0)
       (needs_feedback q7_a1)
       (needs_validation q7_a1)
       (answer_failed_behavior q7_a1 answer_failed ignore)

       (belongs q7_t1 q7)
       (= (label_order q7_t1) 6)
       (= (max_repetitions q7_t1) 1)
       (= (repetitions q7_t1) 0)
       (is_last_label q7_t1)
       (is_first_farewell q7_t1)
;==============================================================================
 	;;q8
       (= (question_position q8) 8)
       (behavior_of_event q8 question_failed ignore)
       (behavior_of_event q8 doctor_needed call_doctor)
       (behavior_of_event q8 patient_absent call_patient)
       (behavior_of_event q8 max_q_failed call_doctor)
       (behavior_of_event q8 max_a_failed ignore)
       (ready_for_answer q8)
       (= (answers q8) 0)
       (= (answers_required q8) 1)
       (= (number_failed_answers q8) 0)
       (= (max_failed_answers q8) 1)
       ;;(needs_confirmation q8)
       
       (belongs q8_s1 q8)
       (= (label_order q8_s1) 0)
       (= (max_repetitions q8_s1) 1)
       (= (repetitions q8_s1) 0)

       (belongs q8_o1 q8)
       (= (label_order q8_o1) 1)
       (= (max_repetitions q8_o1) 1)
       (= (repetitions q8_o1) 0)

       (belongs q8_o2 q8)
       (= (label_order q8_o2) 2)
       (= (max_repetitions q8_o2) 1)
       (= (repetitions q8_o2) 0)

       (belongs q8_o3 q8)
       (= (label_order q8_o3) 3)
       (= (max_repetitions q8_o3) 1)
       (= (repetitions q8_o3) 0)

       (belongs q8_o4 q8)
       (= (label_order q8_o4) 4)
       (= (max_repetitions q8_o4) 1)
       (= (repetitions q8_o4) 0)

       (belongs q8_e1 q8)
       (= (label_order q8_e1) 5)
       (= (max_repetitions q8_e1) 1)
       (= (repetitions q8_e1) 0)
       
       (belongs q8_a1 q8)
       (= (label_order q8_a1) 6)
       (= (max_repetitions q8_a1) 3)
       (= (repetitions q8_a1) 0)
       (needs_feedback q8_a1)
       (needs_validation q8_a1)
       (answer_failed_behavior q8_a1 answer_failed ignore)

       (belongs q8_t1 q8)
       (= (label_order q8_t1) 7)
       (= (max_repetitions q8_t1) 1)
       (= (repetitions q8_t1) 0)
       (is_last_label q8_t1)
       (is_first_farewell q8_t1)
;==============================================================================
 	;;q9
       (= (question_position q9) 9)
       (behavior_of_event q9 question_failed ignore)
       (behavior_of_event q9 doctor_needed call_doctor)
       (behavior_of_event q9 patient_absent call_patient)
       (behavior_of_event q9 max_q_failed call_doctor)
       (behavior_of_event q9 max_a_failed ignore)
       (ready_for_answer q9)
       (= (answers q9) 0)
       (= (answers_required q9) 1)
       (= (number_failed_answers q9) 0)
       (= (max_failed_answers q9) 1)
       ;;(needs_confirmation q9)
       
       (belongs q9_s1 q9)
       (= (label_order q9_s1) 0)
       (= (max_repetitions q9_s1) 1)
       (= (repetitions q9_s1) 0)

       (belongs q9_o1 q9)
       (= (label_order q9_o1) 1)
       (= (max_repetitions q9_o1) 1)
       (= (repetitions q9_o1) 0)

       (belongs q9_o2 q9)
       (= (label_order q9_o2) 2)
       (= (max_repetitions q9_o2) 1)
       (= (repetitions q9_o2) 0)

       (belongs q9_o3 q9)
       (= (label_order q9_o3) 3)
       (= (max_repetitions q9_o3) 1)
       (= (repetitions q9_o3) 0)

       (belongs q9_o4 q9)
       (= (label_order q9_o4) 4)
       (= (max_repetitions q9_o4) 1)
       (= (repetitions q9_o4) 0)

       (belongs q9_e1 q9)
       (= (label_order q9_e1) 5)
       (= (max_repetitions q9_e1) 1)
       (= (repetitions q9_e1) 0)
       
       (belongs q9_a1 q9)
       (= (label_order q9_a1) 6)
       (= (max_repetitions q9_a1) 3)
       (= (repetitions q9_a1) 0)
       (needs_feedback q9_a1)
       (needs_validation q9_a1)
       (answer_failed_behavior q9_a1 answer_failed ignore)

       (belongs q9_t1 q9)
       (= (label_order q9_t1) 7)
       (= (max_repetitions q9_t1) 1)
       (= (repetitions q9_t1) 0)
       (is_last_label q9_t1)
       (is_first_farewell q9_t1)
;==============================================================================
  	;;q10
       (= (question_position q10) 10)
       (behavior_of_event q10 question_failed ignore)
       (behavior_of_event q10 doctor_needed call_doctor)
       (behavior_of_event q10 patient_absent call_patient)
       (behavior_of_event q10 max_q_failed call_doctor)
       (behavior_of_event q10 max_a_failed ignore)
       (ready_for_answer q10)
       (= (answers q10) 0)
       (= (answers_required q10) 1)
       (= (number_failed_answers q10) 0)
       (= (max_failed_answers q10) 1)
       ;;(needs_confirmation q10)
       
       (belongs q10_s1 q10)
       (= (label_order q10_s1) 0)
       (= (max_repetitions q10_s1) 1)
       (= (repetitions q10_s1) 0)

       (belongs q10_o1 q10)
       (= (label_order q10_o1) 1)
       (= (max_repetitions q10_o1) 1)
       (= (repetitions q10_o1) 0)

       (belongs q10_o2 q10)
       (= (label_order q10_o2) 2)
       (= (max_repetitions q10_o2) 1)
       (= (repetitions q10_o2) 0)

       (belongs q10_o3 q10)
       (= (label_order q10_o3) 3)
       (= (max_repetitions q10_o3) 1)
       (= (repetitions q10_o3) 0)

       (belongs q10_e1 q10)
       (= (label_order q10_e1) 4)
       (= (max_repetitions q10_e1) 1)
       (= (repetitions q10_e1) 0)
       
       (belongs q10_a1 q10)
       (= (label_order q10_a1) 5)
       (= (max_repetitions q10_a1) 3)
       (= (repetitions q10_a1) 0)
       (needs_feedback q10_a1)
       (needs_validation q10_a1)
       (answer_failed_behavior q10_a1 answer_failed ignore)

       (belongs q10_t1 q10)
       (= (label_order q10_t1) 6)
       (= (max_repetitions q10_t1) 1)
       (= (repetitions q10_t1) 0)
       (is_last_label q10_t1)
       (is_first_farewell q10_t1)
  )     
;;As the only way to finish the last question is to pass by all the other ones
;;we just have the last goal
(:goal	(and (test_finished)
))
)

