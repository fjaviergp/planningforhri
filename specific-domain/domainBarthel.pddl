;; -----------------------------------------------------------------------------------------------------------------
;; DOMAIN clark -- test type closed options
;;
;; This domain is for controlling the interaction between a robot and a person while performing a test with closed
;; answers.
;;
;; Based on the Barthel test for elder people
;;
;; Raquel Fuentetaja February 2016
;; -----------------------------------------------------------------------------------------------------------------

(define (domain clark)
  (:requirements :strips :typing :fluents :negative-preconditions :equality) 
  (:types
   
       configuration_properties options_configuration robot patient test test_element time property
       behavior person_type language verbal_tense - object
       ;;AGO: No usamos patient en ning�n predicado, aunque de momento lo podemos dejar
       ;;patient_property environment_property robot_property - property
       pause dur - time
       presentation - test_element
       test_introduction - test_element
       test_end - test_element
       question - test_element
       question_option - test_element
       question_start - test_element
       question_end - test_element
       question_ask_answer - test_element
       question_transition - test_element
       answer - test_element
      

  )

  (:predicates
      ;; test configuration
      (current_test ?test - test)
      (test_configuration ?language - language ?p - person_type ?t - verbal_tense)
      (test_configured)
   
      (can_continue) ;; from robot control. The test can continue
      (internal_can_continue) ;; from robot control. The test can continue
   
      (robot_presentation ?p - presentation ?robot - robot) ;; there is a robot presentation for
							    ;; ?robot with label ?p AGO: si vemos
							    ;; que nos cuesta mucho encontrar planes
							    ;; podr�amos asumir que es un �nico
							    ;; robot y quitar el par�metro robot.

      ;; Next predicates are redundant. Decide if the specific robot is important in actions?
      (robot_introduced ?robot - robot) ;; the robot ?robot has been
					;; introduced
      (finished_introduce_robot) ;; introduction finished AGO: �no tendr�a que tener como par�metro
				 ;; el robot? �no valdr�a con el de robot_introduced?
      
      (test_finished) 
      
      (is_test_introduction ?ti - test_introduction) ;; there is a test introduction for ?test with label ?ti 
      (is_test_introduction_video ?ti - test_introduction) ;; FJGP
      (introduction_finished ?ti - test_introduction) 

      (question ?question - question) ;; there is a question with label ?question in ?test
      (question_started ?question - question) 
      (question_finished ?q - question)

      ;; the current configuration for question ?q is to start with ?s and finish with ?e
      (current_configuration_question_start ?q - question ?s - question_start)
      (current_configuration_question_end ?q - question ?e - question_end) 
      (current_options_configuration ?q - question ?c - options_configuration)
      (is_option_configuration ?c - options_configuration ?master - question_option ?label - question_option)

     
      (is_question_start ?s - question_start ?q - question)
      (is_question_end ?e - question_end ?q - question)
      (is_question_transition ?t  - question_transition ?q - question)
      
      (option ?qo - question_option ?q - question) ;; there is a question option for ?q with label ?qo
      
      (is_ask_answer ?qa - question_ask_answer ?q - question) ;; ?qa represents a way for asking for a question answer
      (valid_answer) ;; exogeneous event
      (changed_answer ?q - question) ;; exogeneous event
      (implies_reproduction ?qa - question_ask_answer ?q - question) ;; a way of asking for an
								     ;; answer is to reproduce the
								     ;; question

      ;; configuration of the question: start, end, when reproducing the question
      (reproduction_configuration ?qa - question_ask_answer  ?s - question_start ?e - question_end)
      (reproduction_configuration_options ?qa - question_ask_answer  ?o - options_configuration) 

      ;; (reproduced_valid_answer ?q - question) ;; the answer has been reproduced
      (do_question_transition) ;;  make a transition to the next question
      (waiting_response)
      
      ;; pause
      (pause ?p - pause)
      (dur ?d - dur)
      
      ;; pause after a test element
      (after_pause ?te - test_element ?p - pause)
      (max_dur ?te - test_element ?d - dur)

      ;; ROBOT CONTROL PREDICATES
      (detected_property ?p - property)
      (behavior_of_property ?p - property ?e - behavior)
      (waiting_restore_from  ?p - property ?e - behavior)
      (waiting_finish ?b - behavior)
      (is_failed_n ?property - property)
      
     
  )
  (:functions
     
     (test_introduction_position ?ti - test_introduction ) ;; relative position of a test
							   ;; introdution
     (number_test_introductions) ;; number of test introduction actions
     (current_introduction) ;; number of executed test introductions

     (test_question_position ?q - question) ;; relative position of a test question
     (number_test_questions ) ;; number of test questions
     (current_question ) ;; number of executed questions

     (question_option_position ?qo - question_option) ;; relative position of an option of a test
						      ;; question
     (number_options ?q - question) ;; number of options
     (current_option ?q - question) ;; number of showed options
    
     (seconds ?t - time)  ;; seconds of a pause


     (current_attempt_answer ?q - question) ;; attempts to obtain valid a asnwer
     (number_attempts_answer ?q - question) ;; number of attempts to obtain a valid answer
     (question_ask_answer_position ?qa - question_ask_answer) ;; relative position of an option of a
							      ;; test question
     (number_failed_questions)
     (max_failed_questions)
 )

 

;; -----------------------------------------------------------------------------------------------------------------
;; TEST CONTROL ACTIONS
;; -----------------------------------------------------------------------------------------------------------------
(:action configure-test
     :parameters    (?test - test ?language - language ?p - person_type ?t - verbal_tense)
     :precondition  (and
		      ;; test control
                      (not (test_configured ))
		      (test_configuration  ?language ?p ?t)
		      (current_test ?test)
		      )
     :effect 	(and
		 (test_configured )
		 )
     
  )



  (:action introduce-robot
     :parameters    (?p - presentation ?robot - robot  ?pause - pause)
     :precondition  (and
		      ;; overall. Robot control
                      (can_continue)
                      (internal_can_continue)

		      ;; test control
		      (test_configured )
		      (robot_presentation ?p ?robot)
        	      (not (robot_introduced ?robot))
                      (after_pause ?p ?pause)
		      )
     :effect 	(and
		 (robot_introduced ?robot)
                 (finished_introduce_robot)
		 )
     
  )

 
(:action introduce-test
  :parameters 	(?i - test_introduction ?pause - pause)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
		 
         	 ;; test control
		 (test_configured )		 
		 (finished_introduce_robot)
		 (is_test_introduction ?i )
                 (= (current_introduction ) (test_introduction_position ?i ))
		 (after_pause ?i ?pause)
		 )
  :effect 	(and
		 (introduction_finished ?i )
                 (increase (current_introduction ) 1)
		 
		 )
  )

;; FJGP
(:action introduce-test-video
  :parameters 	(?i - test_introduction ?pause - pause)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
		 
         	 ;; test control		 
		 (finished_introduce_robot)
		 (is_test_introduction_video ?i )
                 (= (current_introduction ) (test_introduction_position ?i ))
		 (after_pause ?i ?pause)
		 )
  :effect 	(and
		 (introduction_finished ?i )
                 (increase (current_introduction ) 1)
		 
		 )
  )

(:action start-question
  :parameters 	(?question_start - question_start ?question - question  ?pause - pause) ;;AGO: aqu�
											;;tenemos
											;;muchos
											;;par�metros,
											;;si vemos
											;;que no
											;;escala
											;;podr�amos
											;;considerar
											;;partirla
											;;en dos
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
                 (> (current_introduction ) (number_test_introductions ));; AGO: si hiciera falta
									 ;; este se podr�a cambiar
									 ;; por un (introduced_test
									 ;; ), habr�a que hacer una
									 ;; nueva acci�n de
									 ;; finish_introduction que
									 ;; diera ese predicado, as�
									 ;; que no tengo muy claro
									 ;; si ganar�amos mucho,
									 ;; pero si hace falta se
									 ;; puede probar
 		 (question ?question )
		 (is_question_start ?question_start ?question)
		 (current_configuration_question_start ?question ?question_start)
                 (= (current_question ) (test_question_position ?question ))
 		 (<= (current_question ) (number_test_questions ))
 		 (not (question_started ?question ))
		 (after_pause ?question_start ?pause)
		 )
		 
  :effect 	(and
 		 (not (current_configuration_question_start  ?question ?question_start))
		 (not (valid_answer))
		 (question_started ?question )

		 )
  )

 (:action show-question-option
  :parameters 	(
		 ?question_option_label - question_option
		 ?question_option - question_option
	         ?question - question
		 ?options_configuration - options_configuration
		 ?pause - pause)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
		 
         	 ;; test control		    		 
		 (question_started ?question )
                 (= (current_question ) (test_question_position ?question ))
		 (option ?question_option ?question)
		 (= (current_option ?question) (question_option_position ?question_option))
		 (after_pause ?question_option_label ?pause)
		 (current_options_configuration ?question ?options_configuration)
                 (is_option_configuration ?options_configuration ?question_option ?question_option_label)
		 
                )
  :effect 	(and
		 (increase (current_option ?question) 1)
		 (not (is_option_configuration ?options_configuration ?question_option ?question_option))
		 )
  )


(:action finish-question
  :parameters 	(?question_end - question_end ?question - question  ?pause - pause)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (question_started ?question )
                 (= (current_question ) (test_question_position ?question ))		 
 		 (not (question_finished ?question ))
		 (current_configuration_question_end ?question ?question_end)
		 (> (current_option ?question) (number_options ?question))
                 (is_question_end ?question_end ?question)
		 (after_pause ?question_end ?pause)
                )

  :effect 	(and
 		 (not (current_configuration_question_end  ?question ?question_end))
		 (question_finished ?question )

		 )
  )

(:action ask-for-answer
  :parameters 	(?question_ask_answer - question_ask_answer ?question - question)
			   ;?pause - pause
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))		 
		 (not (valid_answer))
                 (is_ask_answer ?question_ask_answer ?question)
		 (not (implies_reproduction ?question_ask_answer ?question))
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))		 
;		 (after_pause ?question_ask_answer ?pause)
                )
  :effect 	(and
;               (increase (current_attempt_answer ?question) 1)
;;		(valid_answer) ;; simulation exogeneous event
		(waiting_response)

		)
 )

(:action ask-for-answer-reproduce
  :parameters 	(?question_ask_answer - question_ask_answer
		 ?question - question
                 ?question_start - question_start ?question_end - question_end
		 ?options_configuration - options_configuration
		 )
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))		 
		 (not (valid_answer))
                 (is_ask_answer ?question_ask_answer ?question)
		 (implies_reproduction ?question_ask_answer ?question)
		 (reproduction_configuration ?question_ask_answer  ?question_start ?question_end)
		 (reproduction_configuration_options ?question_ask_answer ?options_configuration)
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))		 
;		 (after_pause ?question_ask_answer ?pause)
                )
  :effect 	(and
		(not (question_started ?question ))
		(not (question_finished ?question ))
		(assign (current_option ?question) 1)
		
		(current_configuration_question_start ?question ?question_start)
		(current_configuration_question_end ?question ?question_end)
		(current_options_configuration ?question ?options_configuration)
                ;;(increase (current_attempt_answer ?question) 1)
		;;(waiting_response)
		;;(not (implies_reproduction ?question_ask_answer ?question))
                (increase (current_attempt_answer ?question) 1)		 
		 )
 )

(:action receive-answer
  :parameters 	(?question_ask_answer - question_ask_answer  ?question - question  ?dur - dur)
  :precondition (and
                 ;;overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;;test control
		 (waiting_response)
		 (question_finished ?question)
		 
                  (is_ask_answer ?question_ask_answer ?question)
;;   		 (not (reproduced_valid_answer ?question))
                 (= (current_question ) (test_question_position ?question ))		 
		 (> (current_attempt_answer ?question) 0)
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))		 
		 (max_dur ?question_ask_answer ?dur)
		 
                )

  :effect 	(and
                (increase (current_attempt_answer ?question) 1)		 
                (valid_answer)
		(not (waiting_response))


		)
 )
	 

(:action finish-ask-answer-fail-continue
  :parameters 	(?question - question)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		    		 
		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))
		 (not (waiting_response))
 		 (not (valid_answer))
		 (> (current_attempt_answer ?question) (number_attempts_answer ?question))
		 (< (number_failed_questions) (max_failed_questions))
                )

  :effect 	(and
		 (do_question_transition)
         	 (increase (number_failed_questions) 1)		 		 
		 )
  )

(:action finish-ask-answer-fail-interrupt
  :parameters 	(?question - question ?property - property)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		    		 
		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))
		 (not (waiting_response))
 		 (not (valid_answer))
		 (> (current_attempt_answer ?question) (number_attempts_answer ?question))
		 (>= (number_failed_questions) (max_failed_questions))
		 (is_failed_n ?property)
                )

  :effect 	(and
		 ;;(not (can_continue))
		 (not (internal_can_continue))
         	 ;(increase (number_failed_questions) 1)
		 ;;(decrease (number_failed_questions) 1) ;; reset this number if clinician decides to
						        ;; continue the test (maybe assign)
		 (assign (number_failed_questions) 0)
		 (detected_property ?property)
		 (do_question_transition)
		 )
  )




(:action finish-ask-answer-success
  :parameters 	(?question - question)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
 		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))
  		 (not (waiting_response))
;;		 (not (changed_answer ?question))
		 (valid_answer)


;; 		 (reproduced_valid_answer ?question)
                )

  :effect 	(and
		 (do_question_transition)
		 (assign (number_failed_questions) 0)
		 )
  )


(:action make-question-transition
  :parameters 	(?qt - question_transition ?q - question ?p - pause)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (do_question_transition)
		 (is_question_transition ?qt ?q)
		 (after_pause ?qt ?p)
                 (= (current_question ) (test_question_position ?q ))
		 (< (current_question ) (number_test_questions ))
		 )
  :effect 	(and

		 (increase (current_question ) 1)
		 (not (do_question_transition))
		 )
  )

(:action finish-test
  :parameters 	(?test-end - test_end)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
		 (do_question_transition)
		 (= (current_question ) (number_test_questions ))
		 )
  :effect 	(and
		 (not (do_question_transition))
                 (test_finished ))
 )

;; action process test results? ;;AGO: probablemente s�, pero yo no me meter�a de momento


;; -----------------------------------------------------------------------------------------------------------------
;; ROBOT CONTROL ACTIONS
;; -----------------------------------------------------------------------------------------------------------------

(:action interrupt  ;;AGO: esto podr�a incluir una acci�n para que el robot hiciera algo como ir a
			       ;;buscar al paciente. Pero se puede implementar tambi�n a bajo nivel,
			       ;;de forma que si el executive la recibe la descomponga en buscar y
			       ;;esperar por ejemplo.  AGO mi duda aqu� es c�mo vamos a implementar
			       ;;esto, porque mandamos la acci�n y nuestro plan ya no puede
			       ;;continuar. Quiz�s un efecto de esta acci�n deber�a ser (not
			       ;;(patient_absent))
  :parameters 	(?test - test ?prop - property ?behavior - behavior)
  :precondition (and
		 (detected_property ?prop) ;;exogeneous event
                 (behavior_of_property ?prop ?behavior) 
		 (not (can_continue)) ;;exogeneous event
		 (test_configured)
		 )

  :effect 	(and
		 (waiting_restore_from ?prop ?behavior)
                 (not (waiting_response))		 
		 )
  )


(:action internal-interrupt  ;;AGO: esto podr�a incluir una acci�n para que el robot hiciera algo como ir a
			       ;;buscar al paciente. Pero se puede implementar tambi�n a bajo nivel,
			       ;;de forma que si el executive la recibe la descomponga en buscar y
			       ;;esperar por ejemplo.  AGO mi duda aqu� es c�mo vamos a implementar
			       ;;esto, porque mandamos la acci�n y nuestro plan ya no puede
			       ;;continuar. Quiz�s un efecto de esta acci�n deber�a ser (not
			       ;;(patient_absent))
  :parameters 	(?test - test ?prop - property ?behavior - behavior)
  :precondition (and
		 (detected_property ?prop) ;;exogeneous event
                 (behavior_of_property ?prop ?behavior) 
		 (not (internal_can_continue)) ;;exogeneous event
		 (test_configured)
		 )

  :effect 	(and
		 (not (detected_property ?prop))
		 (waiting_restore_from ?prop ?behavior)
                 (not (waiting_response))		 
		 )
  )


(:action restore-from
  :parameters 	(?behavior - behavior ?prop - property)
  :precondition (and
;;    		   (detected_property ?prop) ;;exogeneous event		   
                   (waiting_restore_from ?prop ?behavior))

  :effect 	(and
     		   (not (detected_property ?prop)) ;;exogeneous event		   
                   (not (waiting_restore_from ?prop ?behavior))
		   (can_continue))

)


(:action restore-from-internal
  :parameters 	(?behavior - behavior ?prop - property)
  :precondition (and
;;   		   (detected_property ?prop) ;;exogeneous event		   
                   (waiting_restore_from ?prop ?behavior))

  :effect 	(and
    		   (not (detected_property ?prop));;
                   (not (waiting_restore_from ?prop ?behavior))
		   (internal_can_continue))

)


)


;; Posible sub-states one or more (detected_property prop), not (can_continue), any not
;; (waiting_restore prop) one or more (detected_property prop), not (can_continue), one or more
;; (waiting_restore prop) any (detected_property), (can_continue),



