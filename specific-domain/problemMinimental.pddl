(define (problem p1)
(:domain clark)
(:objects
   the_robot - robot
   the_patient - patient
   spanish english french - language
   present - verbal_tense
   patient  - person_type
   minimental - test
   first second third - question_configuration
   check_eyes  detect_posture_sequence get_posture_sequence handwriting draw - tracking_item

   ;; Behaviors
   robot_pause robot_call_clinician robot_call_technician robot_call_patient robot_front_patient -  behavior
   patient_absent patient_ask_for_help failed_n front_patient - property
   ;;patient_property
   robot_low_battery pause_button_pressed - property
   ;;robot_property

   ;; Labels 
   robot_pres1 - presentation
   intro1 intro2 intro3 intro4 intro5 intro6 intro7 - test_introduction
   end1 - test_end
   q1 q2 q3 q4 q5 q6 q7 q8 q9 q10 q11 q12 q13 q13_1 q13_2 q13_3 q13_4 q13_5 q14 q15 q16 q17 q18 q19 q20 q21 - question
   ;; Even if questions have no options we need to declare an object of this type for the 
   ;; domain to run
;   no_option - question_option
  
   ;; q1
   q1_s1 - question_start 
   q1_e1 - question_end
   q1_a1 q1_a2 q1_a3  - question_ask_answer
   q1_t - question_transition
   q_r - question_repetition
   q_m - question_modification
   q_srm - question_repetition_modification
   q_sr - question_repetition_modification
   q_sm - question_repetition_modification
   q_sn - question_repetition_modification

   ;; q2
   q2_s1  - question_start
   q2_e1  - question_end
   q2_a1 q2_a2 q2_a3  - question_ask_answer
   q2_t - question_transition
  
   ;; q3
   q3_s1  - question_start 
   q3_e1  - question_end
   q3_a1 q3_a2 q3_a3  - question_ask_answer
   q3_t - question_transition

    ;; q4
   q4_s1  - question_start
   q4_e1  - question_end
   q4_a1 q4_a2 q4_a3  - question_ask_answer
   q4_t - question_transition

   ;; q5
   q5_s1  - question_start 
   q5_e1  - question_end
   q5_a1 q5_a2 q5_a3  - question_ask_answer
   q5_t - question_transition

   ;; q6
   q6_s1  - question_start 
   q6_e1  - question_end
   q6_h1 q6_a1 q6_a2 q6_a3  - question_ask_answer
   q6_t - question_transition

   ;; q7
   q7_s1  - question_start 
   q7_e1  - question_end
   q7_a1 q7_a2 q7_a3  - question_ask_answer
   q7_t - question_transition

   ;; q8
   q8_s1 q8_s2 - question_start 
   q8_e1 q8_e2 - question_end
   q8_a1 q8_a2 q8_a3   - question_ask_answer
   q8_t - question_transition


   ;; q9
   q9_s1 q9_s2 - question_start 
   q9_e1 q9_e2 - question_end
   q9_a1 q9_a2 q9_a3   - question_ask_answer
   q9_t - question_transition


   ;; q10
   q10_s1 q10_s2 - question_start 
    q10_e1 q10_e2 - question_end
   q10_a1 q10_a2 q10_a3  - question_ask_answer
   q10_t - question_transition


   ;; q11
   q11_s1  - question_start 
   q11_e1 - question_end
   q11_a1   - question_ask_answer
   q11_t - question_transition
   
   ;; q12
   q12_s1  - question_start 
   q12_e1 - question_end
   q12_a1 q12_a2 - question_ask_answer
   q12_t - question_transition

   ;; q13
   q13_s1  - question_start 
   q13_s2 - question_option ;fake options as the question has many starts
   q13_s3 - question_end
   q13_a1 q13_a2 q13_a3  - question_ask_answer
   q13_t - question_transition

   ;; q13_1
   q13_1_s1  - question_start 
   q13_1_e1 - question_end
   q13_1_a1 q13_1_a2  q13_1_a3  - question_ask_answer
   q13_1_t - question_transition

   ;; q13_2
   q13_2_s1  - question_start 
   q13_2_e1 - question_end
   q13_2_a1 q13_2_a2  q13_2_a3 - question_ask_answer
   q13_2_t - question_transition

   ;; q13_3
   q13_3_s1  - question_start 
   q13_3_e1 - question_end
   q13_3_a1 q13_3_a2  q13_3_a3 - question_ask_answer
   q13_3_t - question_transition

   ;; q13_4
   q13_4_s1  - question_start 
   q13_4_e1 - question_end
   q13_4_a1 q13_4_a2  q13_4_a3 q13_4_t - question_ask_answer
   q13_4_t - question_transition

   ;; q13_5
   q13_5_s1  - question_start 
   q13_5_e1 - question_end
   q13_5_a1 q13_5_a2  q13_5_a3 - question_ask_answer
   q13_5_t - question_transition

   ; ;; q13_6
   ; q13_6_s1  q13_6_s2 - question_start 
   ; q13_6_e1  q13_6_e2 - question_end
   ; q13_6_a1 q13_6_a2  q13_6_a3 q13_6_a4 q13_6_a5 - question_ask_answer
   ; q13_6_t - question_transition

     ;; q14
   q14_s1 - question_start 
   q14_e1 - question_end
   q14_a1 q14_a2 q14_a3  - question_ask_answer
   q14_t - question_transition

   ;; q15
   q15_s1  - question_start 
   q15_s2  - question_option ;fake options as the question has many starts
   q15_e1 - question_end
   q15_a1 q15_a2 q15_a3  - question_ask_answer
   q15_t - question_transition

   ;; q16
   q16_s1  - question_start 
   q16_e1 - question_end
   q16_a1 q16_a2 q16_a3  - question_ask_answer
   q16_t - question_transition

   ;; q17
   q17_s1 q17_s5 - question_start 
   q17_s2 q17_s3 q17_s6 q17_s7 - question_option ;fake options as the question has many starts
   q17_e1 q17_e2 - question_end
   q17_a1 q17_s4 q17_a2  - question_ask_answer
   q17_t - question_transition

   ;; q18
   q18_s1  - question_start 
   q18_e1  - question_end
   q18_o1 q18_o2 - question_option ;fake options as the question has many starts
   q18_a1  - question_ask_answer
   q18_t   - question_transition

   ; ;; q18_1
   ; q18_1_s1  - question_start 
   ; q18_1_e1  - question_end
   ; q18_1_a1  - question_ask_answer
   ; q18_1_t   - question_transition

   ; ;; q18_2
   ; q18_2_s1  - question_start 
   ; q18_2_e1  - question_end
   ; q18_2_a1  - question_ask_answer
;   q18_2_t   - question_transition

   ;; q19
   q19_s1  - question_start 
   q19_e1  - question_end
   q19_a1  - question_ask_answer
   q19_t   - question_transition

   ;; q20
   q20_s1  - question_start 
   q20_e1  - question_end
   q20_a1  - question_ask_answer
   q20_t   - question_transition

   ;; q21
   q21_s1  - question_start 
   q21_s2  - question_option ;fake options as the question has many starts
   q21_e1  - question_end
   q21_a1  - question_ask_answer
   q21_t   - question_transition

   ;; Pauses
   pause_0sg pause_1sg pause_2sg pause_4sg pause_6sg  pause_7sg pause_10sg pause_15sg - pause
   dur_0sg dur_1sg dur_2sg dur_4sg dur_5sg dur_6sg  dur_7sg dur_10sg dur_15sg dur_30sg dur_60sg dur_5min - dur
)
(:init
 ;; test configuration
 (current_test minimental)
 (test_configuration spanish patient present)

 (finished_introduce_robot)
 
 ;; Robot control
(is_failed_n failed_n)
 
(can_continue)
(internal_can_continue)

;(detected_property patient_absent)
;(detected_property patient_ask_for_help)

(behavior_of_property pause_button_pressed robot_pause)
(behavior_of_property patient_absent robot_call_patient)
(behavior_of_property patient_ask_for_help robot_call_clinician)
(behavior_of_property robot_low_battery robot_call_technician)
(behavior_of_property failed_n robot_call_clinician)
(behavior_of_property front_patient robot_call_patient)
		      
(= (number_failed_questions) 0)
(= (max_failed_questions) 2)
(= (number_consecutive_failed_questions) 0) ;; FJGP

 ;;----------------------------------------------------------------------------------------
 ;; Pauses
 ;;----------------------------------------------------------------------------------------
;;Removed AGO: no se usan para nada, se usan solo las etiquetas

;  (pause pause_0sg)
;  (= (seconds pause_0sg) 0)

;  (pause pause_1sg)
;  (= (seconds pause_1sg) 1)

;  (pause pause_2sg) 
;  (= (seconds pause_2sg) 2)

; (pause pause_4sg) 
;  (= (seconds pause_4sg) 4)

;  (pause pause_6sg) 
;  (= (seconds pause_6sg) 6)

;  (pause pause_7sg) 
;  (= (seconds pause_7sg) 7)
 
;  (pause pause_10sg) 
;  (= (seconds pause_10sg) 10)

;  (pause pause_15sg) 
;  (= (seconds pause_15sg) 15)

;  (dur dur_0sg)
;  (= (seconds dur_0sg) 0)

;  (dur dur_1sg)
;  (= (seconds dur_1sg) 1)

;  (dur dur_2sg) 
;  (= (seconds dur_2sg) 2)

;  (dur dur_4sg) 
;  (= (seconds dur_4sg) 4)

;  (dur dur_5sg) 
;  (= (seconds dur_5sg) 5)

;  (dur dur_6sg) 
;  (= (seconds dur_6sg) 6)

;  (dur dur_7sg) 
;  (= (seconds dur_7sg) 7)
 
;  (dur dur_10sg) 
;  (= (seconds dur_10sg) 10)

;  (dur dur_15sg) 
;  (= (seconds dur_15sg) 15)
 
;  (dur dur_30sg) 
;  (= (seconds dur_30sg) 30)

;  (dur dur_5min)
;  (= (seconds dur_5min) 300)

 ;;----------------------------------------------------------------------------------------
 ;; Robot presentation
 ;;----------------------------------------------------------------------------------------
 (robot_presentation robot_pres1 the_robot)
 (after_pause robot_pres1 pause_0sg)


 ;;----------------------------------------------------------------------------------------
 ;; MMSE test introductions
 ;;----------------------------------------------------------------------------------------
 (= (number_test_introductions ) 2)
 ;; Initial performed introductions
 (= (current_introduction ) 1)

 
 ;;(is_test_introduction intro1 )
 ;;(= (test_introduction_position intro1 ) 1)
 ;;(after_pause intro1 pause_1sg)


 ;;(is_test_introduction intro2 )
 ;;(= (test_introduction_position intro2 ) 2)
 ;;(after_pause intro2 pause_1sg)

 ;;(is_test_introduction intro3 )
 ;;(= (test_introduction_position intro3 ) 3)
 ;;(after_pause intro3 pause_1sg)


 ;;(is_test_introduction intro4 )
 ;;(= (test_introduction_position intro4 ) 4)
 ;;(after_pause intro4 pause_1sg)


 (is_test_introduction intro5 )
 (= (test_introduction_position intro5 ) 1)
 (after_pause intro5 pause_1sg)

 
 (is_test_introduction intro6 )
 (= (test_introduction_position intro6 ) 2)
 (after_pause intro6 pause_1sg)


  ;;(is_test_introduction intro7 )
  ;;(= (test_introduction_position intro7 ) 7)
  ;;(after_pause intro7 pause_2sg)


 
 ;;----------------------------------------------------------------------------------------
 ;;  test questions
 ;;----------------------------------------------------------------------------------------
 ( =  (number_test_questions ) 26)
 ;; Initial performed questions
 (= (current_question ) 1)
 (= (current_number_repetitions) 0)
 (current_configuration first)
 (next_configuration first second)
 (next_configuration second third)
 (is_first_configuration first)
 
 ;;----------------------------------------------------------------------------------------
 ;; question 1
 ;;----------------------------------------------------------------------------------------
 (question q1 )
 (= (test_question_position q1 ) 1)
;;For question 1 there is a direct ask for answer
 (is_direct_question q1)
 
; (is_question_start first q1_s1 q1)
; (after_pause q1_s1 pause_0sg)
; (is_question_end first q1_e1 q1)
; (after_pause q1_e1 pause_1sg)
 (is_question_transition first q1_t q1)
 (after_pause q1_t pause_6sg)
;; (is_restore_label q1_a1 q1)
;; (has_restore_label q1)
 
 ;(current_configuration_question_start q1 q1_s1)
 ;(current_configuration_question_end q1 q1_e1)

 (= (number_options q1) 0)
 (= (current_option q1) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q1) 1)
 (= (number_attempts_answer q1) 3)
 (= (consecutive_failed_alternative q1) 20) ;; FJGP
 
 (is_ask_answer  q1_a1 q1)
 (max_dur q1_a1 dur_7sg)
 (= (question_ask_answer_position q1_a1) 1)
 ; (= (current_number_repetitions q1_a1) 0)
 (= (number_repetitions q1_a1) 0)

 (is_ask_answer  q1_a2 q1)
 (max_dur q1_a2 dur_5sg)
 (= (question_ask_answer_position q1_a2) 2)
; (= (current_number_repetitions q1_a2) 0)
 (= (number_repetitions q1_a2) 0)

 (is_ask_answer  q1_a3 q1)
 (max_dur q1_a3 dur_60sg)
 (= (question_ask_answer_position q1_a3) 3)
; (= (current_number_repetitions q1_a3) 0)
 (= (number_repetitions q1_a3) 0)

 (is_question_repetition q_r q1)
 (is_question_modification q_m q1)
 (is_question_repetition_modification q_srm q1)

 ;;----------------------------------------------------------------------------------------
 ;; question 2
 ;;----------------------------------------------------------------------------------------
 (question q2 )
 (= (test_question_position q2 ) 2)

;;For question 2 there is a direct ask for answer
;; (is_question_start first q2_s1 q2)
;; (after_pause q2_s1 pause_0sg)
;; (is_question_end first q2_e1 q2)
;; (after_pause q2_e1 pause_0sg)
 (is_question_transition first q2_t q2)
 (after_pause q2_t pause_6sg)

 (is_direct_question q2)
 
 ;;(current_configuration_question_start q2 q2_s1)
 ;;(current_configuration_question_end q2 q2_e1)

 (= (number_options q2) 0)
 (= (current_option q2) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q2) 1)
 (= (number_attempts_answer q2) 3)
 (= (consecutive_failed_alternative q2) 20)
 
 (is_ask_answer  q2_a1 q2)
 (max_dur q2_a1 dur_7sg)
 (= (question_ask_answer_position q2_a1) 1)
; (= (current_number_repetitions q2_a1) 0)
 (= (number_repetitions q2_a1) 0)

 (is_ask_answer  q2_a2 q2)
 (max_dur q2_a2 dur_5sg)
 (= (question_ask_answer_position q2_a2) 2)
; (= (current_number_repetitions q2_a2) 0)
 (= (number_repetitions q2_a2) 0)

 (is_ask_answer  q2_a3 q2)
 (max_dur q2_a3 dur_60sg)
 (= (question_ask_answer_position q2_a3) 3)
; (= (current_number_repetitions q2_a3) 0)
 (= (number_repetitions q2_a3) 0)

 (is_question_repetition q_r q2)
 (is_question_modification q_m q2)
 (is_question_repetition_modification q_srm q2)

 ;;----------------------------------------------------------------------------------------
 ;; question 3
 ;;----------------------------------------------------------------------------------------
 (question q3 )
 (= (test_question_position q3 ) 3)
 (is_direct_question q3)
 
;;For this question there is a direct ask for answer but we can leave it like this
;; (is_question_start first q3_s1 q3)
;; (after_pause q3_s1 pause_0sg)
;; (is_question_end first q3_e1 q3)
;; (after_pause q3_e1 pause_0sg)
 (is_question_transition first q3_t q3)
 (after_pause q3_t pause_6sg)
 
 ;;(current_configuration_question_start q3 q3_s1)
 ;;(current_configuration_question_end q3 q3_e1)

 (= (number_options q3) 0)
 (= (current_option q3) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q3) 1)
 (= (number_attempts_answer q3) 3)
  (= (consecutive_failed_alternative q3) 20)
 
 (is_ask_answer  q3_a1 q3)
 (max_dur q3_a1 dur_7sg)
 (= (question_ask_answer_position q3_a1) 1)
; (= (current_number_repetitions q3_a1) 0)
 (= (number_repetitions q3_a1) 0)

 (is_ask_answer  q3_a2 q3)
 (max_dur q3_a2 dur_5sg)
 (= (question_ask_answer_position q3_a2) 2)
; (= (current_number_repetitions q3_a2) 0)
 (= (number_repetitions q3_a2) 0)

 (is_ask_answer  q3_a3 q3)
 (max_dur q3_a3 dur_60sg)
 (= (question_ask_answer_position q3_a3) 3)
; (= (current_number_repetitions q3_a3) 0)
 (= (number_repetitions q3_a3) 0)

 (is_question_repetition q_r q3)
 (is_question_modification q_m q3)
 (is_question_repetition_modification q_srm q3)

 ;;----------------------------------------------------------------------------------------
 ;; question 4
 ;;----------------------------------------------------------------------------------------
 (question q4 )
 (= (test_question_position q4 ) 4)
 (is_direct_question q4)
;;For this question there is a direct ask for answer but we can leave it like this
;; (is_question_start first q4_s1 q4)
;; (after_pause q4_s1 pause_0sg)
;; (is_question_end first q4_e1 q4)
;; (after_pause q4_e1 pause_0sg)
 (is_question_transition first q4_t q4)
 (after_pause q4_t pause_6sg)
 
 ;;(current_configuration_question_start q4 q4_s1)
 ;;(current_configuration_question_end q4 q4_e1)

 (= (number_options q4) 0)
 (= (current_option q4) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q4) 1)
 (= (number_attempts_answer q4) 3)
  (= (consecutive_failed_alternative q4) 20)
 
 (is_ask_answer  q4_a1 q4)
 (max_dur q4_a1 dur_7sg)
 (= (question_ask_answer_position q4_a1) 1)
; (= (current_number_repetitions q4_a1) 0)
 (= (number_repetitions q4_a1) 0)

 (is_ask_answer  q4_a2 q4)
 (max_dur q4_a2 dur_5sg)
 (= (question_ask_answer_position q4_a2) 2)
; (= (current_number_repetitions q4_a2) 0)
 (= (number_repetitions q4_a2) 0)

 (is_ask_answer  q4_a3 q4)
 (max_dur q4_a3 dur_60sg)
 (= (question_ask_answer_position q4_a3) 3)
; (= (current_number_repetitions q4_a3) 0)
 (= (number_repetitions q4_a3) 0)
 
 (is_question_repetition q_r q4)
 (is_question_modification q_m q4)
 (is_question_repetition_modification q_srm q4)

 ;;----------------------------------------------------------------------------------------
 ;; question 5
 ;;----------------------------------------------------------------------------------------
 (question q5 )
 (= (test_question_position q5 ) 5)
 (is_direct_question q5)
;;For this question there is a direct ask for answer but we can leave it like this
;; (is_question_start first q5_s1 q5)
;; (after_pause q5_s1 pause_0sg)
;; (is_question_end first q5_e1 q5)
;; (after_pause q5_e1 pause_0sg)
 (is_question_transition first q5_t q5)
 (after_pause q5_t pause_6sg)
 
 ;;(current_configuration_question_start q5 q5_s1)
 ;;(current_configuration_question_end q5 q5_e1)

 (= (number_options q5) 0)
 (= (current_option q5) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q5) 1)
 (= (number_attempts_answer q5) 3)
  (= (consecutive_failed_alternative q5) 20)
 
 (is_ask_answer  q5_a1 q5)
 (max_dur q5_a1 dur_7sg)
 (= (question_ask_answer_position q5_a1) 1)
; (= (current_number_repetitions q5_a1) 0)
 (= (number_repetitions q5_a1) 0)

 (is_ask_answer  q5_a2 q5)
 (max_dur q5_a2 dur_5sg)
 (= (question_ask_answer_position q5_a2) 2)
; (= (current_number_repetitions q5_a2) 0)
 (= (number_repetitions q5_a2) 0)

 (is_ask_answer  q5_a3 q5)
 (max_dur q5_a3 dur_60sg)
 (= (question_ask_answer_position q5_a3) 3)
; (= (current_number_repetitions q5_a3) 0)
 (= (number_repetitions q5_a3) 0)
 
 (is_question_repetition q_r q5)
 (is_question_modification q_m q5)
 (is_question_repetition_modification q_srm q5)

 ;;----------------------------------------------------------------------------------------
 ;; question 6
 ;;----------------------------------------------------------------------------------------
 (question q6 )
 (= (test_question_position q6 ) 6)
 (is_direct_question q6) 

 ; (is_question_start first q6_s1 q6)
 ; (after_pause q6_s1 pause_1sg)
 ; (is_question_end first q6_e1 q6)
 ; (after_pause q6_e1 pause_0sg)
 (is_question_transition first q6_t q6)
 (after_pause q6_t pause_6sg)
 
 ;;(current_configuration_question_start q6 q6_s1)
 ;;(current_configuration_question_end q6 q6_e1)

 (= (number_options q6) 0)
 (= (current_option q6) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q6) 1)
 (= (number_attempts_answer q6) 3)
 (= (consecutive_failed_alternative q6) 20)
 
 (is_ask_answer  q6_a1 q6)
 (max_dur q6_a1 dur_7sg)
 (= (question_ask_answer_position q6_a1) 1)
; (= (current_number_repetitions q6_a1) 0)
 (= (number_repetitions q6_a1) 0)

 (is_ask_answer  q6_a2 q6)
 (max_dur q6_a2 dur_5sg)
 (= (question_ask_answer_position q6_a2) 2)
; (= (current_number_repetitions q6_a2) 0)
 (= (number_repetitions q6_a2) 0)

 (is_ask_answer  q6_a3 q6)
 (max_dur q6_a3 dur_60sg)
 (= (question_ask_answer_position q6_a3) 3)
; (= (current_number_repetitions q6_a3) 0)
 (= (number_repetitions q6_a3) 0)

 ;This is a hint, so no order is given
 (is_ask_answer  q6_h1 q6)
 (max_dur q6_h1 dur_7sg)
 (is_hint q6_h1)
; (= (current_number_repetitions q6_h1) 0)
 (= (number_repetitions q6_h1) 0)

 (is_question_repetition q_r q6)
 (is_question_modification q_m q6)
 (is_question_repetition_modification q_srm q6)
 
 ;;----------------------------------------------------------------------------------------
 ;; question 7
 ;;----------------------------------------------------------------------------------------
 (question q7 )
 (= (test_question_position q7 ) 7)
 (is_direct_question q7)

;;For this question there is a direct ask for answer but we can leave it like this
;; (is_question_start first q7_s1 q7)
;; (after_pause q7_s1 pause_0sg)
;; (is_question_end first q7_e1 q7)
;; (after_pause q7_e1 pause_0sg)
 (is_question_transition first q7_t q7)
 (after_pause q7_t pause_6sg)
 
 ;;(current_configuration_question_start q7 q7_s1)
 ;;(current_configuration_question_end q7 q7_e1)

 (= (number_options q7) 0)
 (= (current_option q7) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q7) 1)
 (= (number_attempts_answer q7) 3)
 (= (consecutive_failed_alternative q7) 20) ;; FJGP
 
 (is_ask_answer  q7_a1 q7)
 (max_dur q7_a1 dur_7sg)
 (= (question_ask_answer_position q7_a1) 1)
; (= (current_number_repetitions q7_a1) 0)
 (= (number_repetitions q7_a1) 0)

 (is_ask_answer  q7_a2 q7)
 (max_dur q7_a2 dur_5sg)
 (= (question_ask_answer_position q7_a2) 2)
; (= (current_number_repetitions q7_a2) 0)
 (= (number_repetitions q7_a2) 0)

 (is_ask_answer  q7_a3 q7)
 (max_dur q7_a3 dur_60sg)
 (= (question_ask_answer_position q7_a3) 3)
; (= (current_number_repetitions q7_a3) 0)
 (= (number_repetitions q7_a3) 0)
 
 (is_question_repetition q_r q7)
 (is_question_modification q_m q7)
 (is_question_repetition_modification q_srm q7)

 ;;----------------------------------------------------------------------------------------
 ;; question 8
 ;;----------------------------------------------------------------------------------------
 (question q8 )
 (= (test_question_position q8 ) 8)
 (is_direct_question q8)

;;For this question there is a direct ask for answer but we can leave it like this
;; (is_question_start first q8_s1 q8)
;; (after_pause q8_s1 pause_0sg)
;; (is_question_end first q8_e1 q8)
;; (after_pause q8_e1 pause_0sg)
 (is_question_transition first q8_t q8)
 (after_pause q8_t pause_6sg)
 
 ;;(current_configuration_question_start q8 q8_s1)
 ;;(current_configuration_question_end q8 q8_e1)

 (= (number_options q8) 0)
 (= (current_option q8) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q8) 1)
 (= (number_attempts_answer q8) 3)
 (= (consecutive_failed_alternative q8) 20) ;; FJGP
 
 (is_ask_answer  q8_a1 q8)
 (max_dur q8_a1 dur_7sg)
 (= (question_ask_answer_position q8_a1) 1)
; (= (current_number_repetitions q8_a1) 0)
 (= (number_repetitions q8_a1) 0)

 (is_ask_answer  q8_a2 q8)
 (max_dur q8_a2 dur_5sg)
 (= (question_ask_answer_position q8_a2) 2)
; (= (current_number_repetitions q8_a2) 0)
 (= (number_repetitions q8_a2) 0)

 (is_ask_answer  q8_a3 q8)
 (max_dur q8_a3 dur_60sg)
 (= (question_ask_answer_position q8_a3) 3)
; (= (current_number_repetitions q8_a3) 0)
 (= (number_repetitions q8_a3) 0)
 
 (is_question_repetition q_r q8)
 (is_question_modification q_m q8)
 (is_question_repetition_modification q_srm q8)

 ;;----------------------------------------------------------------------------------------
 ;; question 9
 ;;----------------------------------------------------------------------------------------
 (question q9 )
 (= (test_question_position q9 ) 9)
 (is_direct_question q9)

;;For this question there is a direct ask for answer but we can leave it like this
;; (is_question_start first q9_s1 q9)
;; (after_pause q9_s1 pause_0sg)
;; (is_question_end first q9_e1 q9)
;; (after_pause q9_e1 pause_0sg)
 (is_question_transition first q9_t q9)
 (after_pause q9_t pause_6sg)
 
 ;;(current_configuration_question_start q9 q9_s1)
 ;;(current_configuration_question_end q9 q9_e1)

 (= (number_options q9) 0)
 (= (current_option q9) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q9) 1)
 (= (number_attempts_answer q9) 3)
 (= (consecutive_failed_alternative q9) 20) ;; FJGP
 
 (is_ask_answer  q9_a1 q9)
 (max_dur q9_a1 dur_7sg)
 (= (question_ask_answer_position q9_a1) 1)
; (= (current_number_repetitions q9_a1) 0)
 (= (number_repetitions q9_a1) 0)

 (is_ask_answer  q9_a2 q9)
 (max_dur q9_a2 dur_5sg)
 (= (question_ask_answer_position q9_a2) 2)
; (= (current_number_repetitions q9_a2) 0)
 (= (number_repetitions q9_a2) 0)

 (is_ask_answer  q9_a3 q9)
 (max_dur q9_a3 dur_60sg)
 (= (question_ask_answer_position q9_a3) 3)
; (= (current_number_repetitions q9_a3) 0)
 (= (number_repetitions q9_a3) 0)

 (is_question_repetition q_r q9)
 (is_question_modification q_m q9)
 (is_question_repetition_modification q_srm q9)

 ;;----------------------------------------------------------------------------------------
 ;; question 10
 ;;----------------------------------------------------------------------------------------
 (question q10 )
 (= (test_question_position q10 ) 10)
 (is_direct_question q10)

;;For this question there is a direct ask for answer but we can leave it like this
;; (is_question_start first q10_s1 q10)
;; (after_pause q10_s1 pause_0sg)
;; (is_question_end first q10_e1 q10)
;; (after_pause q10_e1 pause_0sg)
 (is_question_transition first q10_t q10)
 (after_pause q10_t pause_6sg)
 
 ;;(current_configuration_question_start q10 q10_s1)
 ;;(current_configuration_question_end q10 q10_e1)

 (= (number_options q10) 0)
 (= (current_option q10) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q10) 1)
 (= (number_attempts_answer q10) 3)
 (= (consecutive_failed_alternative q10) 20) ;; FJGP
 
 (is_ask_answer  q10_a1 q10)
 (max_dur q10_a1 dur_7sg)
 (= (question_ask_answer_position q10_a1) 1)
; (= (current_number_repetitions q10_a1) 0)
 (= (number_repetitions q10_a1) 0)

 (is_ask_answer  q10_a2 q10)
 (max_dur q10_a2 dur_5sg)
 (= (question_ask_answer_position q10_a2) 2)
; (= (current_number_repetitions q10_a2) 0)
 (= (number_repetitions q10_a2) 0)

 (is_ask_answer  q10_a3 q10)
 (max_dur q10_a3 dur_60sg)
 (= (question_ask_answer_position q10_a3) 3)
; (= (current_number_repetitions q10_a3) 0)
 (= (number_repetitions q10_a3) 0)

 (is_question_repetition q_r q10)
 (is_question_modification q_m q10)
 (is_question_repetition_modification q_srm q10)

 ;;----------------------------------------------------------------------------------------
 ;; question 11
 ;;----------------------------------------------------------------------------------------
 (question q11 )
 (= (test_question_position q11 ) 11)
 (cannot_be_repeated q11)
 
 (is_question_start first q11_s1 q11)
 (after_pause q11_s1 pause_1sg)
 (is_question_end first q11_e1 q11)
 (after_pause q11_e1 pause_1sg)
 (is_question_transition first q11_t q11)
 (after_pause q11_t pause_6sg)
 
 ;;(current_configuration_question_start q11 q11_s1)
 ;;(current_configuration_question_end q11 q11_e1)

 (= (number_options q11) 0)
 (= (current_option q11) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q11) 1)
 (= (number_attempts_answer q11) 1)
 (= (consecutive_failed_alternative q11) 20) ;; FJGP
 
 (is_ask_answer  q11_a1 q11)
 (max_dur q11_a1 dur_30sg)
 (= (question_ask_answer_position q11_a1) 1)
; (= (current_number_repetitions q11_a1) 0)
 (= (number_repetitions q11_a1) 0)

 (is_question_repetition q_r q11)
 (is_question_modification q_m q11)
 (is_question_repetition_modification q_sm q11)

 ;;----------------------------------------------------------------------------------------
 ;; question 12
 ;;----------------------------------------------------------------------------------------
 (question q12 )
 (= (test_question_position q12 ) 12)
 (is_direct_question q12)
 (cannot_be_repeated q12)
 
;; (is_question_start first q12_s1 q12)
;; (after_pause q12_s1 pause_0sg)
;; (is_question_end first q12_e1 q12)
;; (after_pause q12_e1 pause_0sg)
 (is_question_transition first q12_t q12)
 (after_pause q12_t pause_6sg)
 
 ;;(current_configuration_question_start q12 q12_s1)
 ;;(current_configuration_question_end q12 q12_e1)

 (= (number_options q12) 0)
 (= (current_option q12) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q12) 1)
 (= (number_attempts_answer q12) 2)
 (= (consecutive_failed_alternative q12) 20) ;; FJGP
 
 (is_ask_answer  q12_a1 q12)
 (validate_answer q12 q12_a1)
 (max_dur q12_a1 dur_7sg)
 (= (question_ask_answer_position q12_a1) 1)
 (= (number_repetitions q12_a1) 0)


 (is_ask_answer  q12_a2 q12)
 ;;(validate_answer q12 q12_a2)
 (max_dur q12_a2 dur_7sg)
 (= (question_ask_answer_position q12_a2) 2)
; (= (current_number_repetitions q12_a2) 0)
 (= (number_repetitions q12_a2) 5)

; (is_ask_answer  q12_a3 q12)
; (validate_answer q12 q12_a3)
; (max_dur q12_a3 dur_7sg)
; (= (question_ask_answer_position q12_a3) 3)

; (is_ask_answer  q12_a4 q12)
; (validate_answer q12 q12_a4)
; (max_dur q12_a4 dur_7sg)
; (= (question_ask_answer_position q12_a4) 4)

; (is_ask_answer  q12_a5 q12)
; (validate_answer q12 q12_a5) 
; (max_dur q12_a5 dur_7sg)
; (= (question_ask_answer_position q12_a5) 5)

; (is_ask_answer  q12_a6 q12)
; (max_dur q12_a6 dur_7sg)
; (= (question_ask_answer_position q12_a6) 6)

 (is_question_repetition q_r q12)
 (is_question_modification q_m q12)
 (is_question_repetition_modification q_sm q12)

 ;;----------------------------------------------------------------------------------------
 ;; question 13, first substraction
 ;;----------------------------------------------------------------------------------------
 (question q13 )
 (= (test_question_position q13 ) 13)
 (skip_next q13)
 ;;(skipped q13)
 
 (is_question_start first q13_s1 q13)
 (after_pause q13_s1 pause_1sg)
 (is_question_end first q13_s3 q13)
 (after_pause q13_s3 pause_1sg)
 (is_question_transition first q13_t q13)
 (after_pause q13_t pause_6sg)
 
 ;;(current_configuration_question_start q13 q13_s1)
 ;;(current_configuration_question_end q13 q13_e1)

;; options
;; master labels for options
 (is_question_option first q13_s2 q13)
 (after_pause q13_s2 pause_1sg)
 ; (is_question_option first q13_s3 q13)
 ; (after_pause q13_s3 pause_1sg)

 (= (number_options q13) 1)
 (= (current_option q13) 1)
 
 (= (question_option_position q13_s2) 1)
; (= (question_option_position q13_s3) 2)

;; (current_options_configuration q13 first)
 ;;(is_option_configuration  first q13_s2 q13_s2)
 ;;(is_option_configuration  first q13_s3 q13_s3)

 ;; ask_answer
 (= (current_attempt_answer q13) 1)
 (= (number_attempts_answer q13) 3)
 (= (consecutive_failed_alternative q13) 20) ;; FJGP
 
 (is_ask_answer  q13_a1 q13)
 (max_dur q13_a1 dur_7sg)
 (= (question_ask_answer_position q13_a1) 1)
; (= (current_number_repetitions q13_a1) 0)
 (= (number_repetitions q13_a1) 0)

 (is_ask_answer  q13_a2 q13)
 (max_dur q13_a2 dur_5sg)
 (= (question_ask_answer_position q13_a2) 2)
; (= (current_number_repetitions q13_a2) 0)
 (= (number_repetitions q13_a2) 0)

 (is_ask_answer  q13_a3 q13)
 (max_dur q13_a3 dur_60sg)
 (= (question_ask_answer_position q13_a3) 3)
; (= (current_number_repetitions q13_a3) 0)
 (= (number_repetitions q13_a3) 0)

 (is_question_repetition q_r q13)
 (is_question_modification q_m q13)
 (is_question_repetition_modification q_srm q13)

 ;;----------------------------------------------------------------------------------------
 ;; question 13, second substraction
 ;;----------------------------------------------------------------------------------------
 (question q13_1 )
 (= (test_question_position q13_1 ) 14)
 (skip_next q13_1)
 (is_direct_question q13_1)
  

;; (is_question_start first q13_1_s1 q13_1)
;; (after_pause q13_1_s1 pause_0sg)
;; (is_question_end first q13_1_e1 q13_1)
;; (after_pause q13_1_e1 pause_0sg)
 (is_question_transition first q13_1_t q13_1)
 (after_pause q13_1_t pause_0sg)
 
 ;;(current_configuration_question_start q13_1 q13_1_s1)
 ;;(current_configuration_question_end q13_1 q13_1_e1)

;; options
 (= (number_options q13_1) 0)
 (= (current_option q13_1) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q13_1) 1)
 (= (number_attempts_answer q13_1) 3)
 (= (consecutive_failed_alternative q13_1) 20) ;; FJGP
 
 (is_ask_answer  q13_1_a1 q13_1)
 (max_dur q13_1_a1 dur_7sg)
 (= (question_ask_answer_position q13_1_a1) 1)
; (= (current_number_repetitions q13_1_a1) 0)
 (= (number_repetitions q13_1_a1) 0)

 (is_ask_answer  q13_1_a2 q13_1)
 (max_dur q13_1_a2 dur_5sg)
 (= (question_ask_answer_position q13_1_a2) 2)
; (= (current_number_repetitions q13_1_a2) 0)
 (= (number_repetitions q13_1_a2) 0)

 (is_ask_answer  q13_1_a3 q13_1)
 (max_dur q13_1_a3 dur_60sg)
 (= (question_ask_answer_position q13_1_a3) 3)
; (= (current_number_repetitions q13_1_a3) 0)
 (= (number_repetitions q13_1_a3) 0)

 (is_question_repetition q_r q13_1)
 (is_question_modification q_m q13_1)
 (is_question_repetition_modification q_srm q13_1)

 ;;----------------------------------------------------------------------------------------
 ;; question 13, third substraction
 ;;----------------------------------------------------------------------------------------
 (question q13_2 )
 (= (test_question_position q13_2 ) 15)
 (skip_next q13_2)
  (is_direct_question q13_2)

;; (is_question_start first q13_2_s1 q13_2)
;; (after_pause q13_2_s1 pause_0sg)
;; (is_question_end first q13_2_e1 q13_2)
;; (after_pause q13_2_e1 pause_0sg)
 (is_question_transition first q13_2_t q13_2)
 (after_pause q13_2_t pause_0sg)
 
 ;;(current_configuration_question_start q13_2 q13_2_s1)
 ;;(current_configuration_question_end q13_2 q13_2_e1)

;; options
 (= (number_options q13_2) 0)
 (= (current_option q13_2) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q13_2) 1)
 (= (number_attempts_answer q13_2) 3)
 (= (consecutive_failed_alternative q13_2) 20) ;; FJGP
 
 (is_ask_answer  q13_2_a1 q13_2)
 (max_dur q13_2_a1 dur_7sg)
 (= (question_ask_answer_position q13_2_a1) 1)
; (= (current_number_repetitions q13_2_a1) 0)
 (= (number_repetitions q13_2_a1) 0)

 (is_ask_answer  q13_2_a2 q13_2)
 (max_dur q13_2_a2 dur_5sg)
 (= (question_ask_answer_position q13_2_a2) 2)
; (= (current_number_repetitions q13_2_a2) 0)
 (= (number_repetitions q13_2_a2) 0)

 (is_ask_answer  q13_2_a3 q13_2)
 (max_dur q13_2_a3 dur_60sg)
 (= (question_ask_answer_position q13_2_a3) 3)
; (= (current_number_repetitions q13_2_a3) 0)
 (= (number_repetitions q13_2_a3) 0)

 (is_question_repetition q_r q13_2)
 (is_question_modification q_m q13_2)
 (is_question_repetition_modification q_srm q13_2)

 ;;----------------------------------------------------------------------------------------
 ;; question 13, fourth substraction
 ;;----------------------------------------------------------------------------------------
 (question q13_3 )
 (= (test_question_position q13_3 ) 16)
 (skip_next q13_3)
 (is_direct_question q13_3)

;; (is_question_start first q13_3_s1 q13_3)
;; (after_pause q13_3_s1 pause_0sg)
;; (is_question_end first q13_3_e1 q13_3)
;; (after_pause q13_3_e1 pause_0sg)
 (is_question_transition first q13_3_t q13_3)
 (after_pause q13_3_t pause_0sg)
 
 ;;(current_configuration_question_start q13_3 q13_3_s1)
 ;;(current_configuration_question_end q13_3 q13_3_e1)

;; options
 (= (number_options q13_3) 0)
 (= (current_option q13_3) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q13_3) 1)
 (= (number_attempts_answer q13_3) 3)
 (= (consecutive_failed_alternative q13_3) 20) ;; FJGP
 
 (is_ask_answer  q13_3_a1 q13_3)
 (max_dur q13_3_a1 dur_7sg)
 (= (question_ask_answer_position q13_3_a1) 1)
; (= (current_number_repetitions q13_3_a1) 0)
 (= (number_repetitions q13_3_a1) 0)

 (is_ask_answer  q13_3_a2 q13_3)
 (max_dur q13_3_a2 dur_5sg)
 (= (question_ask_answer_position q13_3_a2) 2)
; (= (current_number_repetitions q13_3_a2) 0)
 (= (number_repetitions q13_3_a2) 0)

 (is_ask_answer  q13_3_a3 q13_3)
 (max_dur q13_3_a3 dur_60sg)
 (= (question_ask_answer_position q13_3_a3) 3)
; (= (current_number_repetitions q13_3_a3) 0)
 (= (number_repetitions q13_3_a3) 0)

 (is_question_repetition q_r q13_3)
 (is_question_modification q_m q13_3)
 (is_question_repetition_modification q_srm q13_3)

 ;;----------------------------------------------------------------------------------------
 ;; question 13, fifth substraction
 ;;----------------------------------------------------------------------------------------
 (question q13_4 )
 (= (test_question_position q13_4 ) 17)
 (is_direct_question q13_4)
 (skip_next q13_4)

 ;;(is_question_start first q13_4_s1 q13_4)
 ;;(after_pause q13_4_s1 pause_0sg)
 ;;(is_question_end first q13_4_e1 q13_4)
 ;;(after_pause q13_4_e1 pause_0sg)
 (is_question_transition first q13_4_t q13_4)
 (after_pause q13_4_t pause_6sg)
 
 ;;(current_configuration_question_start q13_4 q13_4_s1)
 ;;(current_configuration_question_end q13_4 q13_4_e1)

;; options
 (= (number_options q13_4) 0)
 (= (current_option q13_4) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q13_4) 1)
 (= (number_attempts_answer q13_4) 3)
 (= (consecutive_failed_alternative q13_4) 20) ;; FJGP
 
 (is_ask_answer  q13_4_a1 q13_4)
 (max_dur q13_4_a1 dur_7sg)
 (= (question_ask_answer_position q13_4_a1) 1)
; (= (current_number_repetitions q13_4_a1) 0)
 (= (number_repetitions q13_4_a1) 0)

 (is_ask_answer  q13_4_a2 q13_4)
 (max_dur q13_4_a2 dur_5sg)
 (= (question_ask_answer_position q13_4_a2) 2)
 (= (number_repetitions q13_4_a2) 0)

 (is_ask_answer  q13_4_a3 q13_4)
 (max_dur q13_4_a3 dur_60sg)
 (= (question_ask_answer_position q13_4_a3) 3)
 (= (number_repetitions q13_4_a3) 0)

 (is_question_repetition q_r q13_4)
 (is_question_modification q_m q13_4)
 (is_question_repetition_modification q_srm q13_4)

 ;;----------------------------------------------------------------------------------------
 ;; question 13_5 alternative
 ;;----------------------------------------------------------------------------------------
 (question q13_5 )
 (is_alternative_question q13_5)
 (= (test_question_position q13_5 ) 18)
 (skip_next q13_5)

 (is_question_start first q13_5_s1 q13_5)
 (after_pause q13_5_s1 pause_0sg)
 (is_question_end first q13_5_e1 q13_5)
 (after_pause q13_5_e1 pause_1sg)
 (is_question_transition first q13_5_t q13_5)
 (after_pause q13_5_t pause_6sg)

 (= (number_options q13_5) 0)
 (= (current_option q13_5) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q13_5) 1)
 (= (number_attempts_answer q13_5) 1)
 (= (consecutive_failed_alternative q13_5) 4)
 
 (is_ask_answer  q13_5_a1 q13_5)
 (max_dur q13_5_a1 dur_7sg)
 (= (question_ask_answer_position q13_5_a1) 1)
 (= (number_repetitions q13_5_a1) 0)

 (is_question_repetition q_r q13_5)
 (is_question_modification q_m q13_5)
 (is_question_repetition_modification q_srm q13_5)

 ;;----------------------------------------------------------------------------------------
 ;; question 14
 ;;----------------------------------------------------------------------------------------
 (question q14 )
 (= (test_question_position q14 ) 19)
 (validate_answer q14 q14_a1)	;; FJGP
 (validate_answer q14 q14_a2) ;; FJGP

 (is_question_start first q14_s1 q14)
 (after_pause q14_s1 pause_4sg)
 (is_question_end first q14_e1 q14)
 (after_pause q14_e1 pause_0sg)
 (is_question_transition first q14_t q14)
 (after_pause q14_t pause_6sg)
 
 ;;(current_configuration_question_start q14 q14_s1)
 ;;(current_configuration_question_end q14 q14_e1)

 (= (number_options q14) 0)
 (= (current_option q14) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q14) 1)
 (= (number_attempts_answer q14) 3)
 (= (consecutive_failed_alternative q14) 20) ;; FJGP
 
 (is_ask_answer  q14_a1 q14)
 (max_dur q14_a1 dur_10sg)
 (= (question_ask_answer_position q14_a1) 1)
 (= (number_repetitions q14_a1) 0)

 (is_ask_answer  q14_a2 q14)
 (max_dur q14_a2 dur_10sg)
 (= (question_ask_answer_position q14_a2) 2)
   (= (number_repetitions q14_a2) 0)

 (is_ask_answer  q14_a3 q14)
 (max_dur q14_a3 dur_60sg)
 (= (question_ask_answer_position q14_a3) 3)
   (= (number_repetitions q14_a3) 0)

 (is_question_repetition q_r q14)
 (is_question_modification q_m q14)
 (is_question_repetition_modification q_srm q14)

 ;;----------------------------------------------------------------------------------------
 ;; question 15
;;----------------------------------------------------------------------------------------
 (question q15)
 (= (test_question_position q15 ) 20)
 (validate_answer q15 q15_a1)
 (validate_answer q15 q15_a2)

 (is_question_start first q15_s1 q15)
 (after_pause q15_s1 pause_1sg)
 (is_question_end first q15_e1 q15)
 (after_pause q15_e1 pause_4sg)
 (is_question_transition first q15_t q15)
 (after_pause q15_t pause_6sg)
 
 ;;(current_configuration_question_start q15 q15_s1)
 ;;(current_configuration_question_end q15 q15_e1)

;; options
;; master labels for options
 (is_question_option first q15_s2 q15)
 (after_pause q15_s2 pause_1sg)

 (= (number_options q15) 1)
 (= (current_option q15) 1)
 
 (= (question_option_position q15_s2) 1)

;; (current_options_configuration q15 first)
 ;;(is_option_configuration  first q15_s2 q15_s2)

 ;; ask_answer
 (= (current_attempt_answer q15) 1)
 (= (number_attempts_answer q15) 3)
 (= (consecutive_failed_alternative q15) 20) ;; FJGP
 
 (is_ask_answer  q15_a1 q15)
 (max_dur q15_a1 dur_5sg)
 (= (question_ask_answer_position q15_a1) 1)
 (= (number_repetitions q15_a1) 0)

 (is_ask_answer  q15_a2 q15)
 (max_dur q15_a2 dur_4sg)
 (= (question_ask_answer_position q15_a2) 2)
 (= (number_repetitions q15_a2) 0)
 
 (is_ask_answer  q15_a3 q15)
 (max_dur q15_a3 dur_60sg)
 (= (question_ask_answer_position q15_a3) 3)
 (= (number_repetitions q15_a3) 0)
 
 (is_question_repetition q_r q15)
 (is_question_modification q_m q15)
 (is_question_repetition_modification q_srm q15)
 
 ;;----------------------------------------------------------------------------------------
 ;; question 16
;;----------------------------------------------------------------------------------------
 (question q16)
 (= (test_question_position q16 ) 21)
 (validate_answer q16 q16_a1)
 (validate_answer q16 q16_a2)

 (is_question_start first q16_s1 q16)
 (after_pause q16_s1 pause_1sg)
 (is_question_end first q16_e1 q16)
 (after_pause q16_e1 pause_4sg)
 (is_question_transition first q16_t q16)
 (after_pause q16_t pause_6sg)
 
 ;;(current_configuration_question_start q16 q16_s1)
 ;;(current_configuration_question_end q16 q16_e1)

 (= (number_options q16) 0)
 (= (current_option q16) 1)
 

 ;; ask_answer
 (= (current_attempt_answer q16) 1)
 (= (number_attempts_answer q16) 3)
 (= (consecutive_failed_alternative q16) 20) ;; FJGP
 
 (is_ask_answer  q16_a1 q16)
 (max_dur q16_a1 dur_5sg)
 (= (question_ask_answer_position q16_a1) 1)
 (= (number_repetitions q16_a1) 0)
 
 (is_ask_answer  q16_a2 q16)
 (max_dur q16_a2 dur_4sg)
 (= (question_ask_answer_position q16_a2) 2)
 (= (number_repetitions q16_a2) 0)

 (is_ask_answer  q16_a3 q16)
 (max_dur q16_a3 dur_60sg)
 (= (question_ask_answer_position q16_a3) 3)
 (= (number_repetitions q16_a3) 0)

 (is_question_repetition q_r q16)
 (is_question_modification q_m q16)
 (is_question_repetition_modification q_srm q16)
 
  ;;----------------------------------------------------------------------------------------
 ;; question 17
 ;;----------------------------------------------------------------------------------------
 (question q17 )
 (= (test_question_position q17 ) 22)

 
 (is_question_start first q17_s1 q17)
 (after_pause q17_s1 pause_1sg)
 (is_question_end first q17_e1 q17)
 (after_pause q17_e1 pause_2sg)
 (is_question_transition first q17_t q17)
 (is_question_transition second q17_t q17)
 (after_pause q17_t pause_6sg)
 
 ;;(current_configuration_question_start q17 q17_s1)
 ;;(current_configuration_question_end q17 q17_e1)

;; options
;; master labels for options
 (is_question_option first q17_s2 q17)
 (after_pause q17_s2 pause_1sg)
 (is_question_option first q17_s3 q17)
 (after_pause q17_s3 pause_2sg)

 (= (number_options q17) 2)
 (= (current_option q17) 1)
 
 (= (question_option_position q17_s2) 1)
 (= (question_option_position q17_s3) 2)

;; (current_options_configuration q17 first)
 ;;(is_option_configuration  first q17_s2 q17_s2)
 ;;(is_option_configuration  first q17_s3 q17_s3)

 ;; ask_answer
 (= (current_attempt_answer q17) 1)
 (= (number_attempts_answer q17) 3)
 (= (consecutive_failed_alternative q17) 20) ;; FJGP
 
 (is_ask_answer  q17_a1 q17)
 (max_dur q17_a1 dur_7sg)
 (= (question_ask_answer_position q17_a1) 1)
 (= (number_repetitions q17_a1) 0)

 (is_ask_answer  q17_s4 q17)
 (max_dur q17_s4 dur_1sg)
 (= (question_ask_answer_position q17_s4) 2)
 (= (number_repetitions q17_s4) 0)
 (implies_reproduction q17_s4 q17)
 ;(reproduction_configuration q17_s4 q17_s5 q17_e2)
 ;(reproduction_configuration_options q17_s4 second)

 (is_question_start second q17_s5 q17)
 (after_pause q17_s5 pause_1sg)
 (is_question_end second q17_e2 q17)
 (after_pause q17_e2 pause_2sg)
 

 ;AGO: fake options to enforce that the reproduction has also two options

 ;;(is_option_configuration second q17_s6 q17_s6)
 ;;(is_option_configuration second q17_s7 q17_s7)
 (is_question_option second q17_s6 q17)
 (is_question_option second q17_s7 q17)
 (after_pause q17_s6 pause_0sg)
 (after_pause q17_s7 pause_0sg)
 (= (question_option_position q17_s6) 1)
 (= (question_option_position q17_s7) 2)

 (is_ask_answer q17_a2 q17)
 (max_dur q17_a2 dur_6sg)
 (= (question_ask_answer_position q17_a2) 3)
 (= (number_repetitions q17_a2) 0)

 (is_question_repetition q_r q17)
 (is_question_modification q_m q17)
 (is_question_repetition_modification q_srm q17)
 
 ;;----------------------------------------------------------------------------------------
 ;; question 18 (check front patient)
;;----------------------------------------------------------------------------------------
 (question q18)
 (= (test_question_position q18 ) 23)
 (tracking_pose q18)
 (tracking q18 detect_posture_sequence)
 (is-sequence detect_posture_sequence)
 (needs_extra_monitoring q18)
 (monitor q18 front_patient)
 (is_restore_label q18_o1 q18)
 (has_restore_label q18 front_patient)


 (is_question_start first q18_s1 q18)
 (after_pause q18_s1 pause_1sg)
 (is_question_end first q18_e1 q18)
 (after_pause q18_e1 pause_1sg)
 (is_question_transition first q18_t q18)
 (after_pause q18_t pause_6sg)
 
 ;;(current_configuration_question_start q18 q18_s1)
 ;;(current_configuration_question_end q18 q18_e1)

 (is_question_option first q18_o1 q18)
 (after_pause q18_o1 pause_6sg)
 (is_question_option first q18_o2 q18)
 (after_pause q18_o2 pause_1sg)
 (= (number_options q18) 2)
 (= (current_option q18) 1)
 (= (question_option_position q18_o1) 1)
 (= (question_option_position q18_o2) 2)

 ;; ask_answer
 (= (current_attempt_answer q18) 1)
 (= (number_attempts_answer q18) 1)
 (= (consecutive_failed_alternative q18) 20) ;; FJGP
 
 (is_ask_answer q18_a1 q18)
 (max_dur q18_a1 dur_10sg)
 (= (question_ask_answer_position q18_a1) 1)
 (= (number_repetitions q18_a1) 0)

 (is_question_repetition q_r q18)
 (is_question_modification q_m q18)
 (is_question_repetition_modification q_srm q18)

;  ;;----------------------------------------------------------------------------------------
;  ;; question 18_1 (get-posture-sequence)
; ;;----------------------------------------------------------------------------------------
;   (question q18_1)
;   (= (test_question_position q18_1 ) 24)
;   (tracking_pose q18_1)
;   (tracking q18_1 detect_posture_sequence)  

;   (is_question_start first q18_1_s1 q18_1)
;   (after_pause q18_1_s1 pause_0sg)
;   (is_question_end first q18_1_e1 q18_1)
;   (after_pause q18_1_e1 pause_0sg)
;   (is_question_transition q18_1_t q18_1)
;   (after_pause q18_1_t pause_0sg)
 
;   ;;(current_configuration_question_start q18_1 q18_1_s1)
;   ;;(current_configuration_question_end q18_1 q18_1_e1)

;   (= (number_options q18_1) 0)
;   (= (current_option q18_1) 1)
 
;   ;; ask_answer
;   (= (current_attempt_answer q18_1) 1)
;   (= (number_attempts_answer q18_1) 1)
;   (= (consecutive_failed_alternative q18_1) 20) ;; FJGP	

;   (is_ask_answer q18_1_a1 q18_1)
;   (max_dur q18_1_a1 dur_10sg)
;   (= (question_ask_answer_position q18_1_a1) 1)
;   (= (number_repetitions q18_1_a1) 0)
 
;  ;;----------------------------------------------------------------------------------------
;  ;; question 18_2 (get-posture-sequence)
; ;;----------------------------------------------------------------------------------------
;   (question q18_2)
;   (= (test_question_position q18_2 ) 25)
;   (tracking_pose q18_2)
;   (tracking q18_2 get_posture_sequence)  
;   (is_direct_question q18_2)
  
; ;;  (is_question_start first q18_2_s1 q18_2)
; ;;  (after_pause q18_2_s1 pause_0sg)
; ;;  (is_question_end first q18_2_e1 q18_2)
; ;;  (after_pause q18_2_e1 pause_0sg)
;   (is_question_transition q18_2_t q18_2)
;   (after_pause q18_2_t pause_2sg)
 
;   ;;(current_configuration_question_start q18_2 q18_2_s1)
;   ;;(current_configuration_question_end q18_2 q18_2_e1)

;   (= (number_options q18_2) 0)
;   (= (current_option q18_2) 1)
 
;   ;; ask_answer
;   (= (current_attempt_answer q18_2) 1)
;   (= (number_attempts_answer q18_2) 1)
;   (= (consecutive_failed_alternative q18_2) 20) ;; FJGP	

;   (is_ask_answer q18_2_a1 q18_2)
;   (max_dur q18_2_a1 dur_1sg)
;   (= (question_ask_answer_position q18_2_a1) 1)
;   (= (number_repetitions q18_2_a1) 0)

  ;;----------------------------------------------------------------------------------------
 ;; question 19
;;----------------------------------------------------------------------------------------
 (question q19)
 (= (test_question_position q19 ) 24)
 (tracking_pose q19)
 (tracking q19 check_eyes)
 (needs_extra_monitoring q19)
 (monitor q19 front_patient)

 (is_question_start first q19_s1 q19)
 (after_pause q19_s1 pause_1sg)
 (is_question_end first q19_e1 q19)
 (after_pause q19_e1 pause_1sg)
 (is_question_transition first q19_t q19)
 (after_pause q19_t pause_6sg)
 
 ;;(current_configuration_question_start q19 q19_s1)
 ;;(current_configuration_question_end q19 q19_e1)

 (= (number_options q19) 0)
 (= (current_option q19) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q19) 1)
 (= (number_attempts_answer q19) 1)
 (= (consecutive_failed_alternative q19) 20) ;; FJGP
 
 (is_ask_answer  q19_a1 q19)
 (max_dur q19_a1 dur_7sg)
 (= (question_ask_answer_position q19_a1) 1)
 (= (number_repetitions q19_a1) 0)

 (is_question_repetition q_r q19)
 (is_question_modification q_m q19)
 (is_question_repetition_modification q_srm q19)

;   ;;----------------------------------------------------------------------------------------
;  ;; question 20
; ;;----------------------------------------------------------------------------------------
 (question q20)
 (= (test_question_position q20 ) 25)
 (tracking_pose q20)
 (tracking q20 handwriting)

 (is_question_start first q20_s1 q20)
 (after_pause q20_s1 pause_1sg)
 (is_question_end first q20_e1 q20)
 (after_pause q20_e1 pause_1sg)
 (is_question_transition first q20_t q20)
 (after_pause q20_t pause_6sg)
 
 ;;(current_configuration_question_start q20 q20_s1)
 ;;(current_configuration_question_end q20 q20_e1)

 (= (number_options q20) 0)
 (= (current_option q20) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q20) 1)
 (= (number_attempts_answer q20) 1)
 (= (consecutive_failed_alternative q20) 20) ;; FJGP
 
 (is_ask_answer  q20_a1 q20)
 (max_dur q20_a1 dur_5min)
 (= (question_ask_answer_position q20_a1) 1)
 (= (number_repetitions q20_a1) 0)

 (is_question_repetition q_r q20)
 (is_question_modification q_m q20)
 (is_question_repetition_modification q_srm q20)
 
 ;;----------------------------------------------------------------------------------------
 ;; question 21
;;----------------------------------------------------------------------------------------
 (question q21)
 (= (test_question_position q21 ) 26)
 (tracking_pose q21)
 (tracking q21 draw)

 (is_question_start first q21_s1 q21)
 (after_pause q21_s1 pause_1sg)
 (is_question_end first q21_e1 q21)
 (after_pause q21_e1 pause_1sg)
 (is_question_transition first q21_t q21)
 (after_pause q21_t pause_0sg)
 
 ;;(current_configuration_question_start q21 q21_s1)
 ;;(current_configuration_question_end q21 q21_e1)

;; options
;; master labels for options
 (is_question_option first q21_s2 q21)
 (after_pause q21_s2 pause_1sg)

 (= (number_options q21) 1)
 (= (current_option q21) 1)
 
 (= (question_option_position q21_s2) 1)

;; (current_options_configuration q21 first)
 ;;(is_option_configuration  first q21_s2 q21_s2)

 ;; ask_answer
 (= (current_attempt_answer q21) 1)
 (= (number_attempts_answer q21) 1)
 (= (consecutive_failed_alternative q21) 20) ;; FJGP
 
 (is_ask_answer  q21_a1 q21)
 (max_dur q21_a1 dur_5min)
 (= (question_ask_answer_position q21_a1) 1)
 (= (number_repetitions q21_a1) 0)

 (is_question_repetition q_r q21)
 (is_question_modification q_m q21)
 (is_question_repetition_modification q_srm q21)

)
(:goal (and
	(test_finished )
	))
)
