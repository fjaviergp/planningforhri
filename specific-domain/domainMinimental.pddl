;; -----------------------------------------------------------------------------------------------------------------
;; DOMAIN clark -- test type closed options and open answers
;;
;; This domain is for controlling the interaction between a robot and a person while performing a test with
;; both closed and open answers.
;;
;; Based on the Barthel & Mini-Mental test for elder people
;;
;; Raquel Fuentetaja February 2016
;; Modified: Angel Garcia Olaya & Francisco Javier Garcia Polo.
;; April 2016. Added hints, alternative questions, skipped questions.
;; May 2016. Added repetitions of ask-for-answer, corrections to the answer and repetitions of a whole question
;; -----------------------------------------------------------------------------------------------------------------

(define (domain clark)
  (:requirements :strips :typing :fluents :negative-preconditions :equality) 
  (:types
   
       question_configuration options_configuration robot patient test test_element time property
       behavior person_type language verbal_tense tracking_item - object
       ;;AGO: No usamos patient en ning�n predicado, aunque de momento lo podemos dejar
       ;;patient_property environment_property robot_property - property
       pause dur - time
       presentation - test_element
       test_introduction - test_element
       test_end - test_element
       question - test_element
       question_option - test_element
       question_start - test_element
       question_end - test_element
       question_ask_answer - test_element
       question_transition - test_element
       question_repetition - test_element
       question_modification - test_element
       question_repetition_modification - test_element
       answer - test_element
      

  )

  (:predicates
      ;; test configuration
      (current_test ?test - test)
      (test_configuration ?language - language ?p - person_type ?t - verbal_tense)
      (test_configured)
   
      (can_continue) ;; from robot control. The test can continue
      (internal_can_continue) ;; from robot control. The test can continue
   
      (robot_presentation ?p - presentation ?robot - robot) ;; there is a robot presentation for
							    ;; ?robot with label ?p AGO: si vemos
							    ;; que nos cuesta mucho encontrar planes
							    ;; podr�amos asumir que es un �nico
							    ;; robot y quitar el par�metro robot.

      ;; Next predicates are redundant. Decide if the specific robot is important in actions?
      ;;(robot_introduced ?robot - robot) ;; the robot ?robot has been
					;; introduced
      (finished_introduce_robot) ;; introduction finished AGO: �no tendr�a que tener como par�metro
				 ;; el robot? �no valdr�a con el de robot_introduced?
      
      (test_finished) 
      
      (is_test_introduction ?ti - test_introduction) ;; there is a test introduction for ?test with label ?ti 
      (is_test_introduction_video ?ti - test_introduction) ;; FJGP
      (introduction_finished ?ti - test_introduction) 

      (question ?question - question) ;; there is a question with label ?question in ?test
      (is_direct_question ?q - question);; there is no intro to this question, the user is asked directly
      (is_alternative_question ?q - question)
      (question_started ?question - question) 
      (question_finished ?q - question)

      ;; the current configuration for question ?q is to start with ?s and finish with ?e
      (is_first_configuration ?c -  question_configuration)
      (current_configuration ?c -  question_configuration)
      (next_configuration ?current -  question_configuration ?next -  question_configuration)
;      (current_configuration_question_start ?q - question ?s - question_start ?c -  question_configuration)
;      (current_configuration_question_end ?q - question ?e - question_end ?c -  question_configuration) 
;     (current_options_configuration ?q - question ?c -  question_configuration);;AGO, not needed anymore, use (current_configuration)
;      (is_option_configuration ?c -  question_configuration ?option - question_option)

     
      (is_question_start ?c -  question_configuration ?s - question_start ?q - question)
      (is_question_end ?c -  question_configuration ?e - question_end ?q - question)
      (is_question_transition ?c -  question_configuration ?t  - question_transition ?q - question)
      (is_question_option ?c -  question_configuration ?qo - question_option ?q - question)
      
 ;     (option ?qo - question_option ?q - question) ;; there is a question option for ?q with label ?qo
      
      (is_ask_answer ?qa - question_ask_answer ?q - question) ;; ?qa represents a way for asking for a question answer
      (is_question_repetition ?qr - question_repetition ?q - question)
      (is_question_modification ?qp - question_modification ?q - question)
      (is_question_repetition_modification ?qrm - question_repetition_modification ?q - question)
      (valid_answer) ;; exogeneous event
      (correct_answer) ;; exogeneous event
      ;(changed_answer ?q - question) ;; exogeneous event
      (implies_reproduction ?qa - question_ask_answer ?q - question) ;; a way of asking for an
								     ;; answer is to reproduce the
								     ;; question

      ;; configuration of the question: start, end, when reproducing the question
;     (reproduction_configuration ?qa - question_ask_answer  ?s - question_start ?e - question_end)
;      (reproduction_configuration_options ?qa - question_ask_answer  ?c -  question_configuration) 

      ;; (reproduced_valid_answer ?q - question) ;; the answer has been reproduced
      (do_question_transition) ;;  make a transition to the next question
      (waiting_response)
      
      ;; pause AGO: removed, they are not used in any action
      ;;(pause ?p - pause)
      ;;(dur ?d - dur)
      
      ;; pause after a test element
      (after_pause ?te - test_element ?p - pause)
      (max_dur ?te - test_element ?d - dur)

      ;; ROBOT CONTROL PREDICATES
      (detected_property ?p - property)
      (behavior_of_property ?p - property ?e - behavior)
      (waiting_restore_from  ?p - property ?e - behavior)
      (waiting_finish ?b - behavior)
      (is_failed_n ?property - property)
      
      ;;skipping questions
      (skipped ?q - question)
      (skip_next ?q - question)

       ;;AGO: whether the answer must be validated or any plausible answer is considered correct
       (validate_answer ?q - question ?qa - question_ask_answer)
       ;hints
       (hint_needed)
       (is_hint ?qa - question_ask_answer)

       ;;AGO: created negative as most questions will allow repetitions and changes
       (not_allowed_repetition ?q - question)
       (not_allowed_changes ?q - question)
       ;;AGO: exogenous events
       (repetition_requested)
       (cannot_be_repeated ?q - question)
       (changes_requested)
       (cannot_be_changed ?q - question)
       (is_restore_label ?rl - test_element ?q - question)
       (has_restore_label ?q - question ?prop - property)

       ;;AGO to track poses
       (tracking_pose ?question - question)
       (tracking ?q - question ?ti - tracking_item)
       (is-sequence ?ti - tracking_item)
       ;;In some questions (ej MMSE 18 and 19) we need to monitor extra items and interrupt if they don't hold
       (needs_extra_monitoring ?q - question)
       (monitor ?q - question ?pro - property)
       
  )
  (:functions
     
     (test_introduction_position ?ti - test_introduction ) ;; relative position of a test
							   ;; introdution
     (number_test_introductions) ;; number of test introduction actions
     (current_introduction) ;; number of executed test introductions

     (test_question_position ?q - question) ;; relative position of a test question
     (number_test_questions ) ;; number of test questions
     (current_question ) ;; number of executed questions

     (question_option_position ?qo - question_option) ;; relative position of an option of a test
						      ;; question
     (number_options ?q - question) ;; number of options
     (current_option ?q - question) ;; number of showed options
    
    ; (seconds ?t - time)  ;; seconds of a pause ;;AGO: removed it was not used in any action and the Exe doesn't need it


     (current_attempt_answer ?q - question) ;; attempts to obtain valid a asnwer
     (number_attempts_answer ?q - question) ;; number of attempts to obtain a valid answer
     (question_ask_answer_position ?qa - question_ask_answer) ;; relative position of an option of a
     				   			      ;; test question	     
     (consecutive_failed_alternative ?q - question) ;; FJGP


     (number_failed_questions)
     (max_failed_questions)
     (number_consecutive_failed_questions) ;; FJGP

     (number_repetitions ?q - question_ask_answer)
     (current_number_repetitions)
 )

 

;; -----------------------------------------------------------------------------------------------------------------
;; TEST CONTROL ACTIONS
;; -----------------------------------------------------------------------------------------------------------------
(:action configure-test
     :parameters    (?test - test ?language - language ?p - person_type ?t - verbal_tense)
     :precondition  (and
		      ;; test control
                      (not (test_configured ))
		      (test_configuration  ?language ?p ?t)
		      (current_test ?test)
		      )
     :effect 	(and
		 (test_configured )
		 )
     
  )



  (:action introduce-robot
     :parameters    (?p - presentation ?robot - robot  ?pause - pause)
     :precondition  (and
		      ;; overall. Robot control
                      (can_continue)
                      (internal_can_continue)

		      ;; test control
		      (test_configured )
		      (robot_presentation ?p ?robot)
        	    ;  (not (robot_introduced ?robot))
                      (after_pause ?p ?pause)
		      (not  (finished_introduce_robot))
		      )
     :effect 	(and
	;	 (robot_introduced ?robot)
                 (finished_introduce_robot)
		 )
     
  )

 
(:action introduce-test
  :parameters 	(?i - test_introduction ?pause - pause)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
		 
         	 ;; test control
		 (test_configured )		 
		 (finished_introduce_robot)
		 (is_test_introduction ?i )
                 (= (current_introduction ) (test_introduction_position ?i ))
		 (after_pause ?i ?pause)
		 )
  :effect 	(and
		 (introduction_finished ?i )
                 (increase (current_introduction ) 1)
		 
		 )
  )

;; FJGP
(:action introduce-test-video
  :parameters 	(?i - test_introduction ?pause - pause)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
		 
         	 ;; test control		 
		 (finished_introduce_robot)
		 (is_test_introduction_video ?i )
                 (= (current_introduction ) (test_introduction_position ?i ))
		 (after_pause ?i ?pause)
		 )
  :effect 	(and
		 (introduction_finished ?i )
                 (increase (current_introduction ) 1)
		 
		 )
  )



(:action start-question
  :parameters 	(?question_start - question_start ?question - question  ?pause - pause ?config - question_configuration)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
                 (> (current_introduction ) (number_test_introductions ))
         	 (test_configured )
 		 (question ?question )
		 (current_configuration ?config)
		 (is_question_start ?config ?question_start ?question)
;		 (current_configuration_question_start ?question ?question_start ?config)
                 (= (current_question ) (test_question_position ?question ))
 		 (<= (current_question ) (number_test_questions ))
 		 (not (question_started ?question ))
		 (after_pause ?question_start ?pause)
		 (not (is_alternative_question ?question))
		 (not (repetition_requested))
		 (not (changes_requested))
		 (not (needs_extra_monitoring ?question))
		 )
		 
  :effect 	(and
; 		 (not (current_configuration_question_start  ?question ?question_start))
		 (not (valid_answer))
		 (question_started ?question )
		 (assign (current_option ?question) 1)
		 )
  )

(:action start-direct-question
  :parameters 	(?question - question ?current - question_configuration ?next - question_configuration)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
                 (> (current_introduction ) (number_test_introductions ))
          	 (test_configured )
 		 (question ?question )
		 (is_direct_question ?question)
                 (= (current_question ) (test_question_position ?question ))
 		 (<= (current_question ) (number_test_questions ))
		 (not (is_alternative_question ?question))
 		 (not (question_started ?question ))
		 (not (repetition_requested))
		 (not (changes_requested))
 		 (not (needs_extra_monitoring ?question))
		 (current_configuration ?current)
		 (next_configuration ?current ?next)
		 )
		 
  :effect 	(and
		 (not (valid_answer))
		 (question_started ?question )
		 (question_finished ?question)
		 (not (current_configuration ?current))
		 (current_configuration ?next)
		 )
  )


 (:action show-question-option
  :parameters 	(;?question_option_label - question_option
		 ?question_option - question_option
		 ?current_configuration - question_configuration
	         ?question - question
		 ?pause - pause)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
		 
         	 ;; test control		    		 
		 (question_started ?question )
                 (= (current_question ) (test_question_position ?question ))
;		 (option ?question_option ?question)
		 (= (current_option ?question) (question_option_position ?question_option))
		 ;(after_pause ?question_option_label ?pause)
		 (after_pause ?question_option ?pause)
		 (current_configuration ?current_configuration)
;                 (is_option_configuration ?current_configuration ?question_option)
		 (is_question_option ?current_configuration ?question_option ?question)
		 
                )
  :effect 	(and
		 (increase (current_option ?question) 1)
;		 (not (is_option_configuration ?options_configuration ?question_option ?question_option))
		 )
  )


(:action finish-question
  :parameters 	(?question_end - question_end ?question - question  ?pause - pause ?current - question_configuration 
  		?next - question_configuration)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (question_started ?question )
                 (= (current_question ) (test_question_position ?question ))		 
 		 (not (question_finished ?question ))
		 (current_configuration ?current)
		 (next_configuration ?current ?next)
;		 (current_configuration_question_end ?question ?question_end ?current)
		 (> (current_option ?question) (number_options ?question))
                 (is_question_end ?current ?question_end ?question)
		 (after_pause ?question_end ?pause)
                )

  :effect 	(and
; 		 (not (current_configuration_question_end  ?question ?question_end))
		 (not (current_configuration ?current))
		 (current_configuration ?next)
		 (question_finished ?question )

		 )
  )

(:action ask-for-answer
  :parameters 	(?question_ask_answer - question_ask_answer ?question - question)
			   ;?pause - pause
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))		 
		 (not (valid_answer))
                 (is_ask_answer ?question_ask_answer ?question)
		 (not (implies_reproduction ?question_ask_answer ?question))
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))	 
;		 (after_pause ?question_ask_answer ?pause)
		 (not (skipped ?question))
		 (not (hint_needed))
                )
  :effect 	(and
;               (increase (current_attempt_answer ?question) 1)
;;		(valid_answer) ;; simulation exogeneous event
		(waiting_response)
     		(assign (current_number_repetitions) 0)

		)
 )


;;AGO: we give three new chances to answer the question
(:action ask-for-answer-hint
  :parameters 	(?question_ask_answer - question_ask_answer ?question - question)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))		 
		 (not (valid_answer))
                 (is_ask_answer ?question_ask_answer ?question)
		 (not (implies_reproduction ?question_ask_answer ?question))
		 (hint_needed)
		 (is_hint ?question_ask_answer)
                )
  :effect 	(and
		(waiting_response)
		(not (hint_needed))
		(assign (current_number_repetitions) 0)
		(assign (current_attempt_answer ?question) 1)
		(not (is_hint ?question_ask_answer));;AGO only one hint per question, even if question is repeated
		)
 )
 
(:action skip-ask-for-answer
  :parameters 	(?question_ask_answer - question_ask_answer ?question - question)
	  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))		 
		 (not (valid_answer))
                 (is_ask_answer ?question_ask_answer ?question)
;		 (not (implies_reproduction ?question_ask_answer ?question))
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))
		 (skipped ?question)
		 (<= (current_attempt_answer ?question) (number_attempts_answer ?question))
                )
  :effect 	(and
                (increase (current_attempt_answer ?question) 1)
		(assign (current_number_repetitions) 0)
;;		(valid_answer) ;; simulation exogeneous event
;;		(waiting_response)

		)
 )

(:action ask-for-answer-reproduce
  :parameters 	(?question_ask_answer - question_ask_answer
                ;?question_start - question_start ?question_end - question_end
		 ;?current_configuration - question_configuration
		 ?question - question)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))		 
		 ;;(not (valid_answer)) ;; FJGP ;;Descomentado AGO
                 (is_ask_answer ?question_ask_answer ?question)
		 (implies_reproduction ?question_ask_answer ?question)
;		 (reproduction_configuration ?question_ask_answer  ?question_start ?question_end)
;		 (reproduction_configuration_options ?question_ask_answer ?options_configuration)
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))
		 (not (skipped ?question))
		 (not (do_question_transition))
;		 (after_pause ?question_ask_answer ?pause)
;		 (is_question_start ?question_start ?question)
;		 (is_question_end ?question_end ?question)
                )
  :effect 	(and
		(not (question_started ?question ))
		(not (question_finished ?question ))
		(not (do_question_transition))
		(assign (current_option ?question) 1)
		(assign (number_consecutive_failed_questions) 0) ;; FJGP
		(assign (current_number_repetitions) 0)
		
;		(current_configuration_question_start ?question ?question_start)
;		(current_configuration_question_end ?question ?question_end)
;		(current_options_configuration ?question ?options_configuration)
		;;(waiting_response)
		;;(not (implies_reproduction ?question_ask_answer ?question))
                (increase (current_attempt_answer ?question) 1)		 
		 )
 )


(:action receive-answer
  :parameters 	(?question_ask_answer - question_ask_answer  ?question - question  ?dur - dur)
  :precondition (and
                 ;;overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;;test control
		 (waiting_response)
		 (question_finished ?question)
 		 (not (tracking_pose ?question))
                 (is_ask_answer ?question_ask_answer ?question)
;;   		 (not (reproduced_valid_answer ?question))
                 (= (current_question ) (test_question_position ?question ))		 
		 (> (current_attempt_answer ?question) 0)
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))
		 (max_dur ?question_ask_answer ?dur)
		 (not (skipped ?question))
		 (not (validate_answer ?question ?question_ask_answer))
                )

  :effect 	(and
                (increase (current_attempt_answer ?question) 1)		 
                (valid_answer)
		(correct_answer) ;; FJGP
		(not (waiting_response))
		)
 )

(:action receive-patient-pose
  :parameters 	(?question_ask_answer - question_ask_answer  ?question - question ?ti - tracking_item ?dur - dur)
  :precondition (and
                 ;;overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;;test control
		 (waiting_response)
		 (question_finished ?question)
                 (is_ask_answer ?question_ask_answer ?question)
;;   		 (not (reproduced_valid_answer ?question))
                 (= (current_question ) (test_question_position ?question )) 
		 (> (current_attempt_answer ?question) 0)
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))
		 (max_dur ?question_ask_answer ?dur)
		 (not (skipped ?question))
		 (not (validate_answer ?question ?question_ask_answer))
		 (tracking_pose ?question)
		 (tracking ?question ?ti)
		 (not (is-sequence ?ti))
                )

  :effect 	(and
                (increase (current_attempt_answer ?question) 1)		 
                (valid_answer)
		(correct_answer) ;; FJGP
		(not (waiting_response))
		)
 )


(:action detect-posture-sequence
  :parameters 	(?question_ask_answer - question_ask_answer  ?question - question ?ti - tracking_item ?dur - dur)
  :precondition (and
                 ;;overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;;test control
		 (waiting_response)
		 (question_finished ?question)
                 (is_ask_answer ?question_ask_answer ?question)
;;   		 (not (reproduced_valid_answer ?question))
                 (= (current_question ) (test_question_position ?question )) 
		 (> (current_attempt_answer ?question) 0)
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))
		 (max_dur ?question_ask_answer ?dur)
		 (not (skipped ?question))
		 (tracking_pose ?question)
		 (tracking ?question ?ti)
		 (is-sequence ?ti)
                )

  :effect 	(and
                (increase (current_attempt_answer ?question) 1)	
		)
 )

(:action get-posture-sequence
  :parameters 	(?question - question ?ti - tracking_item ?question_ask_answer - question_ask_answer)
  :precondition (and
                 ;;overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;;test control
		 (waiting_response)
		 (question_finished ?question)
;;   		 (not (reproduced_valid_answer ?question))
                 (= (current_question ) (test_question_position ?question )) 
		 (> (current_attempt_answer ?question) 1)
                 (is_ask_answer ?question_ask_answer ?question)
 		 (= (current_attempt_answer ?question) (+ (question_ask_answer_position ?question_ask_answer) 1))
		 (not (skipped ?question))
		 (tracking_pose ?question)
		 (tracking ?question ?ti)
		 (is-sequence ?ti)
		 (not (validate_answer ?question ?question_ask_answer))
                )

  :effect 	(and
                (valid_answer)
		(correct_answer) ;; FJGP
		(not (waiting_response))
		)
 )
 (:action get-posture-sequence-validate
  :parameters 	(?question - question ?ti - tracking_item ?question_ask_answer - question_ask_answer)
  :precondition (and
                 ;;overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;;test control
		 (waiting_response)
		 (question_finished ?question)
;;   		 (not (reproduced_valid_answer ?question))
                 (= (current_question ) (test_question_position ?question )) 
		 (> (current_attempt_answer ?question) 1)
                 (is_ask_answer ?question_ask_answer ?question)
 		 (= (current_attempt_answer ?question) (+ (question_ask_answer_position ?question_ask_answer) 1))
		 (not (skipped ?question))
		 (tracking_pose ?question)
		 (tracking ?question ?ti)
		 (is-sequence ?ti)
		 (validate_answer ?question ?question_ask_answer)
                )

  :effect 	(and
                (valid_answer)
		(correct_answer) ;; FJGP
		(not (waiting_response))
		)
 )
 
(:action receive-answer-validate
  :parameters 	(?question_ask_answer - question_ask_answer  ?question - question  ?dur - dur)
  :precondition (and
                 ;;overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;;test control
		 (waiting_response)
		 (question_finished ?question)
		 (not (tracking_pose ?question))
                  (is_ask_answer ?question_ask_answer ?question)
;;   		 (not (reproduced_valid_answer ?question))
                 (= (current_question ) (test_question_position ?question ))		 
		 (> (current_attempt_answer ?question) 0)
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))
		 (max_dur ?question_ask_answer ?dur)
		 (not (skipped ?question))
		 (validate_answer ?question ?question_ask_answer)
                )

  :effect 	(and
                (increase (current_attempt_answer ?question) 1)		 
                (valid_answer)
		(correct_answer) ;; FJGP
		(not (waiting_response))
		;;(received_answer ?question) ;; FJGP
		)
 )


(:action receive-patient-pose-validate
  :parameters 	(?question_ask_answer - question_ask_answer  ?question - question  ?dur - dur ?ti - tracking_item)
  :precondition (and
                 ;;overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;;test control
		 (waiting_response)
		 (question_finished ?question)
                  (is_ask_answer ?question_ask_answer ?question)
;;   		 (not (reproduced_valid_answer ?question))
                 (= (current_question ) (test_question_position ?question )) 
		 (> (current_attempt_answer ?question) 0)
		 (= (current_attempt_answer ?question) (question_ask_answer_position ?question_ask_answer))
		 (max_dur ?question_ask_answer ?dur)
		 (not (skipped ?question))
		 (validate_answer ?question ?question_ask_answer)
		 (tracking_pose ?question)
		 (tracking ?question ?ti)
 		 (not (is-sequence ?ti))
                )

  :effect 	(and
                (increase (current_attempt_answer ?question) 1)		 
                (valid_answer)
		(correct_answer) ;; FJGP
		(not (waiting_response))
		;;(received_answer ?question) ;; FJGP
		)
 )


;;AGO: this action is executed when the question has failed and there is an alternative one

(:action start-alternative-question
  :parameters 	(?question_start - question_start ?question - question  ?pause - pause ?config - question_configuration)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
                 (> (current_introduction ) (number_test_introductions ))
         	 (test_configured )
 		 (question ?question )
		 (current_configuration ?config)
		 (is_question_start ?config ?question_start ?question)
;		 (current_configuration_question_start ?question ?question_start ?config)
                 (= (current_question ) (test_question_position ?question ))
 		 (<= (current_question ) (number_test_questions ))
 		 (not (question_started ?question ))
		 (after_pause ?question_start ?pause)
		 (not (repetition_requested))
		 (not (changes_requested))
		 (not (needs_extra_monitoring ?question))
		 (is_alternative_question ?question)
		 (> (number_consecutive_failed_questions) (consecutive_failed_alternative ?question)) ;; FJGP
		 )
		 
  :effect 	(and
; 		 (not (current_configuration_question_start  ?question ?question_start))
		 (not (valid_answer))
		 (question_started ?question )
		 (assign (current_option ?question) 1)
		 )
  )

(:action skip-alternative-question
  :parameters 	(?question - question ?current - question_configuration ?next - question_configuration)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
                 (> (current_introduction ) (number_test_introductions ))
          	 (test_configured )
 		 (question ?question )
                 (= (current_question ) (test_question_position ?question ))
 		 (<= (current_question ) (number_test_questions ))
 		 (not (question_started ?question ))
		 (not (repetition_requested))
		 (not (changes_requested))
 		 (not (needs_extra_monitoring ?question))
		 (current_configuration ?current)
		 (next_configuration ?current ?next)
		 (is_alternative_question ?question)
		 (<= (number_consecutive_failed_questions) (consecutive_failed_alternative ?question)) ;; FJGP
		 )
		 
  :effect 	(and
		 (not (valid_answer))
		 (increase (current_question ) 1)
		 )
  )

(:action finish-ask-answer-fail-continue
  :parameters 	(?question - question)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		    		 
		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))
		 (not (waiting_response))
 		 (not (valid_answer))
		 (> (current_attempt_answer ?question) (number_attempts_answer ?question))
		 (< (number_failed_questions) (max_failed_questions))
                )

  :effect 	(and
		 (do_question_transition)
         	 (increase (number_failed_questions) 1)		 		 
		 (increase (number_consecutive_failed_questions) 1) 
		 )
  )

(:action finish-ask-answer-fail-interrupt
  :parameters 	(?question - question ?property - property)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		    		 
		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))
		 (not (waiting_response))
 		 (not (valid_answer))
		 (> (current_attempt_answer ?question) (number_attempts_answer ?question))
		 (>= (number_failed_questions) (max_failed_questions))
		 (is_failed_n ?property)
		 ;;(not (skipped ?question))
                )

  :effect 	(and
		 ;;(not (can_continue))
		 (not (internal_can_continue))
         	 ;(increase (number_failed_questions) 1)
		 ;;(decrease (number_failed_questions) 1) ;; reset this numest (maybe assign)
		 (assign (number_failed_questions) 0)
		 (detected_property ?property)
		 (do_question_transition)
		 )
  )




(:action finish-ask-answer-success
  :parameters 	(?question - question)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
 		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))
  		 (not (waiting_response))
;;		 (not (changed_answer ?question))
		 (valid_answer)
		 (correct_answer) ;; FJGP

;; 		 (reproduced_valid_answer ?question)
                )

  :effect 	(and
		 (assign (number_consecutive_failed_questions) 0) ;; FJGP
		 (assign (number_failed_questions) 0)
		 (do_question_transition)
		 )
  )

;; FJGP
(:action finish-ask-answer-success-not-correct
  :parameters 	(?question - question  ?ask - question_ask_answer)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
 		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))
  		 (not (waiting_response))
;;		 (not (changed_answer ?question))
		 (valid_answer)
		 (not (correct_answer))
		 (is_ask_answer ?ask ?question) ;; FJGP
		 (= (question_ask_answer_position ?ask) (- (current_attempt_answer ?question) 1))
		 (>= (current_number_repetitions) (number_repetitions ?ask))

;; 		 (reproduced_valid_answer ?question)
                )

  :effect 	(and
		 (increase (number_consecutive_failed_questions) 1) 
		 (assign (number_failed_questions) 0)
		 (do_question_transition)
		 )
  )

(:action repeat-ask-answer-not-correct
  :parameters 	(?ask - question_ask_answer ?question - question)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
 		 (question_finished ?question )
                 (= (current_question ) (test_question_position ?question ))
  		 (not (waiting_response))
		 (valid_answer)
		 (not (correct_answer))
		 (is_ask_answer ?ask ?question) ;; FJGP
		 (= (question_ask_answer_position ?ask) (- (current_attempt_answer ?question) 1))
		 (<  (current_number_repetitions) (number_repetitions ?ask))
                )

  :effect 	(and
  		(not (valid_answer))
		(decrease (current_attempt_answer ?question) 1)
		(increase (current_number_repetitions) 1)
		(waiting_response)
		 )
  )

(:action make-question-transition
  :parameters 	(?qt - question_transition ?q - question ?question_repetition_modification - question_repetition_modification ?p - pause ?current - question_configuration
  		?first - question_configuration ?previous - question_configuration)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (do_question_transition)
		 (is_question_transition ?previous ?qt ?q)
		 (after_pause ?qt ?p)
                 (= (current_question ) (test_question_position ?q ))
		 (<= (current_question ) (number_test_questions ))
		 (not (skipped ?q))
		 (is_first_configuration ?first)
		 (current_configuration ?current)
		 (next_configuration ?previous ?current)
		 (is_question_repetition_modification ?question_repetition_modification ?q)
		 )
  :effect 	(and

		 (increase (current_question ) 1)
		 (not (do_question_transition))
		 (not (current_configuration ?current))
		 (current_configuration ?first)
		 (not (valid_answer))
		 (not (correct_answer))
		 )
  )

;;AGO: to avoid a disyunctive precondition in the former one
 (:action make-question-transition-skip
   :parameters 	(?qt - question_transition ?q - question ?p - pause ?current - question_configuration ?first - question_configuration ?previous - question_configuration)
   :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (do_question_transition)
		 (is_question_transition ?previous ?qt ?q)
		 (after_pause ?qt ?p)
                 (= (current_question ) (test_question_position ?q ))
		 (< (current_question ) (number_test_questions ))
		 (<= ( number_consecutive_failed_questions) ( consecutive_failed_alternative ?q) ) ;; FJGP
		 (skipped ?q)
		 (not (skip_next ?q))
		 (is_first_configuration ?first)
		 (current_configuration ?current)
		 (next_configuration ?previous ?current)
		 )
  :effect 	(and

		 (increase (current_question ) 1)
		 (not (do_question_transition))
		 (not (current_configuration ?current))
		 (current_configuration ?first)
 		 (not (valid_answer))
 		 (not (correct_answer))
		 )
  )

  ;;AGO: when skipping a question means skipping also the next one (as in questions 13_ of minimental)
  ;;both are counted just as a single fail
(:action make-question-transition-skip-next
   :parameters 	(?qt - question_transition ?q - question ?q_next - question ?p - pause 
   		 ?current - question_configuration ?first - question_configuration ?previous - question_configuration)
   :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control		 		 
		 (do_question_transition)
		 (is_question_transition ?previous ?qt ?q)
		 (after_pause ?qt ?p)
                 (= (current_question ) (test_question_position ?q ))
                 (= (test_question_position ?q_next ) (+ 1 (current_question )))
		 (< (current_question ) (number_test_questions ))
		 (<= ( number_consecutive_failed_questions) ( consecutive_failed_alternative ?q) ) ;; FJGP
		 (skipped ?q)
		 (skip_next ?q)
		 (is_first_configuration ?first)
		 (current_configuration ?current)
		 (next_configuration ?previous ?current)
		 )
  :effect 	(and
  		(skipped ?q_next)
		(increase (current_question ) 1)
		(not (do_question_transition))
		(decrease (number_failed_questions) 1)
		(not (current_configuration ?current))
		(current_configuration ?first)
  		(not (valid_answer))
 		(not (correct_answer))
		 )
  )

(:action finish-test
  :parameters 	(?test-end - test_end)
  :precondition (and
                 ;; overall. Robot control		 
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
		 (not (do_question_transition))
		 (> (current_question ) (number_test_questions ))
		 )
  :effect 	(and
;		 (not (do_question_transition))
                 (test_finished ))
 )


(:action prepare-question-repetition
  :parameters 	(?question - question ?question_repetition - question_repetition ?current_config - question_configuration ?first_config - question_configuration)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
		 (repetition_requested)
		 (not (cannot_be_repeated ?question))
 		 (not (changes_requested));;AGO creo que sobra
		 (= (current_question) (+ (test_question_position ?question) 1))
		 (> (current_question) 1)
		 (is_question_repetition ?question_repetition ?question)
 		 (is_first_configuration ?first_config)
		 (current_configuration ?current_config)
 		 (question ?question)
		 )
		 
  :effect 	(and
		 (not (repetition_requested))
		 (decrease (current_question) 1)
		 (not (question_started ?question))
		 (not (question_finished ?question))
		 (not (current_configuration ?current_config))
		 (current_configuration ?first_config)
		 (assign (current_attempt_answer ?question) 1)
		 )
  )


(:action ignore-question-repetition
  :parameters 	(?question - question)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
		 (repetition_requested)
		 (cannot_be_repeated ?question)
 		 (not (changes_requested));;AGO creo que sobra
		 (= (current_question) (+ (test_question_position ?question) 1))
		 (> (current_question) 1)
 		 (question ?question)
		 )
		 
  :effect 	(and
		 (not (repetition_requested))
		 )
  )

(:action prepare-question-modification
  :parameters 	(?question - question ?question_modification - question_modification ?current_config - question_configuration ?next_config - question_configuration)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
		 (not (repetition_requested))
		 (changes_requested)
		 (not (cannot_be_changed ?question))
		 (= (current_question) (+ (test_question_position ?question) 1))
		 (> (current_question) 1)
 		 (question ?question)
		 (is_question_modification ?question_modification ?question)
		 (current_configuration  ?current_config) ;; FJGP
		 (next_configuration  ?current_config ?next_config) ;; FJGP
		 )
		 
  :effect 	(and
		 (not (changes_requested))
		 (question_started ?question)
		 (question_finished ?question)
		 (decrease (current_question) 1)
		 (not (current_configuration ?current_config)) ;; FJGP
		 (current_configuration ?next_config) ;; FJGP
 		 (assign (current_attempt_answer ?question) 1)
		 (waiting_response) ;; FJGP
     		 (assign (current_number_repetitions) 0) ;; FJGP
		 (assign (number_attempts_answer ?question) 1) ;; FJGP
		 )
  )


(:action ignore-question-modification
  :parameters 	(?question - question)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
		 (not (repetition_requested))
		 (changes_requested)
		 (cannot_be_changed ?question)
		 (= (current_question) (+ (test_question_position ?question) 1))
		 (> (current_question) 1)
 		 (question ?question)
		 )
		 
  :effect 	(and
		 (not (changes_requested))
		 )
  )


(:action ignore-question-modification
  :parameters 	(?question - question)
  :precondition (and
                 ;; overall. Robot control
                 (can_continue)
                 (internal_can_continue)
         	 ;; test control
		 (not (repetition_requested))
		 (changes_requested)
		 (cannot_be_changed ?question)
		 (= (current_question) (+ (test_question_position ?question) 1))
		 (> (current_question) 1)
 		 (question ?question)
		 )
		 
  :effect 	(and
		 (not (changes_requested))
		 )
  )
;; action process test results? ;;AGO: probablemente s�, pero yo no me meter�a de momento


;; -----------------------------------------------------------------------------------------------------------------
;; ROBOT CONTROL ACTIONS
;; -----------------------------------------------------------------------------------------------------------------

(:action interrupt  ;;AGO: esto podr�a incluir una acci�n para que el robot hiciera algo como ir a
			       ;;buscar al paciente. Pero se puede implementar tambi�n a bajo nivel,
			       ;;de forma que si el executive la recibe la descomponga en buscar y
			       ;;esperar por ejemplo.  AGO mi duda aqu� es c�mo vamos a implementar
			       ;;esto, porque mandamos la acci�n y nuestro plan ya no puede
			       ;;continuar. Quiz�s un efecto de esta acci�n deber�a ser (not
			       ;;(patient_absent))
  :parameters 	(?test - test ?prop - property ?behavior - behavior)
  :precondition (and
		 (detected_property ?prop) ;;exogeneous event
                 (behavior_of_property ?prop ?behavior) 
		 (not (can_continue)) ;;exogeneous event
		 (test_configured)
		 )

  :effect 	(and
		 (waiting_restore_from ?prop ?behavior)
                 (not (waiting_response))
		 )
  )


(:action internal-interrupt  
  :parameters 	(?test - test ?prop - property ?behavior - behavior)
  :precondition (and
		 (detected_property ?prop) ;;exogeneous event
                 (behavior_of_property ?prop ?behavior) 
		 (not (internal_can_continue)) ;;exogeneous event
		 (test_configured)
		 )

  :effect 	(and
		 (not (detected_property ?prop))
		 (waiting_restore_from ?prop ?behavior)
                 (not (waiting_response))		 
		 )
  )


(:action restore-from
  :parameters 	(?behavior - behavior ?prop - property ?question - question)
  :precondition (and
;;    		   (detected_property ?prop) ;;exogeneous event		   
                   (waiting_restore_from ?prop ?behavior)
		   (not (waiting_response))
      		   (not (has_restore_label ?question ?prop))
		   )

  :effect 	(and
     		   (not (detected_property ?prop)) ;;exogeneous event		   
                   (not (waiting_restore_from ?prop ?behavior))
		   (can_continue))

)


(:action restore-from-internal
  :parameters 	(?behavior - behavior ?prop - property  ?question  - question)
  :precondition (and
;;   		   (detected_property ?prop) ;;exogeneous event		   
                   (waiting_restore_from ?prop ?behavior)
   		   (not (has_restore_label ?question ?prop))
		   )

  :effect 	(and
    		   (not (detected_property ?prop));;
                   (not (waiting_restore_from ?prop ?behavior))
		   (internal_can_continue))

)

(:action restore-from-and-show
  :parameters 	(?behavior - behavior ?prop - property ?question - question ?restore-label - test_element)
  :precondition (and
;;    		   (detected_property ?prop) ;;exogeneous event		   
                   (waiting_restore_from ?prop ?behavior)
		   (question ?question)
   	    	   (= (current_question ) (test_question_position ?question ))
           	   (<= (current_question ) (number_test_questions ))
   		   (has_restore_label ?question ?prop)
   		   (is_restore_label ?restore-label ?question)
		   )

  :effect 	(and
     		   (not (detected_property ?prop)) ;;exogeneous event		   
                   (not (waiting_restore_from ?prop ?behavior))
		   (can_continue))
)



(:action restore-from-internal-and-show
  :parameters 	(?behavior - behavior ?prop - property ?question - question ?restore-label - test_element)
  :precondition (and
;;    		   (detected_property ?prop) ;;exogeneous event		   
                   (waiting_restore_from ?prop ?behavior)
		   (question ?question)
   	    	   (= (current_question ) (test_question_position ?question ))
           	   (<= (current_question ) (number_test_questions ))
   		   (has_restore_label ?question ?prop)
   		   (is_restore_label ?restore-label ?question)
		   )

  :effect 	(and
     		   (not (detected_property ?prop)) ;;exogeneous event		   
                   (not (waiting_restore_from ?prop ?behavior))
		   (internal_can_continue)		   )
		   )		   


(:action start-monitoring
  :parameters 	(?question - question ?pro - property)
  :precondition (and
		   (question ?question)
   	    	   (= (current_question ) (test_question_position ?question ))
           	   (<= (current_question ) (number_test_questions ))
   		   (needs_extra_monitoring ?question)
   		   (monitor ?question ?pro)
		   (not (repetition_requested)) ;; FJGP
		   (not (changes_requested)) ;; FJGP
		   )

  :effect 	(and
		   (not (needs_extra_monitoring ?question))
		   )		   
)
)


;; Posible sub-states one or more (detected_property prop), not (can_continue), any not
;; (waiting_restore prop) one or more (detected_property prop), not (can_continue), one or more
;; (waiting_restore prop) any (detected_property), (can_continue),



