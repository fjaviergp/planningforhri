(define (problem p1)
(:domain clark)
(:objects
   the_robot - robot
   the_patient - patient
   spanish english french - language
   present - verbal_tense
   patient  - person_type
   getupandgo - test
   first second third - question_configuration
   patient_seated patient_is_far - tracking_item

   ;; Behaviors
   robot_pause robot_call_clinician robot_call_technician robot_call_patient -  behavior
   patient_absent patient_ask_for_help - property
   ;;patient_property
   robot_low_battery pause_button_pressed - property
   ;;robot_property

   ;; Labels 
   robot_pres1 - presentation
   intro4 intro6 intro8 intro9 intro9_1 intro10 intro11 intro11_1 - test_introduction
   end1 - test_end
   q1 q2 - question
  
   ;; q1
   q1_s1 - question_start 
   q1_o1 q1_o2 - question_option
   q1_s2 - question_end
   q1_a1 q1_a2 - question_ask_answer
   q1_t - question_transition
   q_r - question_repetition
   q_m - question_modification
   q_sn - question_repetition_modification

   ;; q2
   q2_s1  - question_start
   q2_e1  - question_end
   q2_a1  - question_ask_answer
   q2_t - question_transition

   ;; Pauses
   pause_0sg pause_1sg - pause
   dur_20sg dur_5min - dur
)
(:init
 ;; test configuration
 (current_test getupandgo)
 (test_configuration spanish patient present)
 
(can_continue)
(internal_can_continue)

(behavior_of_property pause_button_pressed robot_pause)
(behavior_of_property patient_absent robot_call_patient)
(behavior_of_property patient_ask_for_help robot_call_clinician)
(behavior_of_property robot_low_battery robot_call_technician)
		      
(= (number_failed_questions) 0)
(= (max_failed_questions) 2)
(= (number_consecutive_failed_questions) 0) ;; FJGP

(robot_initial_position the_robot)

(max_duration dur_20sg)

 ;;----------------------------------------------------------------------------------------
 ;; Robot presentation
 ;;----------------------------------------------------------------------------------------
 (robot_presentation robot_pres1 the_robot)
 (after_pause robot_pres1 pause_0sg)

 (finished_introduce_robot)


 ;;----------------------------------------------------------------------------------------
 ;; GetUpAndGo test introductions
 ;;----------------------------------------------------------------------------------------
 (= (number_test_introductions ) 6)
 ;; Initial performed introductions
 (= (current_introduction ) 1)
 
 (is_test_introduction_initial intro4 )
 (= (test_introduction_position intro4 ) 1)
 (after_pause intro4 pause_1sg)

 (is_test_introduction_video_initial intro6 )
 (= (test_introduction_position intro6 ) 2)
 (after_pause intro6 pause_1sg)

 (is_test_introduction_initial intro8 )
 (= (test_introduction_position intro8 ) 3)
 (after_pause intro8 pause_1sg)

 (is_test_introduction_get_up intro9 )
 (= (test_introduction_position intro9 ) 4)
 (after_pause intro9 pause_1sg)

 (is_test_introduction_not_get_up intro9_1)
 (after_pause intro9_1 pause_0sg)

 (is_test_introduction_start intro10 )
 (= (test_introduction_position intro10 ) 5)
 (after_pause intro10 pause_1sg)
 
 (is_test_introduction_start intro11 )
 (= (test_introduction_position intro11 ) 6)
 (after_pause intro11 pause_1sg)

 (is_test_introduction_not_detect intro11_1)
 (after_pause intro11_1 pause_1sg)

 (= (current_repetition_get_up) 0)
 (= (repetitions_get_up) 2)
 (= (current_attempt_detect_patient_chair) 0)
 (= (attempts_detect_patient_chair) 2)

 ;;----------------------------------------------------------------------------------------
 ;;  test questions
 ;;----------------------------------------------------------------------------------------
 ( =  (number_test_questions ) 2)
 ;; Initial performed questions
 (= (current_question ) 1)
 (= (current_number_repetitions) 0)
 (current_configuration first)
 (next_configuration first second)
 (next_configuration second third)
 (is_first_configuration first)
 
 (= (number_resets) 5)
 (= (current_number_resets) 0)

 ;;----------------------------------------------------------------------------------------
 ;; question 1
 ;;----------------------------------------------------------------------------------------
  (question q1)
 (= (test_question_position q1 ) 1)
 (is_direct_question q1)

 (tracking_pose q1)
 (tracking_sequence q1 patient_seated q1_a1)
 (tracking_sequence q1 patient_is_far q1_a2)
 (has_reset_label q1 patient_seated q1_o1)
 (has_reset_label q1 patient_is_far q1_o2)
 (reset_question q1)
 (is_pose_sequence q1)

 (is_question_start first q1_s1 q1)
 (after_pause q1_s1 pause_1sg)
 (is_question_end first q1_s2 q1)
 (after_pause q1_s2 pause_1sg)
 (is_question_transition first q1_t q1)
 (after_pause q1_t pause_0sg)

 (= (number_options q1) 0)
 (= (current_option q1) 0)

 ;; ask_answer
 (= (current_attempt_answer q1) 1)
 (= (number_attempts_answer q1) 2)
 (= (consecutive_failed_alternative q1) 20) ;; FJGP
 
 (is_ask_answer  q1_a1 q1)
 (max_dur q1_a1 dur_20sg)
 (= (question_ask_answer_position q1_a1) 1)
 (= (number_repetitions q1_a1) 0)

 (is_ask_answer  q1_a2 q1)
 (disable_ask_answer q1_a2)
 (max_dur q1_a2 dur_5min)
 (= (question_ask_answer_position q1_a2) 2)
 (= (number_repetitions q1_a2) 0)

 (is_question_repetition_modification q_sn q1)

 ;;----------------------------------------------------------------------------------------
 ;; question 2
 ;;----------------------------------------------------------------------------------------
  (question q2 )
  (= (test_question_position q2 ) 2)

 (is_question_transition first q2_t q2)
 (after_pause q2_t pause_0sg)

 (is_process_data q2)

 (= (number_options q2) 0)
 (= (current_option q2) 1)
 
 ;; ask_answer
 (= (current_attempt_answer q2) 1)
 (= (number_attempts_answer q2) 1)
 (= (consecutive_failed_alternative q2) 20)
 
 (is_ask_answer  q2_a1 q2)
 (max_dur q2_a1 dur_5min)
 (= (question_ask_answer_position q2_a1) 1)
 (= (number_repetitions q2_a1) 0)

 (is_question_repetition_modification q_sn q2)


)
(:goal (and
	(test_finished)
	))
)
