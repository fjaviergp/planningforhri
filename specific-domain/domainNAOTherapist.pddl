(define (domain therapist)
  (:requirements :strips :typing :fluents :negative-preconditions :equality) 
  (:types 
	patient
	robot 
	session - object
	exercise 
	pose
	posture
    stand_up sit_down - mode
	timestamp
	location)
(:predicates 
	; Room definition	
	(training-area ?l - location)
	(show-area ?l - location)
	(at ?o - object ?l - location)

	; Unexpected situations
	(patient_distracted ?r - robot ?p - patient)
	(emergency_situation ?r - robot ?p - patient)
	(paused_session ?r - robot ?p - patient ?s - session)
	(uncontrolled_situation ?r - robot ?p - patient)
    (mode_state ?o - object ?m - mode) ; ROBOT and PATIENT sit_down or stand_up

	; Session control
	(rehabilitation_procedure ?r - robot ?p - patient)
	(detected_patient ?r - robot ?p - patient)
	(identified_patient ?r - robot ?p - patient)
	(greeted-patient ?r - robot ?p - patient)
	(started-session ?r - robot ?p - patient ?s - session)
	(robot-is-training ?r - robot)
	(introduced-exercise ?e - exercise)
	(training-exercise ?e - exercise)
	(finished-exercise ?e - exercise)
	(finished-training ?r - robot ?p - patient)
	(say-good-bye-patient ?r - robot ?p - patient)
	(finished-session ?r - robot ?p - patient ?s - session)
	
	; Exercise, poses and postures
    (mode_required ?e - exercise ?m - mode)
	(pose_exercise ?pos - pose ?e - exercise) 
	(pose_postures ?pos - pose ?leftarm ?rightarm - posture)
	(pose_timestamp ?pos - pose ?ts - timestamp)
	(executed-pose ?pos - pose) 
	(correct-pose ?pos - pose)
)
(:functions
	(e-position ?e - exercise)
	(exercise-number)
	(exercise-counter)
	(p-position ?pos - pose)
	(pose-number ?e - exercise)
	(pose-counter)
)

(:action detect-patient
  :parameters 	(?r - robot ?p - patient)
  :precondition (and
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(not (detected_patient ?r ?p)))
  :effect 	(and
			(detected_patient ?r ?p))
)
(:action identify-patient
  :parameters 	(?r - robot ?p - patient)
  :precondition (and 
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(not (identified_patient ?r ?p)))
  :effect 	(identified_patient ?r ?p)
)
(:action greet-patient
  :parameters 	(?r - robot ?p - patient)
  :precondition (and 
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(identified_patient ?r ?p)
				(not (greeted-patient ?r ?p)))
  :effect 	(greeted-patient ?r ?p)
)
(:action start-training
  :parameters 	(?r - robot ?tr - location ?p - patient ?sh - location ?s - session)
  :precondition (and 
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(at ?p ?tr) (training-area ?tr) 
				(at ?r ?sh) (show-area ?sh) 
				(greeted-patient ?r ?p)
				(not (started-session ?r ?p ?s)))
  :effect 	(started-session ?r ?p ?s)
)
(:action introduce-exercise
  :parameters 	(?e - exercise ?r - robot ?p - patient ?s - session)
  :precondition (and 
				(not (patient_distracted ?r ?p)) ; If patient is distracted
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(started-session ?r ?p ?s) 
				(not (introduced-exercise ?e)) 
				(= (e-position ?e) (exercise-counter))
				(not (robot-is-training ?r)))
  :effect 	(and 
			(introduced-exercise ?e) 
			(robot-is-training ?r))
)
(:action stand-up
  :parameters 	(?e - exercise ?r - robot ?p - patient ?sup - stand_up ?sdw - sit_down)
  :precondition (and 
				(not (patient_distracted ?r ?p))  ; If patient is distracted
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(introduced-exercise ?e) 
				(mode_required ?e ?sup)
				(mode_state ?r ?sdw)) ; If robot and patient are sit-down
  :effect 	(and
            (not (mode_state ?r ?sdw))
            (not (mode_state ?p ?sdw))
            (mode_state ?r ?sup)
            (mode_state ?p ?sup))
)
(:action sit-down
  :parameters 	(?e - exercise ?r - robot ?p - patient ?sup - stand_up ?sdw - sit_down)
  :precondition (and 
				(not (patient_distracted ?r ?p))  ; If patient is distracted
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(introduced-exercise ?e) 
				(mode_required ?e ?sdw)
				(mode_state ?r ?sup)) ; If robot and patient are sit-down
  :effect 	(and
            (not (mode_state ?r ?sup))
            (not (mode_state ?p ?sup))
            (mode_state ?r ?sdw)
            (mode_state ?p ?sdw))
)
(:action start-exercise
  :parameters 	(?e - exercise ?r - robot ?p - patient ?m - mode)
  :precondition (and 
				(not (patient_distracted ?r ?p))  ; If patient is distracted
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(introduced-exercise ?e) 
				(mode_required ?e ?m)
				(mode_state ?r ?m)
				(mode_state ?p ?m)
				(not (training-exercise ?e)))
  :effect 	(and
			(training-exercise ?e)
			(assign (pose-counter) 0))
)
(:action execute-pose
  :parameters 	(?pos - pose ?e - exercise ?pleft - posture ?pright - posture ?ts - timestamp ?r - robot ?p - patient ?m - mode)
  :precondition (and 
				(not (patient_distracted ?r ?p))  ; If patient is distracted
				(detected_patient ?r ?p)
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
                (mode_state ?r ?m)
                (mode_state ?p ?m)
				(training-exercise ?e) 
				(pose_exercise ?pos ?e)
				(pose_postures ?pos ?pleft ?pright)
				(pose_timestamp ?pos ?ts)
				(= (p-position ?pos) (pose-counter)) 
				(not (executed-pose ?pos))
				(< (pose-counter) (pose-number ?e)))
  :effect 	(and
			(executed-pose ?pos)
			(correct-pose ?pos))
)
(:action correct-pose
  :parameters 	(?pos - pose ?e - exercise ?pleft - posture ?pright - posture ?ts - timestamp ?r - robot ?p - patient ?m - mode)
  :precondition (and 
				(not (patient_distracted ?r ?p))  ; If patient is distracted
				(detected_patient ?r ?p)
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
                (mode_state ?r ?m)
                (mode_state ?p ?m)
				(training-exercise ?e)
				(pose_postures ?pos ?pleft ?pright)
				(pose_timestamp ?pos ?ts) 
				(executed-pose ?pos)
				(not (correct-pose ?pos)))
  :effect 	(and
			(correct-pose ?pos))
)
(:action finish-pose
  :parameters 	(?pos - pose ?e - exercise ?pleft - posture ?pright - posture ?ts - timestamp ?r - robot ?p - patient)
  :precondition (and 
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(training-exercise ?e) 
				(executed-pose ?pos)
				(pose_postures ?pos ?pleft ?pright)
				(pose_timestamp ?pos ?ts) 
				(correct-pose ?pos))
  :effect 	(and
			(increase (pose-counter) 1)
			(not (executed-pose ?pos))
			(not (correct-pose ?pos)))
)
(:action finish-exercise
  :parameters 	(?e - exercise ?r - robot ?p - patient)
  :precondition (and 
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(training-exercise ?e) 
				(= (pose-counter) (pose-number ?e))	
				(not (finished-exercise ?e)))
  :effect 	(and 
			(finished-exercise ?e)
			(increase (exercise-counter) 1))
)
(:action perform-relaxation
  :parameters 	(?e - exercise ?r - robot ?p - patient)
  :precondition (and 
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(training-exercise ?e) 	
				(finished-exercise ?e))
  :effect 	(and 
			(not (training-exercise ?e))
			(not (introduced-exercise ?e)) 
			(not (robot-is-training ?r)))
)
(:action finish-training
  :parameters 	(?r - robot ?p - patient ?s - session)
  :precondition (and 
				(detected_patient ?r ?p)
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(started-session ?r ?p ?s)
				(= (exercise-counter) (exercise-number)))
  :effect 	(and
			(not (started-session ?r ?p ?s)) 
			(finished-training ?r ?p))
)
(:action say-good-bye
  :parameters 	(?r - robot ?p - patient)
  :precondition (and 
				(detected_patient ?r ?p)
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(finished-training ?r ?p)
				(not (say-good-bye-patient ?r ?p)))				
  :effect 	(say-good-bye-patient ?r ?p)
)
(:action finish-session
  :parameters 	(?r - robot ?p - patient ?s - session)
  :precondition (say-good-bye-patient ?r ?p)
  :effect 	(and
			(finished-session ?r ?p ?s)
			(not (say-good-bye-patient ?r ?p))
			(not (detected_patient ?r ?p))
			(not (identified_patient ?r ?p))
			(not (greeted-patient ?r ?p)))
)
(:action claim-stand-up
  :parameters 	(?r - robot ?p - patient ?sup - stand_up ?sdw - sit_down)
  :precondition (and 
                (not (patient_distracted ?r ?p))  ; If patient is distracted
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
                (mode_state ?r ?sup)
                (mode_state ?p ?sdw)) ; If robot is stand-up and patient is sit-down
  :effect 	(and
			(not(mode_state ?p ?sdw))
            (mode_state ?p ?sup))
)
(:action claim-sit-down
  :parameters 	(?r - robot ?p - patient ?sup - stand_up ?sdw - sit_down)
  :precondition (and 
                (not (patient_distracted ?r ?p))  ; If patient is distracted
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
                (mode_state ?r ?sdw)
                (mode_state ?p ?sup)) ; If robot is stand-up and patient is sit-down
  :effect 	(and
			(not(mode_state ?p ?sup))
            (mode_state ?p ?sdw))
)
(:action claim-attention
  :parameters 	(?r - robot ?p - patient)
  :precondition (and 
				(not (emergency_situation ?r ?p)) ; If happens an emergency situation
				(detected_patient ?r ?p)
				(patient_distracted ?r ?p))
  :effect 	(not (patient_distracted ?r ?p))
)
(:action pause-session
  :parameters 	(?r - robot ?p - patient ?s - session)
  :precondition (and 
				(emergency_situation ?r ?p))
  :effect 	(paused_session ?r ?p ?s)
)
(:action resume-session
  :parameters 	(?r - robot ?p - patient ?s - session)
  :precondition (and 
				(paused_session ?r ?p ?s)
				(not (uncontrolled_situation ?r ?p)))
  :effect 	(and 
            (not (emergency_situation ?r ?p))
            (not (paused_session ?r ?p ?s)))
)
(:action cancel-session
  :parameters 	(?r - robot ?p - patient ?s - session)
  :precondition (and 
				(paused_session ?r ?p ?s)
				(uncontrolled_situation ?r ?p))
  :effect 	(and
            (finished-session ?r ?p ?s)
            (not (paused_session ?r ?p ?s)))
)

)
