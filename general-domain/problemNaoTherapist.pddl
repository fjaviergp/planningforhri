(define (problem NaoTherapist)
(:domain interaction)
(:objects
	;;Events raised by the planner
	component_failed response_failed max_q_failed max_a_failed - event
	;;External events
	doctor_needed patient_absent - event
	;;behaviors
	call_doctor call_patient ignore show_video - behavior
	finish_and_call_doctor - behavior
	
	; Intro
	intro - component
	greet_patient - com_act
	
	; Ejercicio 1
	edemo1 - component
	intro_edemo1 start_edemo1 p_id0 p_id1 p_id2 p_id3 p_id4 p_id5 p_id6 p_id7 p_id8 relaxation - com_act  

	; Ejercicio 2
	edemo2 - component
	intro_edemo2 start_edemo2 p_id9 p_id10 p_id11 p_id12 p_id13 p_id14 p_id15 p_id16 p_id17 - com_act
)
(:init 
       ;;General
       (can_continue)
;       (detected_event patient_absent)
       (= (number_failed_components) 0)
       (= (max_failed_components) 2)
       (= (number_of_components) 2)
       (= (current_component) 0)
       (component_failed_event component_failed)
       (max_components_failed_event max_q_failed)
       (response_failed_event response_failed)
       (max_responses_failed_event max_a_failed)
       (= (current_com_act) 0)
;=============================================================================
       ;; intro
       (= (component_position intro) 0)
       (behavior_of_event intro component_failed call_doctor)
       (behavior_of_event intro doctor_needed call_doctor)
       (behavior_of_event intro patient_absent call_patient)
       (behavior_of_event intro max_q_failed call_doctor)
       (behavior_of_event intro max_a_failed call_doctor)
       (= (responses_received intro) 0)
       (= (responses_required intro) 0)
       (= (number_failed_responses intro) 0)
       (= (max_failed_responses intro) 1)

       (belongs greet_patient intro)
       ;;(use_speaker greet_patient)
       (= (com_act_position greet_patient) 0)
       (= (max_repetitions greet_patient) 1)
       (= (repetitions greet_patient) 0)
       (is_last_of_component greet_patient)
       (is_first_of_component_farewell greet_patient)

;=============================================================================  
       ;; edemo1
       (= (component_position edemo1) 1)
       (behavior_of_event edemo1 component_failed call_doctor)
       (behavior_of_event edemo1 doctor_needed call_doctor)
       (behavior_of_event edemo1 patient_absent call_patient)
       (behavior_of_event edemo1 max_q_failed call_doctor)
       (behavior_of_event edemo1 max_a_failed call_doctor)
       (= (responses_received edemo1) 0)
       (= (responses_required edemo1) 9)
       (= (number_failed_responses edemo1) 0)
       (= (max_failed_responses edemo1) 1)
       
       (belongs intro_edemo1 edemo1)
       ;;(use_speaker intro_edemo1)
       (= (com_act_position intro_edemo1) 0)
       (= (max_repetitions intro_edemo1) 1)
       (= (repetitions intro_edemo1) 0)

       (belongs start_edemo1 edemo1)
       ;;(use_speaker start_edemo1)
       (= (com_act_position start_edemo1) 1)
       (= (max_repetitions start_edemo1) 1)
       (= (repetitions start_edemo1) 0)

       (belongs p_id0 edemo1)
       ;;(use_speaker p_id0)
       (= (com_act_position p_id0) 2)
       (= (max_repetitions p_id0) 2) ;;2 tries to get the response
       (= (repetitions p_id0) 0)
       (requires_response p_id0)
       (requires_plausible_response p_id0)
       (requires_correct_response p_id0)
       (response_failed_behavior p_id0 response_failed finish_and_call_doctor)

       (belongs p_id1 edemo1)
       ;;(use_speaker p_id1)
       (= (com_act_position p_id1) 3)
       (= (max_repetitions p_id1) 2) ;;2 tries to get the response
       (= (repetitions p_id1) 0)
       (requires_response p_id1)
       (requires_plausible_response p_id1)
       (requires_correct_response p_id1)
       (response_failed_behavior p_id1 response_failed finish_and_call_doctor)

	(belongs p_id2 edemo1)
       ;;(use_speaker p_id2)
       (= (com_act_position p_id2) 4)
       (= (max_repetitions p_id2) 2) ;;2 tries to get the response
       (= (repetitions p_id2) 0)
       (requires_response p_id2)
       (requires_plausible_response p_id2)
       (requires_correct_response p_id2)
       (response_failed_behavior p_id2 response_failed finish_and_call_doctor)

	(belongs p_id3 edemo1)
       ;;(use_speaker p_id3)
       (= (com_act_position p_id3) 5)
       (= (max_repetitions p_id3) 2) ;;2 tries to get the response
       (= (repetitions p_id3) 0)
       (requires_response p_id3)
       (requires_plausible_response p_id3)
       (requires_correct_response p_id3)
       (response_failed_behavior p_id3 response_failed finish_and_call_doctor)        

	(belongs p_id4 edemo1)
       ;;(use_speaker p_id4)
       (= (com_act_position p_id4) 6)
       (= (max_repetitions p_id4) 2) ;;2 tries to get the response
       (= (repetitions p_id4) 0)
       (requires_response p_id4)
       (requires_plausible_response p_id4)
       (requires_correct_response p_id4)
       (response_failed_behavior p_id4 response_failed finish_and_call_doctor)    

	(belongs p_id5 edemo1)
       ;;(use_speaker p_id5)
       (= (com_act_position p_id5) 7)
       (= (max_repetitions p_id5) 2) ;;2 tries to get the response
       (= (repetitions p_id5) 0)
       (requires_response p_id5)
       (requires_plausible_response p_id5)
       (requires_correct_response p_id5)
       (response_failed_behavior p_id5 response_failed finish_and_call_doctor)    

	(belongs p_id6 edemo1)
       ;;(use_speaker p_id6)
       (= (com_act_position p_id6) 8)
       (= (max_repetitions p_id6) 2) ;;2 tries to get the response
       (= (repetitions p_id6) 0)
       (requires_response p_id6)
       (requires_plausible_response p_id6)
       (requires_correct_response p_id6)
       (response_failed_behavior p_id6 response_failed finish_and_call_doctor)    

	(belongs p_id7 edemo1)
       ;;(use_speaker p_id7)
       (= (com_act_position p_id7) 9)
       (= (max_repetitions p_id7) 2) ;;2 tries to get the response
       (= (repetitions p_id7) 0)
       (requires_response p_id7)
       (requires_plausible_response p_id7)
       (requires_correct_response p_id7)
       (response_failed_behavior p_id7 response_failed finish_and_call_doctor)    

	(belongs p_id8 edemo1)
       ;;(use_speaker p_id8)
       (= (com_act_position p_id8) 10)
       (= (max_repetitions p_id8) 2) ;;2 tries to get the response
       (= (repetitions p_id8) 0)
       (requires_response p_id8)
       (requires_plausible_response p_id8)
       (requires_correct_response p_id8)
       (response_failed_behavior p_id8 response_failed finish_and_call_doctor)    

	 (belongs relaxation edemo1)
       ;;(use_speaker relaxation)
       (= (com_act_position relaxation) 11)
       (= (max_repetitions relaxation) 1)
       (= (repetitions relaxation) 0)
       ;;(requires_response relaxation)
       ;;(requires_plausible_response relaxation)
       ;;(requires_correct_response relaxation)
       ;;(response_failed_behavior relaxation response_failed show_video)
       (requires_behavior relaxation)
       (required_behavior relaxation show_video)

	(is_last_of_component relaxation)
       (is_first_of_component_farewell relaxation)

;============================================================================= 

	;; edemo2
	(= (component_position edemo2) 2)
       (behavior_of_event edemo2 component_failed call_doctor)
       (behavior_of_event edemo2 doctor_needed call_doctor)
       (behavior_of_event edemo2 patient_absent call_patient)
       (behavior_of_event edemo2 max_q_failed call_doctor)
       (behavior_of_event edemo2 max_a_failed call_doctor)
       (= (responses_received edemo2) 0)
       (= (responses_required edemo2) 9)
       (= (number_failed_responses edemo2) 0)
       (= (max_failed_responses edemo2) 1)
       
       (belongs intro_edemo2 edemo2)
       ;;(use_speaker intro_edemo2)
       (= (com_act_position intro_edemo2) 0)
       (= (max_repetitions intro_edemo2) 1)
       (= (repetitions intro_edemo2) 0)

       (belongs start_edemo2 edemo2)
       ;;(use_speaker start_edemo2)
       (= (com_act_position start_edemo2) 1)
       (= (max_repetitions start_edemo2) 1)
       (= (repetitions start_edemo2) 0)

       (belongs p_id0 edemo2)
       ;;(use_speaker p_id9)
       (= (com_act_position p_id9) 2)
       (= (max_repetitions p_id9) 2) ;;2 tries to get the response
       (= (repetitions p_id9) 0)
       (requires_response p_id9)
       (requires_plausible_response p_id9)
       (requires_correct_response p_id9)
       (response_failed_behavior p_id9 response_failed finish_and_call_doctor)

       (belongs p_id10 edemo2)
       ;;(use_speaker p_id10)
       (= (com_act_position p_id10) 3)
       (= (max_repetitions p_id10) 2) ;;2 tries to get the response
       (= (repetitions p_id10) 0)
       (requires_response p_id10)
       (requires_plausible_response p_id10)
       (requires_correct_response p_id10)
       (response_failed_behavior p_id10 response_failed finish_and_call_doctor)

	(belongs p_id11 edemo2)
       ;;(use_speaker p_id11)
       (= (com_act_position p_id11) 4)
       (= (max_repetitions p_id11) 2) ;;2 tries to get the response
       (= (repetitions p_id11) 0)
       (requires_response p_id11)
       (requires_plausible_response p_id11)
       (requires_correct_response p_id11)
       (response_failed_behavior p_id11 response_failed finish_and_call_doctor)

	(belongs p_id12 edemo2)
       ;;(use_speaker p_id12)
       (= (com_act_position p_id12) 5)
       (= (max_repetitions p_id12) 2) ;;2 tries to get the response
       (= (repetitions p_id12) 0)
       (requires_response p_id12)
       (requires_plausible_response p_id12)
       (requires_correct_response p_id12)
       (response_failed_behavior p_id12 response_failed finish_and_call_doctor)        

	(belongs p_id13 edemo2)
       ;;(use_speaker p_id13)
       (= (com_act_position p_id13) 6)
       (= (max_repetitions p_id13) 2) ;;2 tries to get the response
       (= (repetitions p_id13) 0)
       (requires_response p_id13)
       (requires_plausible_response p_id13)
       (requires_correct_response p_id13)
       (response_failed_behavior p_id13 response_failed finish_and_call_doctor)    

	(belongs p_id14 edemo2)
       ;;(use_speaker p_id14)
       (= (com_act_position p_id14) 7)
       (= (max_repetitions p_id14) 2) ;;2 tries to get the response
       (= (repetitions p_id14) 0)
       (requires_response p_id14)
       (requires_plausible_response p_id14)
       (requires_correct_response p_id14)
       (response_failed_behavior p_id14 response_failed finish_and_call_doctor)    

	(belongs p_id15 edemo2)
       ;;(use_speaker p_id15)
       (= (com_act_position p_id15) 8)
       (= (max_repetitions p_id15) 2) ;;2 tries to get the response
       (= (repetitions p_id15) 0)
       (requires_response p_id15)
       (requires_plausible_response p_id15)
       (requires_correct_response p_id15)
       (response_failed_behavior p_id15 response_failed finish_and_call_doctor)    

	(belongs p_id16 edemo2)
       ;;(use_speaker p_id16)
       (= (com_act_position p_id16) 9)
       (= (max_repetitions p_id16) 2) ;;2 tries to get the response
       (= (repetitions p_id16) 0)
       (requires_response p_id16)
       (requires_plausible_response p_id16)
       (requires_correct_response p_id16)
       (response_failed_behavior p_id16 response_failed finish_and_call_doctor)    

	(belongs p_id17 edemo2)
       ;;(use_speaker p_id17)
       (= (com_act_position p_id17) 10)
       (= (max_repetitions p_id17) 2) ;;2 tries to get the response
       (= (repetitions p_id17) 0)
       (requires_response p_id17)
       (requires_plausible_response p_id17)
       (requires_correct_response p_id17)
       (response_failed_behavior p_id17 response_failed finish_and_call_doctor)    
       (is_last_of_component p_id17)
       (is_first_of_component_farewell p_id17)


;============================================================================= 

  )     
;;As the only way to finish the last component is to pass by all the other ones
;;we just have the last goal
(:goal	(and (interaction_finished)
))
)

