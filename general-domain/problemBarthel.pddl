(define (problem bartheltest)
(:domain interaction)
(:objects
	;;Events raised by the planner
	component_failed response_failed max_q_failed max_a_failed - event
	;;External events
	doctor_needed patient_absent - event
	;;behaviors
	call_doctor call_patient ignore speak_louder show_video - behavior
	finish_and_call_doctor - behavior
	
	;qI1
	qI1 - component
	intro_1 intro_2 - com_act
	;q1
	q1 - component
	q1_s1 q1_o1 q1_o2 q1_o3 q1_e1 q1_a1 q1_t1 - com_act
	;q2
	q2 - component
	q2_s1 q2_o1 q2_o2 q2_e1 q2_a1 q2_t1 - com_act
	;q3
	q3 - component
	q3_s1 q3_o1 q3_o2 q3_o3 q3_e1 q3_a1 q3_t1 - com_act
	;q4
	q4 - component
	q4_s1 q4_o1 q4_o2 q4_e1 q4_a1 q4_t1 - com_act
	;q5
	q5 - component
	q5_s1 q5_o1 q5_o2 q5_o3 q5_e1 q5_a1 q5_t1 - com_act
	;q6
	q6 - component
	q6_s1 q6_o1 q6_o2 q6_o3 q6_e1 q6_a1 q6_t1 - com_act
	;q7
	q7 - component
	q7_s1 q7_o1 q7_o2 q7_o3 q7_e1 q7_a1 q7_t1 - com_act
	;q8
	q8 - component
	q8_s1 q8_o1 q8_o2 q8_o3 q8_o4 q8_e1 q8_a1 q8_t1 - com_act
	;q9
	q9 - component
	q9_s1 q9_o1 q9_o2 q9_o3 q9_o4 q9_e1 q9_a1 q9_t1 - com_act
	;q10
	q10 - component
	q10_s1 q10_o1 q10_o2 q10_o3 q10_e1 q10_a1 q10_t1 - com_act
)
(:init 
       ;;General
       (can_continue)
;       (detected_event patient_absent)
       (= (number_failed_components) 0)
       (= (max_failed_components) 2)
       (= (number_of_components) 10)
       (= (current_component) 0)
       (component_failed_event component_failed)
       (max_components_failed_event max_q_failed)
       (response_failed_event response_failed)
       (max_responses_failed_event max_a_failed)
       (= (current_com_act) 0)
;=============================================================================  
       ;;qI1
       (= (component_position qI1) 0)
       (behavior_of_event qI1 component_failed call_doctor)
       (behavior_of_event qI1 doctor_needed call_doctor)
       (behavior_of_event qI1 patient_absent call_patient)
       (behavior_of_event qI1 max_q_failed call_doctor)
       (behavior_of_event qI1 max_a_failed call_doctor)
       (= (responses_received qI1) 0)
       (= (responses_required qI1) 0)
       (= (number_failed_responses qI1) 0)
       (= (max_failed_responses qI1) 1)

       (belongs intro1 qI1)
       (= (com_act_position intro1) 0)
       (= (max_repetitions intro1) 1)
       (= (repetitions intro1) 0)

       (belongs intro2 qI1)
       (= (com_act_position intro2) 1)
       (= (max_repetitions intro2) 1)
       (= (repetitions intro2) 0)
       (is_last_of_component intro2)
       (is_first_of_component_farewell intro2)
       
;==============================================================================
 	;;q1
       (= (component_position q1) 1)
       (behavior_of_event q1 component_failed ignore)
       (behavior_of_event q1 doctor_needed call_doctor)
       (behavior_of_event q1 patient_absent call_patient)
       (behavior_of_event q1 max_q_failed call_doctor)
       (behavior_of_event q1 max_a_failed ignore)
       (= (responses_received q1) 0)
       (= (responses_required q1) 1)
       (= (number_failed_responses q1) 0)
       (= (max_failed_responses q1) 1)
       
       (belongs q1_s1 q1)
       (= (com_act_position q1_s1) 0)
       (= (max_repetitions q1_s1) 1)
       (= (repetitions q1_s1) 0)

       (belongs q1_o1 q1)
       (= (com_act_position q1_o1) 1)
       (= (max_repetitions q1_o1) 1)
       (= (repetitions q1_o1) 0)

       (belongs q1_o2 q1)
       (= (com_act_position q1_o2) 2)
       (= (max_repetitions q1_o2) 1)
       (= (repetitions q1_o2) 0)

       (belongs q1_o3 q1)
       (= (com_act_position q1_o3) 3)
       (= (max_repetitions q1_o3) 1)
       (= (repetitions q1_o3) 0)

       (belongs q1_e1 q1)
       (= (com_act_position q1_e1) 4)
       (= (max_repetitions q1_e1) 1)
       (= (repetitions q1_e1) 0)
       
       (belongs q1_a1 q1)
       (= (com_act_position q1_a1) 5)
       (= (max_repetitions q1_a1) 3)
       (= (repetitions q1_a1) 0)
       (requires_response q1_a1)
       (requires_plausible_response q1_a1)
       (response_failed_behavior q1_a1 response_failed ignore)

       (belongs q1_t1 q1)
       (= (com_act_position q1_t1) 6)
       (= (max_repetitions q1_t1) 1)
       (= (repetitions q1_t1) 0)
       (is_last_of_component q1_t1)
       (is_first_of_component_farewell q1_t1)
;==============================================================================
 	;;q2
       (= (component_position q2) 2)
       (behavior_of_event q2 component_failed ignore)
       (behavior_of_event q2 doctor_needed call_doctor)
       (behavior_of_event q2 patient_absent call_patient)
       (behavior_of_event q2 max_q_failed call_doctor)
       (behavior_of_event q2 max_a_failed ignore)
       (= (responses_received q2) 0)
       (= (responses_required q2) 1)
       (= (number_failed_responses q2) 0)
       (= (max_failed_responses q2) 1)
       
       (belongs q2_s1 q2)
       (= (com_act_position q2_s1) 0)
       (= (max_repetitions q2_s1) 1)
       (= (repetitions q2_s1) 0)

       (belongs q2_o1 q2)
       (= (com_act_position q2_o1) 1)
       (= (max_repetitions q2_o1) 1)
       (= (repetitions q2_o1) 0)

       (belongs q2_o2 q2)
       (= (com_act_position q2_o2) 2)
       (= (max_repetitions q2_o2) 1)
       (= (repetitions q2_o2) 0)

       (belongs q2_e1 q2)
       (= (com_act_position q2_e1) 3)
       (= (max_repetitions q2_e1) 1)
       (= (repetitions q2_e1) 0)
       
       (belongs q2_a1 q2)
       (= (com_act_position q2_a1) 4)
       (= (max_repetitions q2_a1) 3)
       (= (repetitions q2_a1) 0)
       (requires_response q2_a1)
       (requires_plausible_response q2_a1)
       (response_failed_behavior q2_a1 response_failed ignore)

       (belongs q2_t1 q2)
       (= (com_act_position q2_t1) 5)
       (= (max_repetitions q2_t1) 1)
       (= (repetitions q2_t1) 0)
       (is_last_of_component q2_t1)
       (is_first_of_component_farewell q2_t1)
;==============================================================================
 	;;q3
       (= (component_position q3) 3)
       (behavior_of_event q3 component_failed ignore)
       (behavior_of_event q3 doctor_needed call_doctor)
       (behavior_of_event q3 patient_absent call_patient)
       (behavior_of_event q3 max_q_failed call_doctor)
       (behavior_of_event q3 max_a_failed ignore)
       (= (responses_received q3) 0)
       (= (responses_required q3) 1)
       (= (number_failed_responses q3) 0)
       (= (max_failed_responses q3) 1)

       (belongs q3_s1 q3)
       (= (com_act_position q3_s1) 0)
       (= (max_repetitions q3_s1) 1)
       (= (repetitions q3_s1) 0)

       (belongs q3_o1 q3)
       (= (com_act_position q3_o1) 1)
       (= (max_repetitions q3_o1) 1)
       (= (repetitions q3_o1) 0)

       (belongs q3_o2 q3)
       (= (com_act_position q3_o2) 2)
       (= (max_repetitions q3_o2) 1)
       (= (repetitions q3_o2) 0)

       (belongs q3_o3 q3)
       (= (com_act_position q3_o3) 3)
       (= (max_repetitions q3_o3) 1)
       (= (repetitions q3_o3) 0)

       (belongs q3_e1 q3)
       (= (com_act_position q3_e1) 4)
       (= (max_repetitions q3_e1) 1)
       (= (repetitions q3_e1) 0)
       
       (belongs q3_a1 q3)
       (= (com_act_position q3_a1) 5)
       (= (max_repetitions q3_a1) 3)
       (= (repetitions q3_a1) 0)
       (requires_response q3_a1)
       (requires_plausible_response q3_a1)
       (response_failed_behavior q3_a1 response_failed ignore)

       (belongs q3_t1 q3)
       (= (com_act_position q3_t1) 6)
       (= (max_repetitions q3_t1) 1)
       (= (repetitions q3_t1) 0)
       (is_last_of_component q3_t1)
       (is_first_of_component_farewell q3_t1)
;==============================================================================
 	;;q4
       (= (component_position q4) 4)
       (behavior_of_event q4 component_failed ignore)
       (behavior_of_event q4 doctor_needed call_doctor)
       (behavior_of_event q4 patient_absent call_patient)
       (behavior_of_event q4 max_q_failed call_doctor)
       (behavior_of_event q4 max_a_failed ignore)
       (= (responses_received q4) 0)
       (= (responses_required q4) 1)
       (= (number_failed_responses q4) 0)
       (= (max_failed_responses q4) 1)
       
       (belongs q4_s1 q4)
       (= (com_act_position q4_s1) 0)
       (= (max_repetitions q4_s1) 1)
       (= (repetitions q4_s1) 0)

       (belongs q4_o1 q4)
       (= (com_act_position q4_o1) 1)
       (= (max_repetitions q4_o1) 1)
       (= (repetitions q4_o1) 0)

       (belongs q4_o2 q4)
       (= (com_act_position q4_o2) 2)
       (= (max_repetitions q4_o2) 1)
       (= (repetitions q4_o2) 0)

       (belongs q4_e1 q4)
       (= (com_act_position q4_e1) 3)
       (= (max_repetitions q4_e1) 1)
       (= (repetitions q4_e1) 0)
       
       (belongs q4_a1 q4)
       (= (com_act_position q4_a1) 4)
       (= (max_repetitions q4_a1) 3)
       (= (repetitions q4_a1) 0)
       (requires_response q4_a1)
       (requires_plausible_response q4_a1)
       (response_failed_behavior q4_a1 response_failed ignore)

       (belongs q4_t1 q4)
       (= (com_act_position q4_t1) 5)
       (= (max_repetitions q4_t1) 1)
       (= (repetitions q4_t1) 0)
       (is_last_of_component q4_t1)
       (is_first_of_component_farewell q4_t1)

;==============================================================================
 	;;q5
       (= (component_position q5) 5)
       (behavior_of_event q5 component_failed ignore)
       (behavior_of_event q5 doctor_needed call_doctor)
       (behavior_of_event q5 patient_absent call_patient)
       (behavior_of_event q5 max_q_failed call_doctor)
       (behavior_of_event q5 max_a_failed ignore)
       (= (responses_received q5) 0)
       (= (responses_required q5) 1)
       (= (number_failed_responses q5) 0)
       (= (max_failed_responses q5) 1)
       
       (belongs q5_s1 q5)
       (= (com_act_position q5_s1) 0)
       (= (max_repetitions q5_s1) 1)
       (= (repetitions q5_s1) 0)

       (belongs q5_o1 q5)
       (= (com_act_position q5_o1) 1)
       (= (max_repetitions q5_o1) 1)
       (= (repetitions q5_o1) 0)

       (belongs q5_o2 q5)
       (= (com_act_position q5_o2) 2)
       (= (max_repetitions q5_o2) 1)
       (= (repetitions q5_o2) 0)

       (belongs q5_o3 q5)
       (= (com_act_position q5_o3) 3)
       (= (max_repetitions q5_o3) 1)
       (= (repetitions q5_o3) 0)

       (belongs q5_e1 q5)
       (= (com_act_position q5_e1) 4)
       (= (max_repetitions q5_e1) 1)
       (= (repetitions q5_e1) 0)
       
       (belongs q5_a1 q5)
       (= (com_act_position q5_a1) 5)
       (= (max_repetitions q5_a1) 3)
       (= (repetitions q5_a1) 0)
       (requires_response q5_a1)
       (requires_plausible_response q5_a1)
       (response_failed_behavior q5_a1 response_failed ignore)

       (belongs q5_t1 q5)
       (= (com_act_position q5_t1) 6)
       (= (max_repetitions q5_t1) 1)
       (= (repetitions q5_t1) 0)
       (is_last_of_component q5_t1)
       (is_first_of_component_farewell q5_t1)
;==============================================================================
 	;;q6
       (= (component_position q6) 6)
       (behavior_of_event q6 component_failed ignore)
       (behavior_of_event q6 doctor_needed call_doctor)
       (behavior_of_event q6 patient_absent call_patient)
       (behavior_of_event q6 max_q_failed call_doctor)
       (behavior_of_event q6 max_a_failed ignore)
       (= (responses_received q6) 0)
       (= (responses_required q6) 1)
       (= (number_failed_responses q6) 0)
       (= (max_failed_responses q6) 1)

       
       (belongs q6_s1 q6)
       (= (com_act_position q6_s1) 0)
       (= (max_repetitions q6_s1) 1)
       (= (repetitions q6_s1) 0)

       (belongs q6_o1 q6)
       (= (com_act_position q6_o1) 1)
       (= (max_repetitions q6_o1) 1)
       (= (repetitions q6_o1) 0)

       (belongs q6_o2 q6)
       (= (com_act_position q6_o2) 2)
       (= (max_repetitions q6_o2) 1)
       (= (repetitions q6_o2) 0)

       (belongs q6_o3 q6)
       (= (com_act_position q6_o3) 3)
       (= (max_repetitions q6_o3) 1)
       (= (repetitions q6_o3) 0)

       (belongs q6_e1 q6)
       (= (com_act_position q6_e1) 4)
       (= (max_repetitions q6_e1) 1)
       (= (repetitions q6_e1) 0)
       
       (belongs q6_a1 q6)
       (= (com_act_position q6_a1) 5)
       (= (max_repetitions q6_a1) 3)
       (= (repetitions q6_a1) 0)
       (requires_response q6_a1)
       (requires_plausible_response q6_a1)
       (response_failed_behavior q6_a1 response_failed ignore)

       (belongs q6_t1 q6)
       (= (com_act_position q6_t1) 6)
       (= (max_repetitions q6_t1) 1)
       (= (repetitions q6_t1) 0)
       (is_last_of_component q6_t1)
       (is_first_of_component_farewell q6_t1)
;==============================================================================
 	;;q7
       (= (component_position q7) 7)
       (behavior_of_event q7 component_failed ignore)
       (behavior_of_event q7 doctor_needed call_doctor)
       (behavior_of_event q7 patient_absent call_patient)
       (behavior_of_event q7 max_q_failed call_doctor)
       (behavior_of_event q7 max_a_failed ignore)
       (= (responses_received q7) 0)
       (= (responses_required q7) 1)
       (= (number_failed_responses q7) 0)
       (= (max_failed_responses q7) 1)
       
       (belongs q7_s1 q7)
       (= (com_act_position q7_s1) 0)
       (= (max_repetitions q7_s1) 1)
       (= (repetitions q7_s1) 0)

       (belongs q7_o1 q7)
       (= (com_act_position q7_o1) 1)
       (= (max_repetitions q7_o1) 1)
       (= (repetitions q7_o1) 0)

       (belongs q7_o2 q7)
       (= (com_act_position q7_o2) 2)
       (= (max_repetitions q7_o2) 1)
       (= (repetitions q7_o2) 0)

       (belongs q7_o3 q7)
       (= (com_act_position q7_o3) 3)
       (= (max_repetitions q7_o3) 1)
       (= (repetitions q7_o3) 0)

       (belongs q7_e1 q7)
       (= (com_act_position q7_e1) 4)
       (= (max_repetitions q7_e1) 1)
       (= (repetitions q7_e1) 0)
       
       (belongs q7_a1 q7)
       (= (com_act_position q7_a1) 5)
       (= (max_repetitions q7_a1) 3)
       (= (repetitions q7_a1) 0)
       (requires_response q7_a1)
       (requires_plausible_response q7_a1)
       (response_failed_behavior q7_a1 response_failed ignore)

       (belongs q7_t1 q7)
       (= (com_act_position q7_t1) 6)
       (= (max_repetitions q7_t1) 1)
       (= (repetitions q7_t1) 0)
       (is_last_of_component q7_t1)
       (is_first_of_component_farewell q7_t1)
;==============================================================================
 	;;q8
       (= (component_position q8) 8)
       (behavior_of_event q8 component_failed ignore)
       (behavior_of_event q8 doctor_needed call_doctor)
       (behavior_of_event q8 patient_absent call_patient)
       (behavior_of_event q8 max_q_failed call_doctor)
       (behavior_of_event q8 max_a_failed ignore)
       (= (responses_received q8) 0)
       (= (responses_required q8) 1)
       (= (number_failed_responses q8) 0)
       (= (max_failed_responses q8) 1)

       
       (belongs q8_s1 q8)
       (= (com_act_position q8_s1) 0)
       (= (max_repetitions q8_s1) 1)
       (= (repetitions q8_s1) 0)

       (belongs q8_o1 q8)
       (= (com_act_position q8_o1) 1)
       (= (max_repetitions q8_o1) 1)
       (= (repetitions q8_o1) 0)

       (belongs q8_o2 q8)
       (= (com_act_position q8_o2) 2)
       (= (max_repetitions q8_o2) 1)
       (= (repetitions q8_o2) 0)

       (belongs q8_o3 q8)
       (= (com_act_position q8_o3) 3)
       (= (max_repetitions q8_o3) 1)
       (= (repetitions q8_o3) 0)

       (belongs q8_o4 q8)
       (= (com_act_position q8_o4) 4)
       (= (max_repetitions q8_o4) 1)
       (= (repetitions q8_o4) 0)

       (belongs q8_e1 q8)
       (= (com_act_position q8_e1) 5)
       (= (max_repetitions q8_e1) 1)
       (= (repetitions q8_e1) 0)
       
       (belongs q8_a1 q8)
       (= (com_act_position q8_a1) 6)
       (= (max_repetitions q8_a1) 3)
       (= (repetitions q8_a1) 0)
       (requires_response q8_a1)
       (requires_plausible_response q8_a1)
       (response_failed_behavior q8_a1 response_failed ignore)

       (belongs q8_t1 q8)
       (= (com_act_position q8_t1) 7)
       (= (max_repetitions q8_t1) 1)
       (= (repetitions q8_t1) 0)
       (is_last_of_component q8_t1)
       (is_first_of_component_farewell q8_t1)
;==============================================================================
 	;;q9
       (= (component_position q9) 9)
       (behavior_of_event q9 component_failed ignore)
       (behavior_of_event q9 doctor_needed call_doctor)
       (behavior_of_event q9 patient_absent call_patient)
       (behavior_of_event q9 max_q_failed call_doctor)
       (behavior_of_event q9 max_a_failed ignore)
       (= (responses_received q9) 0)
       (= (responses_required q9) 1)
       (= (number_failed_responses q9) 0)
       (= (max_failed_responses q9) 1)
       
       (belongs q9_s1 q9)
       (= (com_act_position q9_s1) 0)
       (= (max_repetitions q9_s1) 1)
       (= (repetitions q9_s1) 0)

       (belongs q9_o1 q9)
       (= (com_act_position q9_o1) 1)
       (= (max_repetitions q9_o1) 1)
       (= (repetitions q9_o1) 0)

       (belongs q9_o2 q9)
       (= (com_act_position q9_o2) 2)
       (= (max_repetitions q9_o2) 1)
       (= (repetitions q9_o2) 0)

       (belongs q9_o3 q9)
       (= (com_act_position q9_o3) 3)
       (= (max_repetitions q9_o3) 1)
       (= (repetitions q9_o3) 0)

       (belongs q9_o4 q9)
       (= (com_act_position q9_o4) 4)
       (= (max_repetitions q9_o4) 1)
       (= (repetitions q9_o4) 0)

       (belongs q9_e1 q9)
       (= (com_act_position q9_e1) 5)
       (= (max_repetitions q9_e1) 1)
       (= (repetitions q9_e1) 0)
       
       (belongs q9_a1 q9)
       (= (com_act_position q9_a1) 6)
       (= (max_repetitions q9_a1) 3)
       (= (repetitions q9_a1) 0)
       (requires_response q9_a1)
       (requires_plausible_response q9_a1)
       (response_failed_behavior q9_a1 response_failed ignore)

       (belongs q9_t1 q9)
       (= (com_act_position q9_t1) 7)
       (= (max_repetitions q9_t1) 1)
       (= (repetitions q9_t1) 0)
       (is_last_of_component q9_t1)
       (is_first_of_component_farewell q9_t1)
;==============================================================================
  	;;q10
       (= (component_position q10) 10)
       (behavior_of_event q10 component_failed ignore)
       (behavior_of_event q10 doctor_needed call_doctor)
       (behavior_of_event q10 patient_absent call_patient)
       (behavior_of_event q10 max_q_failed call_doctor)
       (behavior_of_event q10 max_a_failed ignore)
       (= (responses_received q10) 0)
       (= (responses_required q10) 1)
       (= (number_failed_responses q10) 0)
       (= (max_failed_responses q10) 1)

       
       (belongs q10_s1 q10)
       (= (com_act_position q10_s1) 0)
       (= (max_repetitions q10_s1) 1)
       (= (repetitions q10_s1) 0)

       (belongs q10_o1 q10)
       (= (com_act_position q10_o1) 1)
       (= (max_repetitions q10_o1) 1)
       (= (repetitions q10_o1) 0)

       (belongs q10_o2 q10)
       (= (com_act_position q10_o2) 2)
       (= (max_repetitions q10_o2) 1)
       (= (repetitions q10_o2) 0)

       (belongs q10_o3 q10)
       (= (com_act_position q10_o3) 3)
       (= (max_repetitions q10_o3) 1)
       (= (repetitions q10_o3) 0)

       (belongs q10_e1 q10)
       (= (com_act_position q10_e1) 4)
       (= (max_repetitions q10_e1) 1)
       (= (repetitions q10_e1) 0)
       
       (belongs q10_a1 q10)
       (= (com_act_position q10_a1) 5)
       (= (max_repetitions q10_a1) 3)
       (= (repetitions q10_a1) 0)
       (requires_response q10_a1)
       (requires_plausible_response q10_a1)
       (response_failed_behavior q10_a1 response_failed ignore)

       (belongs q10_t1 q10)
       (= (com_act_position q10_t1) 6)
       (= (max_repetitions q10_t1) 1)
       (= (repetitions q10_t1) 0)
       (is_last_of_component q10_t1)
       (is_first_of_component_farewell q10_t1)
  )     
(:goal	(and (interaction_finished)
)))
