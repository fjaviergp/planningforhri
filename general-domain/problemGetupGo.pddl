(define (problem getupandgo)
(:domain interaction)
(:objects
	;;Events raised by the planner
	component_failed response_failed max_q_failed max_a_failed - event
	;;External events
	;;If I am not wrong for every component we must state what to do if
	;;any of these event happens, not done yet for some of them (most of
	;;these case correspond to the "ignore" behavior)
	doctor_needed patient_absent patient_seated patient_far - event
	;;Things the robot can do
	call_doctor call_patient ignore speak_louder show_video - behavior
	finish_and_call_doctor move_to_intro move_to_start - behavior
	check_patient_standing  check_patient_near_chair - behavior
	check_patient_seated wait_for_start restart_component - behavior
	check_patient_far - behavior
	
	;First interaction: waiting for the test to start
	waiting_for_start - component
	;;This must be shown but not said, it is controled by the low level
	nothing - com_act

	;;Moving to the right position
	moving - component
	nothing1 - com_act

	;;Intro, first part: checking audio
	checking_sound - component
	intro2_s1 intro2_a1 intro2_a2 intro2_a3 intro2_a4 intro3 - com_act

	;;Intro, second part: showing video and asking the patient to stand up
	standing - component
	intro4 intro5 intro6 intro7 intro8 intro9 - com_act

	;;Intro, third part, moving to the right position
	moving2 - component
	nothing2  - com_act

	;;Checking patient near the chair
	checking_near - component
	intro10 intro11 - com_act

	;;Starting the test
	running_test - component
	q1_a1 nothing3 nothing4 - com_act

)
(:init 
       ;;General
       (can_continue)
       (= (number_failed_components) 0)
       (= (max_failed_components) 2)
       (= (number_of_components) 3)
       (= (current_component) 0)
       (component_failed_event component_failed)
       (max_components_failed_event max_q_failed)
       (response_failed_event response_failed)
       (max_responses_failed_event max_a_failed)
       (= (current_com_act) 0)
;=============================================================================
	;;Again similar to the Barthel test, but we join two components together
	;;here
       (= (component_position standing) 0)
       (behavior_of_event standing component_failed call_doctor)
       (behavior_of_event standing doctor_needed call_doctor)
       (behavior_of_event standing patient_absent call_patient)
       (behavior_of_event standing max_q_failed call_doctor)
       (behavior_of_event standing max_a_failed call_doctor)
       (= (responses_received standing) 0)
       (= (responses_required standing) 1)
       (= (number_failed_responses standing) 0)
       (= (max_failed_responses standing) 1)    

       (belongs intro4 standing)
       (= (com_act_position intro4) 0)
       (= (max_repetitions intro4) 1)
       (= (repetitions intro4) 0)


       (belongs intro6 standing)
       (= (com_act_position intro6) 1)
       (= (max_repetitions intro6) 1)
       (= (repetitions intro6) 0)
       ;;(requires_response intro6)
       ;;(requires_plausible_response intro6)
       ;;(requires_correct_response intro6)
       ;;(response_failed_behavior intro6 response_failed show_video)
       (requires_behavior intro6)
       (required_behavior intro6 show_video)

       (belongs intro8 standing)
       (= (com_act_position intro8) 2)
       (= (max_repetitions intro8) 1)
       (= (repetitions intro8) 0)
       (is_first_of_component_farewell intro8)
       
       (belongs intro9 standing)
       (= (com_act_position intro9) 3)
       (= (max_repetitions intro9) 2)
       (= (repetitions intro9) 0)
       (requires_behavior intro9)
       ;;In this case the response is a pose: the patient must be standing up
       (required_behavior intro9 check_patient_standing)
       (is_last_of_component intro9)
       (requires_response intro9)
       (requires_plausible_response intro9)
       (requires_correct_response intro9)
       ;;I think this is redundant as if this is failed responses_failed=2
       ;;check and remove if needed
       (response_failed_behavior intro9 response_failed call_doctor)
       
;=============================================================================
	;;Moving to the position
       (= (component_position moving2) 1)
       (behavior_of_event moving2 component_failed call_doctor)
       (behavior_of_event moving2 doctor_needed call_doctor)
       (behavior_of_event moving2 patient_absent ignore)
       (behavior_of_event moving2 max_q_failed call_doctor)
       (behavior_of_event moving2 max_a_failed call_doctor)
       (= (responses_received moving2) 0)
       (= (responses_required moving2) 0)
       (= (number_failed_responses moving2) 0)
       (= (max_failed_responses moving2) 1)
       
       (belongs nothing2 moving2)
       (= (com_act_position nothing2) 0)
       (= (max_repetitions  nothing2) 1)
       (= (repetitions  nothing2) 0)
       (requires_behavior nothing2)
       (required_behavior nothing2 move_to_start)
       (is_last_of_component nothing2)
       (is_first_of_component_farewell nothing2)

;=============================================================================
       (= (component_position checking_near) 2)
       (behavior_of_event checking_near component_failed call_doctor)
       (behavior_of_event checking_near doctor_needed call_doctor)
       (behavior_of_event checking_near patient_absent ignore)
       ;;if the patient seats again, we call him/her
       (behavior_of_event checking_near patient_seated call_patient)
       (behavior_of_event checking_near max_q_failed call_doctor)
       (behavior_of_event checking_near max_a_failed call_doctor)
       (= (responses_received checking_near) 0)
       (= (responses_required checking_near) 1)
       (= (number_failed_responses checking_near) 0)
       (= (max_failed_responses checking_near) 1)

       (belongs intro10 checking_near)
       (= (com_act_position intro10) 0)
       (= (max_repetitions  intro10) 1)
       (= (repetitions  intro10) 0)

       (belongs intro11 checking_near)
       (= (com_act_position intro11) 1)
       (= (max_repetitions  intro11) 2)
       (= (repetitions  intro11) 0)
       (requires_behavior intro11)
       (required_behavior intro11 check_patient_near_chair)
       ;;In this case the response is a pose: the patient must be near the chair
       (requires_response intro11)
       (requires_plausible_response intro11)
       (requires_correct_response intro11)
       (is_last_of_component intro11)
       (is_first_of_component_farewell intro11)
       ;;I think this is redundant as if this is failed responses_failed=1
       ;;check and remove if needed
       (response_failed_behavior intro11 response_failed call_doctor)


;=============================================================================
       (= (component_position running_test) 3)
       (behavior_of_event running_test component_failed call_doctor)
       (behavior_of_event running_test doctor_needed call_doctor)
       (behavior_of_event running_test patient_absent call_patient)
       (behavior_of_event running_test max_q_failed call_doctor)
       (behavior_of_event running_test max_a_failed call_doctor)
       (behavior_of_event running_test patient_far call_patient)
       (behavior_of_event running_test patient_seated ignore)
       (= (responses_received running_test) 0)
       (= (responses_required running_test) 2)
       (= (number_failed_responses running_test) 0)
       (= (max_failed_responses running_test) 2)

       (belongs q1_a1 running_test)
       (= (com_act_position q1_a1) 0)
       (= (max_repetitions  q1_a1) 1)
       (= (repetitions  q1_a1) 0)
       
       (belongs nothing3 running_test)
       (= (com_act_position nothing3) 1)
       (= (max_repetitions  nothing3) 1)
       (= (repetitions  nothing3) 0)
       (requires_behavior nothing3)
       (required_behavior nothing3 check_patient_seated)
       ;;In this case the response is a pose: the patient must be seated
       (requires_response nothing3)
       (requires_plausible_response nothing3)
       (requires_correct_response nothing3)
       (response_failed_behavior nothing3 response_failed restart_component)

       (belongs nothing4 running_test)
       (= (com_act_position nothing4) 2)
       (= (max_repetitions  nothing4) 1)
       (= (repetitions  nothing4) 0)
       (requires_behavior nothing4)
       (required_behavior nothing4 check_patient_far)
       ;;In this case the response is a pose: the patient must be seated
       (requires_response nothing4)
       (requires_plausible_response nothing4)
       (requires_correct_response nothing4)
       (is_last_of_component nothing4)
       (is_first_of_component_farewell nothing4)
       (response_failed_behavior nothing4 response_failed restart_component)
)     

(:goal	(and (interaction_finished)
))
)

