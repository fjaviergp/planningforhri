;; -----------------------------------------------------------------------------------------------------------------
;; General Interaction domain for Social Robotics 
;;
;; This domain is designed to control the interaction between a robot and a person 
;; Configuration parameters will be read from the database.
;;
;;
;; Authors: Raquel Fuentetaja && Angel Garcia Olaya && Javier Garcia Polo
;;
;; V1.0. March 2017: Allows Barthel, NaoTherapist and Get up and Go
;; V1.1. June 2019: Allows Minimental: questions with hint, going back to a previous step
;; -----------------------------------------------------------------------------------------------------------------

(define (domain interaction)
  (:requirements :strips :typing :fluents :negative-preconditions :equality)
  
  (:types
   component - object
   ;; every interaction part will be a component (questions in tests are components), even if the user is not
   ;; asked anything.

   com_act - object
   ;; something that the robot says or shows on the screen. 
   
   event - object
   ;; something internal or external that needs the system to react to solve it before going on with the test

   behaviour - object)
   ;; behaviours to react to events

  (:predicates

   (interaction_configured)
   ;; the interaction has been configured

   (interaction_finished)
   ;; interaction finished
   
   (component_finished ?q - component)
   
   (belongs ?l - com_act ?q - component)
   ;; this com_act belongs to this component

   (requires_response ?l - com_act)
   ;; after saying/showing this, the patient must answer or do something
   
   (is_first_of_component_farewell ?l - com_act)
   ;; in this com_act the farewell begins (so the flow will jump to this com_act if all the answers have been
   ;; collected. It must only appear in components with multiple ways to get the answer or with more than one answer
   ;; required

   (is_last_of_component ?l - com_act)
   ;; this is the last thing to say before going to the next component

   (has_restoration_points ?q - component)
   ;; this component has at least one restoration point to come back in case of interruption
   
   (is_restoration_point ?l - com_act)
   ;; the flow must come back to this com_act if the action is interrumpted before the next restoration point is reached

   (current_restoration_point ?l - com_act)
   ;; this is the current restoration point to come back

   (skip_response ?q - component)
   ;; the patient has marked the component to answer it later (or he/she exhausted the max number of tries). Notice that
   ;; this predicate can be external or internal
   
   (call_supervisor_at_end)
   ;; at the end of the test the robot must call the doctor. Now it is only used to fill the skipped components (the
   ;; ones marked for later answer)
   
   (requires_response_confirmation ?q - component)
   ;; the patient needs to confirm the answer to this component

   (waiting_for_response_confirmation)
   ;; the patient has answered the component and it can be confirmed if needed

   (response_change_required)
   ;; the patient didn't confirm the answer and it must be changed

   (waiting_for_response_change)
   ;; we are waiting for the patient to provide the new answer for this component
   
   (requires_behavior ?l - com_act)
   ;; before saying this com_act the robot must execute some behavior (for example speak louder, or activate the pose
   ;; detection)

   (required_behavior ?l - com_act ?b - behavior)
   ;; the behavior that must be executed before saying this com_act

   (has_alternative ?q - component)
   ;; there is an alernative to this component if it fails

   (alternative ?a - component ?q - component)
   ;; ?a is the alternative to ?q. Notice that (component_position ?a) must be undefined


   
   ;; ---------------------- PREDICATES RELATED TO THE RESPONSE  ----------------------

   (waiting_for_response)
   ;; the robot must stop until the response is received (by any way)

   (response_received ?q - component)
   ;; the patient has answered this component, this does not imply that the answer is correct. If not validation is
   ;; needed, this is all we need to proceed to the next component

   (requires_plausible_response ?l - com_act)
   ;; the answer to this component must be validated. This only implies the answer must be plausible, but we are not
   ;; requiring a specific correct answer
   
   (response_is_plausible ?l - com_act)
   ;; the answer to this component is plausible (but is not the correct one)

   (requires_correct_response ?l - com_act)
   ;; if the answer is not the right one, we are not yet done. Depending on the state we will try again or fail
   
   (response_is_correct ?l - com_act)
   ;; the answer to this component is correct
   
   (is_hint ?l - com_act ?lh - com_act)
   ;; ?lh is a hint to help the patient to say the right answer after ?l has been asked
   
   (has_hints ?q - component)
   ;; this component has at least one hint


   

   ;; ---------------------- PREDICATES FOR EXTERNAL EVENTS ----------------------
   
   (can_continue)
   ;; no external or internal event prevent the test to continue. The low level will set this to false in case of any
   ;; event, but it can be also deleted by actions raising internal events
   
   (behavior_of_event ?q - component ?e - event ?b - behavior)
   ;; what to do in this event for this component. Notice that this prevents a generic behavior for this event, but it
   ;; is more flexible.

   (response_failed_behavior ?l - com_act ?e - event ?b - behavior)
   ;; what to do in this event occurs after saying this com_act. It is used to act when the patient does not answer or
   ;; the answer is not plausible/correct
   
   (detected_event ?e - event)
   ;; this specific event has been detected. Most of these predicates are generated by the low level. But some of them
   ;; (max numbers of fails for example) are generated by the planer
   
   (response_failed_event ?e - event)
   ;; the event raised when the user is not able to correctly answer when required. It would be easier to use constants
   ;; for it, but Pelea does not currently support constants. See the commented constants field below
   
   (max_responses_failed_event ?e - event)
   ;; the event raised when the user has not been able to provide n answers to this component. It is mainly used in
   ;; components which require multiple answers
   
   (component_failed_event ?e - event)
   ;; the event produced when a component is failed/skipped (this predicate is added because Pelea doesn't support
   ;; constants. It would be easier to use constants for it. See the commented constants field below
   
   (max_components_failed_event ?e - event)
   ;; the event produced when the maximum number of components have been failed/skipped

   )


   ;; ---------------------- CONSTANTS ----------------------  
   ;; Current planning-execution architecture Pelea doesn't support constants but having them would make things easier
   ;; regarding the events and behaviors to include:  repetition_asked - event repeat_component - behavior
   ;; (:constants
   ;; 	component_failed - event
   ;; 	response_failed - event
   ;; 	max_components_failed - event
   ;;	max_answers_failed - event
   ;; 	doctor_needed  - event  
   ;; 	patient_absent - event
   ;;	want_change - event ;;This is raised when the user wants to change the answer to a component
   
   ;; 	call_doctor  - behavior
   ;;	call_patient - behavior
   ;; 	ignore - behavior
   ;; 	speak_louder - behavior
   ;;	show_video - behavior
   ;;	ask_new_answer - behavior
  
   ;;;; this is the behavior to want_change. The low level must make true: (requires_response_confirmation
   ;;;; ?q)(waiting_for_response_confirmation)(response_change_required) so offer-answer-change action is executed.
  
   ;; )

   ;; ---------------------- FUNCTIONS ----------------------  
  (:functions
   (com_act_position ?l - com_act)
   ;; the position of this com_act in the sequence of com_acts of a component

   (current_com_act)
   ;; the current com_act for the current component (we are using this fluent for every component instead of having one
   ;; fluent for each component)

   (max_repetitions ?l - com_act)
   ;; the maximum times this com_act can be repeated until we raise the response_failed event
   
   (repetitions ?l - com_act)
   ;; the number of times this com_act has been repeated
   
   (responses_required ?q - component)
   ;; number of answers this component requires. A component can need more than one answer
   
   (responses_received ?q - component)
   ;; the number of answers already got for this component.
   
   (number_failed_responses ?q - component)
   ;; number of missing/wrong not plausible answers for this component. Notice hat an answer is not marked as failed
   ;; until its maximum repetitions re reached. This is for components where the patient is asked to answer more than
   ;; one time or where there are alternative components.
   
   (max_failed_responses ?q - component)
   ;; max number of missing/wrong not plausible answers before max_failed_answers event occurs
   
   (number_failed_components)
   ;; number of failed or skipped components

   (max_failed_components)
   ;; max number of failed or skipped components before the max_components_failed event occurs

   (current_component)
   ;; the current component we are in.

   (component_position ?q)
   ;; the position of this component in the interaction

   (number_of_components)
   ;; the number of components of this test, including any introductory one. It can be calculated as
   ;; max(component_position)
 )

  ;; -----------------------------------------------------------------------------------------------------------------
  ;; ACTIONS In order to check if an action has been correctly executed, the effects of every action are marked with a
  ;; tag to distinguish which ones reflect changes in the environment (and must come from the Inner World) and which
  ;; ones represent the "logic" of the test and must be generated by the executor. The second ones will always be
  ;; produced, while the first ones depend on the external world and may fail if the action is not executed as expected.
  ;; We will mark the first ones with "external" and the second ones with "automatic" It is very important to notice
  ;; that most "automatic" effects must be only applied after all the external effects have been already checked. If any
  ;; external fails this means the action failed and their automatic effects shouldn't be applied. The exceptio are the
  ;; "automatic-always" effects that must be applied despite the action fails or not as they control the flow of the
  ;; test.
  ;; -----------------------------------------------------------------------------------------------------------------

  ;; this action is always the first unless an event is produced
  (:action configure-interaction
     :parameters    ()
     :precondition  (and
     		      (can_continue)
		      ;; control
                      (not (interaction_configured))
		      )
     :effect 	(and
		 (interaction_configured)
		 ;; automatic (it will be external if we need feedback from the Inner Model saying that everything is
		 ;; ready to start the test)
		 ))


  ;; used for everyhing that must be said, but for the goodbye of each component
  ;; if it must be both said & shown the say-show action will be used instead (pending)
  (:action communicate
     :parameters    (?q - component ?l - com_act)
     :precondition  (and
		      ;; common to all
		     (can_continue)
		     ;; specific
		     (interaction_configured)
		     (belongs ?l ?q)
		     (not (response_received ?q))
		     (= (com_act_position ?l) (current_com_act))
		     (<  (repetitions ?l) (max_repetitions ?l))
		     (= (component_position ?q) (current_component))
		     (not (requires_behavior ?l))
		     (not (waiting_for_response))
		     (not (waiting_for_response_confirmation)))
     
      :effect (and (increase (repetitions ?l) 1)
                 ;; external (we need to be sure this com_act has been said)
		 (when (is_restoration_point ?l) (current_restoration_point ?l))
		 ;; automatic
		 (when (requires_response ?l) (waiting_for_response))
		 ;;automatic
		 (when (not (requires_response ?l)) (increase (current_com_act) 1))
		 ;;automatic
		 ))
  
  ;; used to execute a behavior when a given com_act is said
  (:action communicate-execute-behavior
     :parameters    (?q - component ?l - com_act ?b - behavior)
     :precondition  (and
		     ;;common to all
		     (can_continue)
		     ;;specific
      		     (interaction_configured)
		     (belongs ?l ?q)
		     (not (response_received ?q))
		     (= (com_act_position ?l) (current_com_act))
		     (< (repetitions ?l) (max_repetitions ?l))
		     (= (component_position ?q) (current_component))
		     (requires_behavior ?l)
		     (required_behavior ?l ?b)
		     (not (waiting_for_response))
      		     (not (waiting_for_response_confirmation)))
     :effect 	(and
		 (increase (repetitions ?l) 1)
		 ;; external (we need to be sure this com_act has been said)
		 (when (requires_response ?l) (waiting_for_response))
		 ;; automatic
	       	 (when (is_restoration_point ?l) (current_restoration_point ?l))
		 ;; automatic
		 (when (not (requires_response ?l)) (increase (current_com_act) 1))
		 ;; automatic
		 ))	
  
  ;; action to receive any kind of answer from the patient, it includes pose-detection   
  (:action receive-response
     :parameters    (?q - component ?l - com_act)
     :precondition  (and
		      ;;common to all
		      (can_continue)
		      ;;specific
		      (waiting_for_response)
		      (= (current_com_act) (com_act_position ?l))
		      (requires_response ?l)
		      (= (component_position ?q) (current_component))
		      (belongs ?l ?q))
     :effect 	(and
		 (response_received ?q)
		 ;; external
		 (not (waiting_for_response))
		 ;; automatic-always (to repeat the last com_act if possible) these two effects are automatic given that
		 ;; response_received has been already calculated
		 (when (not (requires_plausible_response ?l))
		     (and
		      (not (response_received ?q))
		      ;; automatic-always
		      (increase (responses_received ?q) 1)
		      ;; automatic
		      (increase (current_com_act) 1)))
		      ;; automatic
		 ))

  ;; this action is executed when we need not only to receive the answer but we must check that it is plausible, 
  ;; for example if we are asking about what day is today the patient must answer a day, even if the day is not
  ;; correct. If the answer is not plausible we will repeat or raise the response_failed event, depending on the
  ;; current state.
  (:action validate-response-plausability
     :parameters (?q - component ?l - com_act)
     :precondition (and 
     		   ;; common to all
		   (can_continue)
		   ;; specific
		   (response_received ?q)
		   (requires_plausible_response ?l)
		   (not (requires_correct_response ?l)) 
		   (= (component_position ?q) (current_component))
		   (belongs ?l ?q)
		   (= (current_com_act) (com_act_position ?l))
		   (not (has_hints ?q))
		   )
      :effect (and
	       (not (response_received ?q))
	       ;; automatic-always
	       (increase (responses_received ?q) 1)
	       ;; external (depends on the answer being plausible)
	       (response_is_plausible ?l)
	       ;; external (depends on the answer being plausible)
	       (increase (current_com_act) 1)
	       ;; automatic
	       ))

  
  ;; this action is the alternative to validate-plausability. In this case the component must be correct, if not,
  ;; we will repeat or raise the response_failed event.
  (:action validate-response-correction
     :parameters (?q - component ?l - com_act)
     :precondition (and 
     		   ;;common to all
		   (can_continue)
		   ;;specific
   		   (response_received ?q)
		   (requires_plausible_response ?l)
		   (requires_correct_response ?l)
       	      	   (= (component_position ?q) (current_component))
		   (belongs ?l ?q)
		   (= (current_com_act) (com_act_position ?l))
		   (not (has_hints ?q))

		   )
      :effect (and
	       (not (response_received ?q))
	       ;; automatic-always
	       (increase (responses_received ?q) 1)
	       ;; external (depends on the answer being correct)
	       (response_is_correct ?l)
	       ;; external (depends on the answer being correct)
	       (increase (current_com_act) 1)
	       ;; automatic
	       ))
  

   ;; this action gives a hint to the patient when her answer is plausible but is not really what we expect
   ;; as in Minimental q6. The low level must validate the component and if it is plausible give the hint before
   ;; asking again
 (:action validate-response-plausability-and-hint
     :parameters (?q - component ?l1 - com_act ?lh - com_act)
     :precondition (and 
     		   ;;common to all
		   (can_continue)
		   ;;specific
		   (response_received ?q)
		   (requires_plausible_response ?l1)
		   (not (requires_correct_response ?l1)) 
		   (= (component_position ?q) (current_component))
		   (belongs ?l1 ?q)
		   (= (current_com_act) (com_act_position ?l1))
		   (has_hints ?q)
		   (is_hint ?l1 ?lh)
		   )
      :effect (and
	       (not (response_received ?q))
	       ;; automatic-always
	       (increase (responses_received ?q) 1)
	       ;; external (depends on the answer being plausible)
	       (response_is_plausible ?l1)
	       ;; external (depends on the answer being plausible)
	       (increase (current_com_act) 1)
	       ;; automatic
	       ))

  ;; equivalent to the check-is-correct action but if not correct it should give a hint to the user. Notice we
  ;; are giving the hint even if the answer is not plausible. If we want to differentiate, we need to add a new
  ;; action with an extra precondition (response_is_plausible ?l) and add (not (response_is_plausible ?l)) to this one
 (:action validate-response-correction-and-hint
     :parameters (?q - component ?l1 - com_act ?lh - com_act)
     :precondition (and 
     		   ;;common to all
		   (can_continue)
		   ;;specific
		   (response_received ?q)
		   (requires_plausible_response ?l1)
		   (requires_correct_response ?l1)
		   (= (component_position ?q) (current_component))
		   (belongs ?l1 ?q)
		   (= (current_com_act) (com_act_position ?l1))
		   (has_hints ?q)
		   (is_hint ?l1 ?lh)
		   )
      :effect (and
	       (not (response_received ?q))
	       ;; automatic-always
	       (increase (responses_received ?q) 1)
	       ;; external (depends on the answer being plausible)
	       (response_is_plausible ?l1)
	       ;; external (depends on the answer being plausible)
	       (increase (current_com_act) 1)
	       ;; automatic
	       ))

  ;; this action is executed in components where there are alternative ways of getting the answer. The
  ;; system will try them in order, once one of them obtains the answer, the flow jumps to the farewell.
  (:action prepare-farewell
     :parameters (?q - component ?l - com_act)
     :precondition (and 
			(can_continue)
			(> (responses_required ?q) 0)
			;; to avoid executing it in interactions that do not need answers if not, the flow could jump to
			;; this component directly
			(= (responses_received ?q) (responses_required ?q))
 			(= (component_position ?q) (current_component))
			(< (current_com_act) (com_act_position ?l))
			;; to avoid to be executed if we are in the farewell
			(is_first_of_component_farewell ?l)
			(belongs ?l ?q)
			)

     :effect (and (assign (current_com_act) (com_act_position ?l))
		  ;; automatic
		  ))
  

  ;; this is the last action of the nominal flow of a component
  (:action end-component-interaction
     :parameters (?q - component ?l - com_act)
     :precondition (and 
			(can_continue)
			(= (responses_received ?q) (responses_required ?q))
 			(= (component_position ?q) (current_component))
			(not (requires_response_confirmation ?q))
			(is_last_of_component ?l)
			(belongs ?l ?q)
			(> (current_com_act) (com_act_position ?l))
			)

     :effect (and (increase (current_component) 1)
		  ;; automatic 
		  (assign (current_com_act) 0)
		  ;; automatic
		  (not (waiting_for_response))
		  ;; automatic
		  (component_finished ?q)
		  ;; external (we need to check that it has been said)
		  )
     )
     
  ;; to offer confirmation in components that need it
    (:action offer-confirmation
     :parameters (?q - component  ?l - com_act)
     :precondition (and
		    (can_continue)
		    (= (responses_received ?q) (responses_required ?q))
                    (= (component_position ?q) (current_component))
		    (requires_response_confirmation ?q)
		    (is_last_of_component ?l)
		    (belongs ?l ?q)
		    (= (current_com_act) (com_act_position ?l)))

     :effect (and (waiting_for_response_confirmation)
		  ;; automatic 
		  ))

    ;; to receive the confirmation to the patient's answer
    (:action receive-confirmation
     :parameters (?q - component)
     :precondition (and 
			(can_continue)
			(= (responses_received ?q) (responses_required ?q))
 			(= (component_position ?q) (current_component))
			(requires_response_confirmation ?q)
			(waiting_for_response_confirmation)
			(not (response_change_required))
			)

     :effect (and
	      (not (waiting_for_response_confirmation))
	      ;; automatic
	      (not (response_change_required))
	      ;; external
	      (not (waiting_for_response))
	      ;; atomatic
	      (not (requires_response_confirmation ?q))
	      ;; automatic
	  ) )

    ;; to offer the patient to change the answer if the answer is not confirmed
    (:action offer-response-change
     :parameters (?q - component)
     :precondition (and 
			(can_continue)
			(= (responses_received ?q) (responses_required ?q))
 			(= (component_position ?q) (current_component))
			(requires_response_confirmation ?q)
			(waiting_for_response_confirmation)
			(response_change_required)
			)

     :effect (and (waiting_for_response_change)
		  ;; automatic 
		  )
     )

    ;; to offer the patient to change the answer if the answer is not confirmed
    (:action receive-changed-response
     :parameters (?q - component)
     :precondition (and 
			(can_continue)
			(= (responses_received ?q) (responses_required ?q))
 			(= (component_position ?q) (current_component))
			(requires_response_confirmation ?q)
			(waiting_for_response_change)
			(response_change_required)
			)

     :effect (and  (not (waiting_for_response_change))
		   ;; external
		   (not (response_change_required))
		   ;; automatic
		   (not (waiting_for_response_confirmation))
		   ;; automatic
		   (not (waiting_for_response))
		   ;; automatic 

		   (not (requires_response_confirmation ?q))
		   ;; automatic
		  ))


  ;; this action raises the response_failed_event when this com_act has been repeated its maximum number of times and
  ;; the answer (or a plausible/correct one) has not been received.
  (:action raise-response-failed-event
      :parameters (?q - component ?l - com_act ?e - event)
      :precondition (and 
     		   ;;common to all
		   (can_continue)
		   ;;specific
  		   (belongs ?l ?q)
  		   (= (com_act_position ?l) (current_com_act))
  		   (= (repetitions ?l) (max_repetitions ?l))
		   (not (waiting_for_response))
		   (not (response_received ?q))
		   (response_failed_event ?e)
		   (= (component_position ?q) (current_component))
		   (requires_response ?l)
  		    )
      :effect (and
	       (increase (number_failed_responses ?q) 1)
	       ;; automatic
	       (detected_event ?e)
	       ;; automatic
               (not (can_continue))
	       ;; automatic
	))

 ;; the action applied when an external event is produced and we can continue with the next item once restored
 (:action handle-event-and-resume
       	:parameters (?q - component ?e - event ?b - behavior)
       	:precondition (and 
       		      (not (can_continue))
		      (detected_event ?e)
		      (behavior_of_event ?q ?e ?b)
      		      (= (component_position ?q) (current_component))
		      (not (has_restoration_points ?q))
		      )
	:effect (and 
		 (can_continue)
		 ;; automatic
		 (not (detected_event ?e))
		 ;; external	   
	))

 ;; the action applied when an external event is produced and we need to go back to a previous step
 (:action handle-event-and-go-back
       	:parameters (?q - component ?e - event ?b - behavior ?l - com_act)
       	:precondition (and 
       		      (not (can_continue))
		      (detected_event ?e)
		      (behavior_of_event ?q ?e ?b)
      		      (= (component_position ?q) (current_component))
		      (has_restoration_points ?q)
		      (current_restoration_point ?l)
		      (belongs ?l ?q)
		      (not (response_received ?q)) ;; if we just received the answer, we keep going
		      )
	:effect (and 
		 (can_continue)
		 ;; automatic
		 (not (detected_event ?e))
		 ;; external
		 (assign (current_com_act) (com_act_position ?l))
		 ;; automatic
		 ;; If we were waiting for the answer we repeat the component
		 (not (waiting_for_response))
		 ;; automatic
		 )
  )

 ;; the action applied when there is no answer or it is wrong or not plausible
 ;;Notice that this is only applied after all the tries to obtain the answer have been exhausted
 (:action handle-wrong-response
       	:parameters (?q - component ?l - com_act ?e - event ?b - behavior)
       	:precondition (and 
       		      (not (can_continue))
		      (detected_event ?e)
		      (response_failed_behavior ?l ?e ?b)
		      (= (component_position ?q) (current_component))
		      (= (com_act_position ?l) (current_com_act))
		      (belongs ?l ?q)
		      )
	:effect (and 
		 (can_continue)
		 ;; automatic
		 (not (detected_event ?e))
		 ;; external
		 (increase (current_com_act) 1)
		 ;; automatic
		 )
  )

  ;; this action is executed when the number of total failed answers is reached. It triggers the component failed event
  (:action raise-component-failed-event-no-answer
      :parameters (?q - component ?e - event)
      :precondition (and 
     		    	;;common to all
		    	(can_continue)
		    	;;specific
		    	(component_failed_event ?e)
		    	(= (number_failed_responses ?q) (max_failed_responses ?q))
		    	(= (component_position ?q) (current_component))
			(not (has_alternative ?q))
  		    )
      :effect (and
	       (detected_event ?e)
	       ;; automatic
	       (assign (number_failed_responses ?q) 0)
	       ;; automatic
	       (not (can_continue))
	       ;; automatic
	       (skip_response ?q)
	       ;; automatic
	       (increase (number_failed_components) 1)
	       ;; automatic
	   )
  )

  ;; this action is executed when we have failed to give answer to a component and we have an alternative way to do it
  (:action switch-to-alternative-component
      :parameters (?q - component ?a - component ?e - event)
      :precondition (and 
     		    	;;common to all
		    	(can_continue)
		    	;;specific
		    	(component_failed_event ?e)
		    	(= (number_failed_responses ?q) (max_failed_responses ?q))
		    	(= (component_position ?q) (current_component))
			(has_alternative ?q)
			(alternative ?a ?q)
  		    )
      :effect (and
	       (assign (component_position ?a) (current_component))
	       ;; automatic-always
	       (assign (component_position ?q) -1)
	       ;; automatic-always 
	   )
  )

   ;; this action is executed when we have come to this restoration point more times than the allowed ones
  (:action raise-component-failed-event-max-repetitions
      :parameters (?q - component ?e - event ?l - com_act)
      :precondition (and 
     		   ;;common to all
		   (can_continue)
		   ;;specific
		   (component_failed_event ?e)
		   (= (com_act_position ?l) (current_com_act))
		   (belongs ?l ?q)
		   (= (repetitions ?l) (max_repetitions ?l))
		   (= (component_position ?q) (current_component))
		   (is_restoration_point ?l)
		   (not (has_alternative ?q))
		   (not (requires_response ?l))
		   ;; to not be raised instead of prepare-farewell in components with feedback
  		   )
      :effect (and
	       (detected_event ?e)
	       ;; automatic
	       (not (can_continue))
	       ;; automatic
	       (skip_response ?q)
	       ;; automatic
	       (increase (number_failed_components) 1)
	       ;; automatic
	))
    
  ;; this action is executed when the (skip_response) predicate is true and the event raised for not having an answer
  ;; has been already handled. It can be triggered directly by the patient or indirectly because he/she
  ;; failed to provide a suitable answer after the maximum number of tries for this component.
    (:action skip-component
      :parameters (?q - component)
      :precondition (and 
     		   ;;common to all
		   (can_continue)
		   ;;specific
  		   (skip_response ?q)
		   (< (number_failed_components) (max_failed_components))
		   (= (component_position ?q) (current_component))
  		    )
      :effect (and
	       (component_finished ?q)
	       ;; automatic
	       (increase (current_component) 1)
	       ;; automatic
	       (assign (current_com_act) 0)
	       ;; automatic
	       (not (waiting_for_response))
	       ;; automatic
	       (call_supervisor_at_end) 
	       ;; automatic
	))

 ;; this action is executed when the (skip_response) predicate is true and the number of total failed components 
 ;; is reached. It triggers the corresponding event.
    (:action raise-max-components-failed-event
      :parameters (?q - component ?e - event)
      :precondition (and 
     		   ;;common to all
		   (can_continue)
		   ;;specific
  		   (skip_response ?q)
		   (max_components_failed_event ?e)
		   (= (number_failed_components) (max_failed_components))
	      	   (= (component_position ?q) (current_component))
  		    )
      :effect (and
	       (detected_event ?e)
	       ;; automatic
	       (assign (number_failed_components) 0)
	       ;; automatic
	       (not (can_continue))
	       ;; automatic
	))

    ;; this action is executed when the test is finished and no component has been skipped
    (:action finish-interaction-ok 
      :parameters (?q - component)
      :precondition (and (= (component_position ?q) (number_of_components))
      		    	 (component_finished ?q)
			 (not (call_supervisor_at_end))
      		    )
      :effect (and (interaction_finished)
		   ;; automatic
      	 ))

    ;; this action is executed when the test is finished and at least one component has been skipped
    ;; the doctor must be called to finish the test
    (:action finish-interaction-call-supervisor
      :parameters (?q - component)
      :precondition (and (= (component_position ?q) (number_of_components))
      		    	 (component_finished ?q)
    		   	 (call_supervisor_at_end)
      		    )
      :effect (and (interaction_finished)
		   ;; automatic
      	      ))
)
