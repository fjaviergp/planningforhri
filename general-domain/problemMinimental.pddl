(define (problem minimental)
(:domain interaction)
(:objects
	;;Events raised by the planner
	component_failed response_failed max_q_failed max_a_failed - event
	;;External events
	doctor_needed patient_absent want_change - event
	;;Behaviors
	call_doctor call_patient ignore restart_component ask_new_response - behavior
	hint_if_plausible show_picture check_eyes - behavior
	
	;qI1
	qI1 - component
	robot_pres1 intro1 intro2 intro3 intro4 intro5 intro6 intro7  - com_act
	;qI2
	;q1
	q1 - component
	q1_a1 q1_t - com_act
	;q2
	q2 - component
	q2_a1 q2_t - com_act
	;q3
	q3 - component
	q3_a1 q3_t - com_act
	;q4
	q4 - component
	q4_a1 q4_t - com_act
	;q5
	q5 - component
	q5_a1 q5_t - com_act
	;q6
	q6 - component
	q6_a1 q6_h1 q6_a2 q6_t - com_act
	;q7
	q7 - component
	q7_a1 q7_t - com_act
	;q8
	 q8 - component
	 q8_a1 q8_t - com_act
	;q9
	q9 - component
	q9_a1  q9_t - com_act
	;q10
	q10 - component
	q10_a1 q10_t - com_act
	;q11
	q11 - component
	q11_s1 q11_e1 q11_a1 q11_t - com_act
	;q12
	q12 - component
	q12_a1 q12_t - com_act
	;q13
	q13 - component
	q13_s1 q13_s2 q13_s3 q13_a1  q13_a2  q13_a3  q13_a4  q13_a5 q13_t - com_act
	;alternative to q13
	q13a - component
	q13a_s1 q13a_a1 q13a_t - com_act
	;q14
	q14 - component
	q14_s1 q14_a1 q14_t - com_act
	;q15
	q15 - component
	q15_s1 q15_s2 q15_e1 q15_a1 q15_t - com_act
	;q16
	q16 - component
	q16_s1 q16_e1 q16_a1 q16_t - com_act
	;q17
	q17 - component
	q17_s1 q17_s2 q17_s3 q17_e1 q17_a1 q17_s4 q17_s5 q17_e2 q17_a2 q17_t - com_act
	;q18
	q18 - component
	q18_s1 q18_o1 q18_o2 q18_e1 q18_a1 q18_t - com_act
	;q19
	q19 - component
	q19_s1 q19_e1 q19_a1 q19_t - com_act
	;q20
	q20 - component
	q20_s1 q20_e1 q20_a1 q20_t - com_act
	;q21
	q21 - component
	q21_s1 q21_o1 q21_e1 q21_a1 q21_t - com_act
)
(:init 
       ;;General
       (can_continue)
;       (detected_event patient_absent)
       (= (number_failed_components) 0)
       (= (max_failed_components) 2)
       (= (number_of_components) 21)
       (= (current_component) 0)
       (component_failed_event component_failed)
       (max_components_failed_event max_q_failed)
       (response_failed_event response_failed)
       (max_responses_failed_event max_a_failed)
       (= (current_com_act) 0)
;=============================================================================  
       ;;qI1
       (= (component_position qI1) 0)
       (behavior_of_event qI1 component_failed call_doctor)
       (behavior_of_event qI1 doctor_needed call_doctor)
       (behavior_of_event qI1 patient_absent call_patient)
       (behavior_of_event qI1 max_q_failed call_doctor)
       ;; Response is not needed if the patient says nothing, we continue
       (behavior_of_event qI1 max_a_failed ignore)
       (= (responses_received qI1) 0)
       (= (responses_required qI1) 1)
       (= (number_failed_responses qI1) 0)
       (= (max_failed_responses qI1) 1)

       ;;The robot speaks 7 times without waiting for response, response comes in the 8th speach
       (belongs robot_pres1 qI1)
       (= (com_act_position robot_pres1) 0)
       (= (max_repetitions robot_pres1) 1)
       (= (repetitions robot_pres1) 0)

       (belongs intro1 qI1)
       (= (com_act_position intro1) 1)
       (= (max_repetitions intro1) 1)
       (= (repetitions intro1) 0)

       (belongs intro2 qI1)
       (= (com_act_position intro2) 2)
       (= (max_repetitions intro2) 1)
       (= (repetitions intro2) 0)

       (belongs intro3 qI1)
       (= (com_act_position intro3) 3)
       (= (max_repetitions intro3) 1)
       (= (repetitions intro3) 0)

       (belongs intro4 qI1)
       (= (com_act_position intro4) 4)
       (= (max_repetitions intro4) 1)
       (= (repetitions intro4) 0)

       (belongs intro5 qI1)
       (= (com_act_position intro5) 5)
       (= (max_repetitions intro5) 1)
       (= (repetitions intro5) 0)

       (belongs intro6 qI1)
       (= (com_act_position intro6) 6)
       (= (max_repetitions intro6) 1)
       (= (repetitions intro6) 0)

       ;Now we ask if the user wants to repeat the instructions
       (belongs intro7 qI1)
       (= (com_act_position intro7) 7)
       (= (max_repetitions intro7) 1)
       (= (repetitions intro7) 0)
       (requires_response intro7)
       (requires_plausible_response intro7)
       (requires_correct_response intro7)
       (response_failed_behavior intro7 response_failed restart_component)
       (is_last_of_component intro7)

;==================================================================================
;; First part of the test, q1 to q10 are equal. The exception is q6 that has a hint
;==================================================================================
  	;;q1
	;==========================================================================
       (= (component_position q1) 1)
       (behavior_of_event q1 component_failed ignore)
       (behavior_of_event q1 doctor_needed call_doctor)
       (behavior_of_event q1 patient_absent call_patient)
       (behavior_of_event q1 max_q_failed call_doctor)
       (behavior_of_event q1 max_a_failed ignore)
       (behavior_of_event q1 want_change ask_new_response)
       (= (responses_received q1) 0)
       (= (responses_required q1) 1)
       (= (number_failed_responses q1) 0)
       (= (max_failed_responses q1) 1)
      
       (belongs q1_a1 q1)
       (= (com_act_position q1_a1) 0)
       (= (max_repetitions q1_a1) 3)
       (= (repetitions q1_a1) 0)
       (requires_response q1_a1)
       (response_failed_behavior q1_a1 response_failed ignore)
       (is_restoration_point q1_a1) ;;if something happens while saying this or receiving response

       (belongs q1_t q1)
       (= (com_act_position q1_t) 1)
       (= (max_repetitions q1_t) 3);;after the third try to say it, we skip it
       (= (repetitions q1_t) 0)
       (is_last_of_component q1_t)
       (is_restoration_point q1_t) ;;if something happens in the farewell it will come here
       
       ;===========================================================================
       ;;q2
       ;===========================================================================
       (= (component_position q2) 2)
       (behavior_of_event q2 component_failed ignore)
       (behavior_of_event q2 doctor_needed call_doctor)
       (behavior_of_event q2 patient_absent call_patient)
       (behavior_of_event q2 max_q_failed call_doctor)
       (behavior_of_event q2 max_a_failed ignore)
       (behavior_of_event q2 want_change ask_new_response)
       (= (responses_received q2) 0)
       (= (responses_required q2) 1)
       (= (number_failed_responses q2) 0)
       (= (max_failed_responses q2) 1)
      
       (belongs q2_a1 q2)
       (= (com_act_position q2_a1) 0)
       (= (max_repetitions q2_a1) 3)
       (= (repetitions q2_a1) 0)
       (requires_response q2_a1)
       (response_failed_behavior q2_a1 response_failed ignore)
       (is_restoration_point q2_a1)

       (belongs q2_t q2)
       (= (com_act_position q2_t) 1)
       (= (max_repetitions q2_t) 1)
       (= (repetitions q2_t) 0)
       (is_last_of_component q2_t)
       (is_restoration_point q2_t)
       
       ;==============================================================================
       ;;q3
       ;==============================================================================
       (= (component_position q3) 3)
       (behavior_of_event q3 component_failed ignore)
       (behavior_of_event q3 doctor_needed call_doctor)
       (behavior_of_event q3 patient_absent call_patient)
       (behavior_of_event q3 max_q_failed call_doctor)
       (behavior_of_event q3 max_a_failed ignore)
       (behavior_of_event q3 want_change ask_new_response)
       (= (responses_received q3) 0)
       (= (responses_required q3) 1)
       (= (number_failed_responses q3) 0)
       (= (max_failed_responses q3) 1)
      
       (belongs q3_a1 q3)
       (= (com_act_position q3_a1) 0)
       (= (max_repetitions q3_a1) 3)
       (= (repetitions q3_a1) 0)
       (requires_response q3_a1)
       (response_failed_behavior q3_a1 response_failed ignore)
       (is_restoration_point q3_a1)

       (belongs q3_t q3)
       (= (com_act_position q3_t) 1)
       (= (max_repetitions q3_t) 1)
       (= (repetitions q3_t) 0)
       (is_last_of_component q3_t)
       (is_restoration_point q3_t)
       
       ;==============================================================================
       ;;q4
       ;==============================================================================
       (= (component_position q4) 4)
       (behavior_of_event q4 component_failed ignore)
       (behavior_of_event q4 doctor_needed call_doctor)
       (behavior_of_event q4 patient_absent call_patient)
       (behavior_of_event q4 max_q_failed call_doctor)
       (behavior_of_event q4 max_a_failed ignore)
       (behavior_of_event q4 want_change ask_new_response)
       (= (responses_received q4) 0)
       (= (responses_required q4) 1)
       (= (number_failed_responses q4) 0)
       (= (max_failed_responses q4) 1)
      
       (belongs q4_a1 q4)
       (= (com_act_position q4_a1) 0)
       (= (max_repetitions q4_a1) 3)
       (= (repetitions q4_a1) 0)
       (requires_response q4_a1)
       (response_failed_behavior q4_a1 response_failed ignore)
       (is_restoration_point q4_a1)

       (belongs q4_t q4)
       (= (com_act_position q4_t) 1)
       (= (max_repetitions q4_t) 1)
       (= (repetitions q4_t) 0)
       (is_last_of_component q4_t)
       (is_restoration_point q4_t)

       ;==============================================================================
       ;;q5
       ;==============================================================================	
       (= (component_position q5) 5)
       (behavior_of_event q5 component_failed ignore)
       (behavior_of_event q5 doctor_needed call_doctor)
       (behavior_of_event q5 patient_absent call_patient)
       (behavior_of_event q5 max_q_failed call_doctor)
       (behavior_of_event q5 max_a_failed ignore)
       (behavior_of_event q5 want_change ask_new_response)
       (= (responses_received q5) 0)
       (= (responses_required q5) 1)
       (= (number_failed_responses q5) 0)
       (= (max_failed_responses q5) 1)
       
       (belongs q5_a1 q5)
       (= (com_act_position q5_a1) 0)
       (= (max_repetitions q5_a1) 3)
       (= (repetitions q5_a1) 0)
       (requires_response q5_a1)
       (response_failed_behavior q5_a1 response_failed ignore)
       (is_restoration_point q5_a1)

       (belongs q5_t q5)
       (= (com_act_position q5_t) 1)
       (= (max_repetitions q5_t) 1)
       (= (repetitions q5_t) 0)
       (is_last_of_component q5_t)
       (is_restoration_point q5_t)

       ;==============================================================================
       ;;q6
       ;==============================================================================
       ;;this component has a hint if the first response is not correct
       (= (component_position q6) 6)
       (behavior_of_event q6 component_failed ignore)
       (behavior_of_event q6 doctor_needed call_doctor)
       (behavior_of_event q6 patient_absent call_patient)
       (behavior_of_event q6 max_q_failed call_doctor)
       (behavior_of_event q6 max_a_failed ignore)
       (behavior_of_event q6 want_change ask_new_response)
       (= (responses_received q6) 0)
       (= (responses_required q6) 1)
       (= (number_failed_responses q6) 0)
       (= (max_failed_responses q6) 1)
       ;;If the patient gives an response which is not totally correct, we give a hint
       (has_hints q6)
       (is_hint q6_a1 q6_h1)
       
       (belongs q6_a1 q6)
       (= (com_act_position q6_a1) 0)
       (= (max_repetitions q6_a1) 3)
       (= (repetitions q6_a1) 0)
       (requires_response q6_a1)
       (requires_plausible_response q6_a1)
       (response_failed_behavior q6_a1 response_failed ignore)
       ;;	(required_behavior q6_a1 hint_if_plausible)
       ;;	(requires_behavior q6_a1)
       (is_restoration_point q6_a1)

       (belongs q6_t q6)
       (= (com_act_position q6_t) 1)
       (= (max_repetitions q6_t) 1)
       (= (repetitions q6_t) 0)
       (is_last_of_component q6_t)
       (is_first_of_component_farewell q6_t)
       (is_restoration_point q6_t)

       ;==============================================================================
       ;;q7
       ;==============================================================================
       (= (component_position q7) 7)
       (behavior_of_event q7 component_failed ignore)
       (behavior_of_event q7 doctor_needed call_doctor)
       (behavior_of_event q7 patient_absent call_patient)
       (behavior_of_event q7 max_q_failed call_doctor)
       (behavior_of_event q7 max_a_failed ignore)
       (behavior_of_event q7 want_change ask_new_response)
       (= (responses_received q7) 0)
       (= (responses_required q7) 1)
       (= (number_failed_responses q7) 0)
       (= (max_failed_responses q7) 1)
       
       (belongs q7_a1 q7)
       (= (com_act_position q7_a1) 0)
       (= (max_repetitions q7_a1) 3)
       (= (repetitions q7_a1) 0)
       (requires_response q7_a1)
       (response_failed_behavior q7_a1 response_failed ignore)
       (is_restoration_point q7_a1)

       (belongs q7_t q7)
       (= (com_act_position q7_t) 1)
       (= (max_repetitions q7_t) 1)
       (= (repetitions q7_t) 0)
       (is_last_of_component q7_t)
       (is_restoration_point q7_t)

       ;==============================================================================
       ;;q8
       ;==============================================================================
       (= (component_position q8) 8)
       (behavior_of_event q8 component_failed ignore)
       (behavior_of_event q8 doctor_needed call_doctor)
       (behavior_of_event q8 patient_absent call_patient)
       (behavior_of_event q8 max_q_failed call_doctor)
       (behavior_of_event q8 max_a_failed ignore)
       (behavior_of_event q8 want_change ask_new_response)
       (= (responses_received q8) 0)
       (= (responses_required q8) 1)
       (= (number_failed_responses q8) 0)
       (= (max_failed_responses q8) 1)
       
       (belongs q8_a1 q8)
       (= (com_act_position q8_a1) 0)
       (= (max_repetitions q8_a1) 3)
       (= (repetitions q8_a1) 0)
       (requires_response q8_a1)
       (response_failed_behavior q8_a1 response_failed ignore)
       (is_restoration_point q8_a1)

       (belongs q8_t q8)
       (= (com_act_position q8_t) 1)
       (= (max_repetitions q8_t) 1)
       (= (repetitions q8_t) 0)
       (is_last_of_component q8_t)
       (is_restoration_point q8_t)

       ;==============================================================================
       ;;q9
       ;==============================================================================
       (= (component_position q9) 9)
       (behavior_of_event q9 component_failed ignore)
       (behavior_of_event q9 doctor_needed call_doctor)
       (behavior_of_event q9 patient_absent call_patient)
       (behavior_of_event q9 max_q_failed call_doctor)
       (behavior_of_event q9 max_a_failed ignore)
       (behavior_of_event q9 want_change ask_new_response)
       (= (responses_received q9) 0)
       (= (responses_required q9) 1)
       (= (number_failed_responses q9) 0)
       (= (max_failed_responses q9) 1)
       
       (belongs q9_a1 q9)
       (= (com_act_position q9_a1) 0)
       (= (max_repetitions q9_a1) 3)
       (= (repetitions q9_a1) 0)
       (requires_response q9_a1)
       (response_failed_behavior q9_a1 response_failed ignore)
       (is_restoration_point q9_a1)

       (belongs q9_t q9)
       (= (com_act_position q9_t) 1)
       (= (max_repetitions q9_t) 1)
       (= (repetitions q9_t) 0)
       (is_last_of_component q9_t)
       (is_restoration_point q9_t)

       ;==============================================================================
       ;;q10
       ;==============================================================================
       (= (component_position q10) 10)
       (behavior_of_event q10 component_failed ignore)
       (behavior_of_event q10 doctor_needed call_doctor)
       (behavior_of_event q10 patient_absent call_patient)
       (behavior_of_event q10 max_q_failed call_doctor)
       (behavior_of_event q10 max_a_failed ignore)
       (behavior_of_event q10 want_change ask_new_response)
       (= (responses_received q10) 0)
       (= (responses_required q10) 1)
       (= (number_failed_responses q10) 0)
       (= (max_failed_responses q10) 1)
       
       (belongs q10_a1 q10)
       (= (com_act_position q10_a1) 0)
       (= (max_repetitions q10_a1) 3)
       (= (repetitions q10_a1) 0)
       (requires_response q10_a1)
       (response_failed_behavior q10_a1 response_failed ignore)
       (is_restoration_point q10_a1)

       (belongs q10_t q10)
       (= (com_act_position q10_t) 1)
       (= (max_repetitions q10_t) 1)
       (= (repetitions q10_t) 0)
       (is_last_of_component q10_t)
       (is_restoration_point q10_t)

       ;==============================================================================
       ;;q11
       ;==============================================================================
       (= (component_position q11) 11)
       (behavior_of_event q11 component_failed ignore)
       (behavior_of_event q11 doctor_needed call_doctor)
       (behavior_of_event q11 patient_absent call_patient)
       (behavior_of_event q11 max_q_failed call_doctor)
       (behavior_of_event q11 max_a_failed ignore)
       (behavior_of_event q11 want_change ask_new_response)
       (= (responses_received q11) 0)
       (= (responses_required q11) 1)
       (= (number_failed_responses q11) 0)
       (= (max_failed_responses q11) 1)

       (belongs q11_s1 q11)
       (= (com_act_position q11_s1) 0)
       (= (max_repetitions q11_s1) 1)
       (= (repetitions q11_s1) 0)

       (belongs q11_e1 q11)
       (= (com_act_position q11_e1) 1)
       (= (max_repetitions q11_e1) 1)
       (= (repetitions q11_e1) 0)
       
       (belongs q11_a1 q11)
       (= (com_act_position q11_a1) 2)
       (= (max_repetitions q11_a1) 1) ;;we cannot repeat the component
       (= (repetitions q11_a1) 0)
       (requires_response q11_a1)
       (response_failed_behavior q11_a1 response_failed ignore)


       (belongs q11_t q11)
       (= (com_act_position q11_t) 3)
       (= (max_repetitions q11_t) 1)
       (= (repetitions q11_t) 0)
       (is_last_of_component q11_t)
       (is_restoration_point q11_t) ;;not really needed as no other r.p. in the component

       ;==============================================================================
       ;;q12
       ;==============================================================================
       (= (component_position q12) 12)
       (behavior_of_event q12 component_failed ignore)
       (behavior_of_event q12 doctor_needed call_doctor)
       (behavior_of_event q12 patient_absent call_patient)
       (behavior_of_event q12 max_q_failed call_doctor)
       (behavior_of_event q12 max_a_failed ignore)
       (behavior_of_event q12 want_change ignore) ;;In this case after last repetition
       			      		  	  ;;it cannot hear the component again
       (= (responses_received q12) 0)
       (= (responses_required q12) 1)
       (= (number_failed_responses q12) 0)
       (= (max_failed_responses q12) 1)
       
       (belongs q12_a1 q12)
       (= (com_act_position q12_a1) 0)
       (= (max_repetitions q12_a1) 6) ;;Six times to get the right response (No, keep going)
       (= (repetitions q12_a1) 0)
       (requires_response q12_a1)
       (requires_plausible_response q12_a1)
       (requires_correct_response q12_a1)
       (response_failed_behavior q12_a1 response_failed ignore)
       (is_restoration_point q12_a1)

       (belongs q12_t q12)
       (= (com_act_position q12_t) 1)
       (= (max_repetitions q12_t) 1)
       (= (repetitions q12_t) 0)
       (is_last_of_component q12_t)
       (is_restoration_point q12_t)

       ;==============================================================================
       ;;q13
       ;==============================================================================
       (= (component_position q13) 13)
       (behavior_of_event q13 component_failed ignore)
       (behavior_of_event q13 doctor_needed call_doctor)
       (behavior_of_event q13 patient_absent call_patient)
       (behavior_of_event q13 max_q_failed call_doctor)
       (behavior_of_event q13 max_a_failed ignore)
       (behavior_of_event q13 want_change ask_new_response)
       (= (responses_received q13) 0)
       (= (responses_required q13) 5) ;;5 substractions
       (= (number_failed_responses q13) 0)
       (= (max_failed_responses q13) 5) ;;it if fails all it will go to the alternative
       (has_alternative q13)
       (alternative q13a q13)

       (belongs q13_s1 q13)
       (= (com_act_position q13_s1) 0)
       (= (max_repetitions q13_s1) 3)
       (= (repetitions q13_s1) 0)
       (is_restoration_point q13_s1)

       (belongs q13_s2 q13)
       (= (com_act_position q13_s2) 1)
       (= (max_repetitions q13_s2) 3)
       (= (repetitions q13_s2) 0)

       (belongs q13_s3 q13)
       (= (com_act_position q13_s3) 2)
       (= (max_repetitions q13_s3) 3)
       (= (repetitions q13_s3) 0)
       
       (belongs q13_a1 q13)
       (= (com_act_position q13_a1) 3)
       (= (max_repetitions q13_a1) 3)
       (= (repetitions q13_a1) 0)
       (requires_response q13_a1)
       (requires_plausible_response q13_a1)
       (response_failed_behavior q13_a1 response_failed ignore)
       (is_restoration_point q13_a1)

       (belongs q13_a2 q13)
       (= (com_act_position q13_a2) 4)
       (= (max_repetitions q13_a2) 3)
       (= (repetitions q13_a2) 0)
       (requires_response q13_a2)
       (requires_plausible_response q13_a2)
       (response_failed_behavior q13_a2 response_failed ignore)
       (is_restoration_point q13_a2)

       (belongs q13_a3 q13)
       (= (com_act_position q13_a3) 5)
       (= (max_repetitions q13_a3) 3)
       (= (repetitions q13_a3) 0)
       (requires_response q13_a3)
       (requires_plausible_response q13_a3)
       (response_failed_behavior q13_a3 response_failed ignore)
       (is_restoration_point q13_a3)

       (belongs q13_a4 q13)
       (= (com_act_position q13_a4) 6)
       (= (max_repetitions q13_a4) 3)
       (= (repetitions q13_a4) 0)
       (requires_response q13_a4)
       (requires_plausible_response q13_a4)
       (response_failed_behavior q13_a4 response_failed ignore)
       (is_restoration_point q13_a4)

       (belongs q13_a5 q13)
       (= (com_act_position q13_a5) 7)
       (= (max_repetitions q13_a5) 3)
       (= (repetitions q13_a5) 0)
       (requires_response q13_a5)
       (requires_plausible_response q13_a5)
       (response_failed_behavior q13_a5 response_failed ignore)
       (is_restoration_point q13_a5)

       (belongs q13_t q13)
       (= (com_act_position q13_t) 8)
       (= (max_repetitions q13_t) 1)
       (= (repetitions q13_t) 0)
       (is_last_of_component q13_t)
       (is_restoration_point q13_t)

       ;==============================================================================
       ;;q13-alternative
       ;==============================================================================
       (behavior_of_event q13a component_failed ignore)
       (behavior_of_event q13a doctor_needed call_doctor)
       (behavior_of_event q13a patient_absent call_patient)
       (behavior_of_event q13a max_q_failed call_doctor)
       (behavior_of_event q13a max_a_failed ignore)
       (behavior_of_event q13a want_change ask_new_response)
       (= (responses_received q13a) 0)
       (= (responses_required q13a) 1)
       (= (number_failed_responses q13a) 0)
       (= (max_failed_responses q13a) 1)

       (belongs q13a_s1 q13a)
       (= (com_act_position q13a_s1) 0)
       (= (max_repetitions q13a_s1) 1)
       (= (repetitions q13a_s1) 0)

       (belongs q13a_a1 q13a)
       (= (com_act_position q13a_a1) 1)
       (= (max_repetitions q13a_a1) 3)
       (= (repetitions q13a_a1) 0)
       (requires_response q13a_a1)
       (requires_plausible_response q13a_a1)
       (response_failed_behavior q13a_a1 response_failed ignore)
       (is_restoration_point q13a_a1)

       (belongs q13a_t q13a)
       (= (com_act_position q13a_t) 2)
       (= (max_repetitions q13a_t) 1)
       (= (repetitions q13a_t) 0)
       (is_last_of_component q13a_t)
       (is_restoration_point q13a_t)

       ;==============================================================================
       ;;q14
       ;==============================================================================
       (= (component_position q14) 14)
       (behavior_of_event q14 component_failed ignore)
       (behavior_of_event q14 doctor_needed call_doctor)
       (behavior_of_event q14 patient_absent call_patient)
       (behavior_of_event q14 max_q_failed call_doctor)
       (behavior_of_event q14 max_a_failed ignore)
       (behavior_of_event q14 want_change ask_new_response)
       (= (responses_received q14) 0)
       (= (responses_required q14) 1)
       (= (number_failed_responses q14) 0)
       (= (max_failed_responses q14) 1)

       (belongs q14_s1 q14)
       (= (com_act_position q14_s1) 0)
       (= (max_repetitions q14_s1) 1)
       (= (repetitions q14_s1) 0)

       (belongs q14_a1 q14)
       (= (com_act_position q14_a1) 1)
       (= (max_repetitions q14_a1) 3)
       (= (repetitions q14_a1) 0)
       (requires_response q14_a1)
       (requires_plausible_response q14_a1)
       (requires_correct_response q14_a1)
       (response_failed_behavior q14_a1 response_failed ignore)
       (is_restoration_point q14_a1)

       (belongs q14_t q14)
       (= (com_act_position q14_t) 2)
       (= (max_repetitions q14_t) 1)
       (= (repetitions q14_t) 0)
       (is_last_of_component q14_t)
       (is_restoration_point q14_t)

       ;==============================================================================
       ;;q15
       ;==============================================================================
       (= (component_position q15) 15)
       (behavior_of_event q15 component_failed ignore)
       (behavior_of_event q15 doctor_needed call_doctor)
       (behavior_of_event q15 patient_absent call_patient)
       (behavior_of_event q15 max_q_failed call_doctor)
       (behavior_of_event q15 max_a_failed ignore)
       (behavior_of_event q15 want_change ask_new_response)
       (= (responses_received q15) 0)
       (= (responses_required q15) 1)
       (= (number_failed_responses q15) 0)
       (= (max_failed_responses q15) 1)

       (belongs q15_s1 q15)
       (= (com_act_position q15_s1) 0)
       (= (max_repetitions q15_s1) 1)
       (= (repetitions q15_s1) 0)

       (belongs q15_s2 q15)
       (= (com_act_position q15_s2) 1)
       (= (max_repetitions q15_s2) 1)
       (= (repetitions q15_s2) 0)

       (belongs q15_e1 q15)
       (= (com_act_position q15_e1) 2)
       (= (max_repetitions q15_e1) 1)
       (= (repetitions q15_e1) 0)

       (belongs q15_a1 q15)
       (= (com_act_position q15_a1) 3)
       (= (max_repetitions q15_a1) 3)
       (= (repetitions q15_a1) 0)
       (requires_response q15_a1)
       (is_restoration_point q15_a1)
       (required_behavior q15_a1 show_picture)
       (requires_behavior q15_a1)

       (belongs q15_t q15)
       (= (com_act_position q15_t) 4)
       (= (max_repetitions q15_t) 1)
       (= (repetitions q15_t) 0)
       (is_last_of_component q15_t)
       (is_restoration_point q15_t)

       ;==============================================================================
       ;;q16
       ;==============================================================================
       (= (component_position q16) 16)
       (behavior_of_event q16 component_failed ignore)
       (behavior_of_event q16 doctor_needed call_doctor)
       (behavior_of_event q16 patient_absent call_patient)
       (behavior_of_event q16 max_q_failed call_doctor)
       (behavior_of_event q16 max_a_failed ignore)
       (behavior_of_event q16 want_change ask_new_response)
       (= (responses_received q16) 0)
       (= (responses_required q16) 1)
       (= (number_failed_responses q16) 0)
       (= (max_failed_responses q16) 1)

       (belongs q16_s1 q16)
       (= (com_act_position q16_s1) 0)
       (= (max_repetitions q16_s1) 1)
       (= (repetitions q16_s1) 0)

       (belongs q16_e1 q16)
       (= (com_act_position q16_e1) 1)
       (= (max_repetitions q16_e1) 1)
       (= (repetitions q16_e1) 0)

       (belongs q16_a1 q16)
       (= (com_act_position q16_a1) 2)
       (= (max_repetitions q16_a1) 3)
       (= (repetitions q16_a1) 0)
       (requires_response q16_a1)
       (is_restoration_point q16_a1)
       (required_behavior q16_a1 show_picture)
       (requires_behavior q16_a1)

       (belongs q16_t q16)
       (= (com_act_position q16_t) 3)
       (= (max_repetitions q16_t) 1)
       (= (repetitions q16_t) 0)
       (is_last_of_component q16_t)
       (is_restoration_point q16_t)

       ;==============================================================================
       ;;q17
       ;==============================================================================
       (= (component_position q17) 17)
       (behavior_of_event q17 component_failed ignore)
       (behavior_of_event q17 doctor_needed call_doctor)
       (behavior_of_event q17 patient_absent call_patient)
       (behavior_of_event q17 max_q_failed call_doctor)
       (behavior_of_event q17 max_a_failed ignore)
       (behavior_of_event q17 want_change ask_new_response)
       (= (responses_received q17) 0)
       (= (responses_required q17) 1)
       (= (number_failed_responses q17) 0)
       (= (max_failed_responses q17) 1)

       (belongs q17_s1 q17)
       (= (com_act_position q17_s1) 0)
       (= (max_repetitions q17_s1) 1)
       (= (repetitions q17_s1) 0)

       (belongs q17_s2 q17)
       (= (com_act_position q17_s2) 1)
       (= (max_repetitions q17_s2) 1)
       (= (repetitions q17_s2) 0)

       (belongs q17_s3 q17)
       (= (com_act_position q17_s3) 2)
       (= (max_repetitions q17_s3) 1)
       (= (repetitions q17_s3) 0)

       (belongs q17_e1 q17)
       (= (com_act_position q17_e1) 3)
       (= (max_repetitions q17_e1) 1)
       (= (repetitions q17_e1) 0)

       (belongs q17_a1 q17)
       (= (com_act_position q17_a1) 4)
       (= (max_repetitions q17_a1) 1)
       (= (repetitions q17_a1) 0)
       (requires_response q17_a1)
       (is_restoration_point q17_a1)

       (belongs q17_s4 q17)
       (= (com_act_position q17_s4) 5)
       (= (max_repetitions q17_s4) 1)
       (= (repetitions q17_s4) 0)
       (is_restoration_point q17_s4)

       (belongs q17_s5 q17)
       (= (com_act_position q17_s5) 6)
       (= (max_repetitions q17_s5) 1)
       (= (repetitions q17_s5) 0)

       (belongs q17_e2 q17)
       (= (com_act_position q17_e2) 7)
       (= (max_repetitions q17_e2) 1)
       (= (repetitions q17_e2) 0)

       (belongs q17_a2 q17)
       (= (com_act_position q17_a2) 8)
       (= (max_repetitions q17_a2) 1)
       (= (repetitions q17_a2) 0)
       (requires_response q17_a2)
       (is_restoration_point q17_a2)
       
       (belongs q17_t q17)
       (= (com_act_position q17_t) 9)
       (= (max_repetitions q17_t) 1)
       (= (repetitions q17_t) 0)
       (is_last_of_component q17_t)
       (is_first_of_component_farewell q17_t)
       (is_restoration_point q17_t)

       ;==============================================================================
       ;;q18
       ;==============================================================================
       (= (component_position q18) 18)
       (behavior_of_event q18 component_failed ignore)
       (behavior_of_event q18 doctor_needed call_doctor)
       (behavior_of_event q18 patient_absent call_patient)
       (behavior_of_event q18 max_q_failed call_doctor)
       (behavior_of_event q18 max_a_failed ignore)
       (behavior_of_event q18 want_change ask_new_response)
       (= (responses_received q18) 0)
       (= (responses_required q18) 1)
       (= (number_failed_responses q18) 0)
       (= (max_failed_responses q18) 1)

       (belongs q18_s1 q18)
       (= (com_act_position q18_s1) 0)
       (= (max_repetitions q18_s1) 2)
       (= (repetitions q18_s1) 0)
       (is_restoration_point q18_s1) ;;it will come here, two chances

       (belongs q18_o1 q18)
       (= (com_act_position q18_o1) 1)
       (= (max_repetitions q18_o1) 2)
       (= (repetitions q18_o1) 0)

       (belongs q18_o2 q18)
       (= (com_act_position q18_o2) 2)
       (= (max_repetitions q18_o2) 2)
       (= (repetitions q18_o2) 0)

       (belongs q18_e1 q18)
       (= (com_act_position q18_e1) 3)
       (= (max_repetitions q18_e1) 2)
       (= (repetitions q18_e1) 0)

       (belongs q18_a1 q18)
       (= (com_act_position q18_a1) 4)
       (= (max_repetitions q18_a1) 1)
       (= (repetitions q18_a1) 0)
       (requires_response q18_a1)
       (is_restoration_point q18_a1)
       
       (belongs q18_t q18)
       (= (com_act_position q18_t) 5)
       (= (max_repetitions q18_t) 1)
       (= (repetitions q18_t) 0)
       (is_last_of_component q18_t)
       (is_first_of_component_farewell q18_t)
       (is_restoration_point q18_t)

       ;==============================================================================
       ;;q19
       ;==============================================================================
       (= (component_position q19) 19)
       (behavior_of_event q19 component_failed ignore)
       (behavior_of_event q19 doctor_needed call_doctor)
       (behavior_of_event q19 patient_absent call_patient)
       (behavior_of_event q19 max_q_failed call_doctor)
       (behavior_of_event q19 max_a_failed ignore)
       (behavior_of_event q19 want_change ask_new_response)
       (= (responses_received q19) 0)
       (= (responses_required q19) 1)
       (= (number_failed_responses q19) 0)
       (= (max_failed_responses q19) 1)

       (belongs q19_s1 q19)
       (= (com_act_position q19_s1) 0)
       (= (max_repetitions q19_s1) 1)
       (= (repetitions q19_s1) 0)

       (belongs q19_e1 q19)
       (= (com_act_position q19_e1) 1)
       (= (max_repetitions q19_e1) 1)
       (= (repetitions q19_e1) 0)

       (belongs q19_a1 q19)
       (= (com_act_position q19_a1) 2)
       (= (max_repetitions q19_a1) 1)
       (= (repetitions q19_a1) 0)
       (requires_response q19_a1)
       (required_behavior q19_a1 check_eyes)
       (requires_behavior q19_a1)

       (belongs q19_t q19)
       (= (com_act_position q19_t) 3)
       (= (max_repetitions q19_t) 1)
       (= (repetitions q19_t) 0)
       (is_last_of_component q19_t)
       (is_restoration_point q19_t)

       ;==============================================================================
       ;;q20
       ;==============================================================================
       (= (component_position q20) 20)
       (behavior_of_event q20 component_failed ignore)
       (behavior_of_event q20 doctor_needed call_doctor)
       (behavior_of_event q20 patient_absent call_patient)
       (behavior_of_event q20 max_q_failed call_doctor)
       (behavior_of_event q20 max_a_failed ignore)
       (behavior_of_event q20 want_change ask_new_response)
       (= (responses_received q20) 0)
       (= (responses_required q20) 1)
       (= (number_failed_responses q20) 0)
       (= (max_failed_responses q20) 1)

       (belongs q20_s1 q20)
       (= (com_act_position q20_s1) 0)
       (= (max_repetitions q20_s1) 3)
       (= (repetitions q20_s1) 0)
       (is_restoration_point q20_s1)

       (belongs q20_e1 q20)
       (= (com_act_position q20_e1) 1)
       (= (max_repetitions q20_e1) 3)
       (= (repetitions q20_e1) 0)

       (belongs q20_a1 q20)
       (= (com_act_position q20_a1) 2)
       (= (max_repetitions q20_a1) 1)
       (= (repetitions q20_a1) 0)
       (requires_response q20_a1)
       (is_restoration_point q20_a1)

       (belongs q20_t q20)
       (= (com_act_position q20_t) 3)
       (= (max_repetitions q20_t) 1)
       (= (repetitions q20_t) 0)
       (is_last_of_component q20_t)
       (is_restoration_point q20_t)

       ;==============================================================================
       ;;q21
       ;==============================================================================
       (= (component_position q21) 21)
       (behavior_of_event q21 component_failed ignore)
       (behavior_of_event q21 doctor_needed call_doctor)
       (behavior_of_event q21 patient_absent call_patient)
       (behavior_of_event q21 max_q_failed call_doctor)
       (behavior_of_event q21 max_a_failed ignore)
       (behavior_of_event q21 want_change ask_new_response)
       (= (responses_received q21) 0)
       (= (responses_required q21) 1)
       (= (number_failed_responses q21) 0)
       (= (max_failed_responses q21) 1)

       (belongs q21_s1 q21)
       (= (com_act_position q21_s1) 0)
       (= (max_repetitions q21_s1) 1)
       (= (repetitions q21_s1) 0)

       (belongs q21_o1 q21)
       (= (com_act_position q21_o1) 1)
       (= (max_repetitions q21_o1) 1)
       (= (repetitions q21_o1) 0)

       (belongs q21_e1 q21)
       (= (com_act_position q21_e1) 2)
       (= (max_repetitions q21_e1) 1)
       (= (repetitions q21_e1) 0)

       (belongs q21_a1 q21)
       (= (com_act_position q21_a1) 3)
       (= (max_repetitions q21_a1) 3)
       (= (repetitions q21_a1) 0)
       (requires_response q21_a1)
       (is_restoration_point q21_a1)
       (required_behavior q21_a1 show_picture)
       (requires_behavior q21_a1)

       (belongs q21_t q21)
       (= (com_act_position q21_t) 4)
       (= (max_repetitions q21_t) 1)
       (= (repetitions q21_t) 0)
       (is_last_of_component q21_t)
       (is_restoration_point q21_t)
)     

(:goal	(and (interaction_finished)
))
)

